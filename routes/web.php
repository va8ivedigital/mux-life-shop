<?php

use App\Http\Controllers\Web\ProductsController;
use App\Http\Controllers\Web\OrdersController;

Route::get('/us', function () {
    return redirect('/');
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Web', 'prefix' => config('app.route_prefix')], function () {
    //Home
    Route::get('/', 'HomeController@index')->name('home');

    //Blog
    Route::get('/blog-categories', 'BlogsController@blogCategories');
    Route::get('/blog', 'BlogsController@index')->name('blogs');
    Route::get('blog/{slug}', ['as'=>'blog.detail', 'uses' => 'BlogsController@detail'])->where('slug','[\w\d\-\_]+');
    Route::get('blog-categories/{slug}', ['as'=>'blog.blog_category', 'uses' => 'BlogsController@categoryWiseBlogs'])->where('slug','[\w\d\-\_]+');

    //FAQ
    Route::get('/faqs', 'FaqsController@index')->name('faqs');

    //About
    Route::get('/about', 'AboutController@index')->name('about');

    //Pages
    Route::get('/page/{slug}', 'PagesController@detail')->name('pages');

    //Contact Us
    Route::get('/contact', 'ContactController@index')->name('contact');

    //Shop
    Route::get('/shop', 'ProductsController@index')->name('products');

    //Product Categories
    Route::get('product-categories/{slug}', ['as'=>'product.product_categories', 'uses' => 'ProductsController@categoryWiseProducts'])->where('slug','[\w\d\-\_]+');

    //Product Tags
    Route::get('product-tag/{slug}', ['as'=>'product.product_tags', 'uses' => 'ProductsController@tagWiseProducts'])->where('slug','[\w\d\-\_]+');

    //Sale
    Route::get('/sale', 'SalesController@index')->name('sales');

    Route::get('/customer/register', 'CustomerController@registerPage');

    // Route::get('/customer-login', 'CustomerController@loginPage');

    Route::post('/register', 'CustomerController@registerForm');

    // Route::get('/logout', 'CustomerController@logoutCustomer');

    //Add to cart
    // Route::post('add-to-cart', [ProductsController::class,'addToCart']);

    Route::get('cart', [ProductsController::class,'cart']);

    Route::get('add-to-cart/{id}', [ProductsController::class,'addToCart']);

    Route::patch('update-cart', [ProductsController::class,'update']);

    Route::delete('remove-from-cart', [ProductsController::class,'remove']);

    Route::redirect('/login', '/customer/login');

    Route::get('checkout', [OrdersController::class,'checkout']);

    Route::post('/process-checkout', [OrdersController::class,'processCheckout']);

    Route::get('/search','ProductSearchController@search');
    /*
        //for old url to new url redirect work
        if(defined('OLD_URL')){
            Route::get('/'.OLD_URL, function () {
                $reg = config('app.route_prefix') ? '/'.config('app.route_prefix').'/' : config('app.route_prefix');
                return redirect($reg.SLUG_LINK);
            });
        }
        //for old url to new url redirect work end
        if(defined('BLOG_CATEGORY')){
            Route::get('/'.BLOG_CATEGORY, 'BlogsController@categoryBlogs');
        }
        if(defined('SLUG_LINK')){
            if(ROUTE_NAME == 'pages'){
                Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
            }

            if(ROUTE_NAME == 'blogs') {
                Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
            }

            if(ROUTE_NAME == 'categories') {
                Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
            }

            if(ROUTE_NAME == 'pages') {
                Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
            }

            if(ROUTE_NAME == 'product_categories') {
                Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
            }
        }
        else{
            Route::match(["get","post"], '/{parm1?}/{parm2?}/{parm3?}/{parm4?}', 'HomeController@_404')->name('FourOFour');
        }
    */
});


Route::prefix('customer')->as('customer.')->group(function() {
    Route::get('my-account', 'Customer\CustomerAccountController@index')->middleware('auth:customer')->name('home');
    Route::get('add-new-billing-address', 'Customer\CustomerAccountController@billingAddressPage')->name('add-new-billing');
    Route::post('new-billing-address', 'Customer\CustomerAccountController@addBillingAddress')->name('post-new-billing');
    Route::get('add-new-shipping-address', 'Customer\CustomerAccountController@shippingAddressPage')->name('add-new-shipping');
    Route::post('new-shipping-address', 'Customer\CustomerAccountController@addShippingAddress')->name('post-new-shipping');
    Route::get('manage-addresses', 'Customer\CustomerAccountController@manageAddresses')->name('manage-addresses');
    Route::post('assign-billing-address', 'Customer\CustomerAccountController@assignBillingAddress')->name('assign-billing-address');
    Route::post('assign-shipping-address', 'Customer\CustomerAccountController@assignShippingAddress')->name('assign-shipping-address');
    Route::namespace('Auth\Login')->group(function() {
        Route::get('login', 'CustomerController@showLoginForm')->name('login');
        Route::post('login', 'CustomerController@login')->name('login');
        Route::get('logout', 'CustomerController@logout')->name('logout');
    });
 });
