<?php
Route::redirect('/admin', 'admin/login');
Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Auth\LoginController@login');
Route::any('admin/logout', 'Auth\LoginController@logout')->name('logout');
Route::redirect('admin/home', '/admin');
Route::redirect('/admin/register', 'admin/login');
//Route::redirect('admin', '/admin/login');
Route::get('/admin/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

#Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('set-id', 'HomeController@setSiteId');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/media', 'UsersController@storeMedia')->name('users.storeMedia');
    Route::resource('users', 'UsersController');

    // Categories
    Route::delete('categories/destroy', 'CategoryController@massDestroy')->name('categories.massDestroy');
    Route::post('categories/media', 'CategoryController@storeMedia')->name('categories.storeMedia');
    Route::resource('categories', 'CategoryController');

	Route::post('ajaxhandleruploadbyck', 'UploadimageController@upload');

    // Blogs
    Route::delete('blogs/destroy', 'BlogController@massDestroy')->name('blogs.massDestroy');
    Route::post('blogs/media', 'BlogController@storeMedia')->name('blogs.storeMedia');
    Route::resource('blogs', 'BlogController');

    // Clients
    Route::delete('clients/destroy', 'ClientController@massDestroy')->name('clients.massDestroy');
    Route::post('clients/media', 'ClientController@storeMedia')->name('clients.storeMedia');
    Route::resource('clients', 'ClientController');

    // User Reviews
    Route::delete('reviews/destroy', 'ReviewController@massDestroy')->name('reviews.massDestroy');
    Route::post('reviews/media', 'ReviewController@storeMedia')->name('reviews.storeMedia');
    Route::resource('reviews', 'ReviewController');

    // FAQs
    Route::delete('faqs/destroy', 'FaqController@massDestroy')->name('faqs.massDestroy');
    Route::post('faqs/media', 'FaqController@storeMedia')->name('faqs.storeMedia');
    Route::resource('faqs', 'FaqController');

    // Sites
    Route::delete('sites/destroy', 'SitesController@massDestroy')->name('sites.massDestroy');
    Route::post('sites/media', 'SitesController@storeMedia')->name('sites.storeMedia');
    Route::resource('sites', 'SitesController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // Pages
    Route::delete('pages/destroy', 'PagesController@massDestroy')->name('pages.massDestroy');
    Route::post('pages/media', 'PagesController@storeMedia')->name('pages.storeMedia');
    Route::resource('pages', 'PagesController');

    // Contact
    Route::delete('contacts/destroy', 'ContactController@massDestroy')->name('contacts.massDestroy');
    Route::post('contacts/media', 'ContactController@storeMedia')->name('contacts.storeMedia');
    Route::resource('contacts', 'ContactController');

    // About
    Route::delete('about/destroy', 'AboutController@massDestroy')->name('about.massDestroy');
    Route::post('about/media', 'AboutController@storeMedia')->name('about.storeMedia');
    Route::resource('about', 'AboutController');

    // Teams
    Route::delete('teams/destroy', 'TeamsController@massDestroy')->name('teams.massDestroy');
    Route::post('teams/media', 'TeamsController@storeMedia')->name('teams.storeMedia');
    Route::resource('teams', 'TeamsController');

    // Presses
    Route::delete('presses/destroy', 'PressController@massDestroy')->name('presses.massDestroy');
    Route::post('presses/media', 'PressController@storeMedia')->name('presses.storeMedia');
    Route::resource('presses', 'PressController');

    // Tags
    Route::delete('tags/destroy', 'TagsController@massDestroy')->name('tags.massDestroy');
    Route::resource('tags', 'TagsController');

    // Product Categories
    Route::delete('product-categories/destroy', 'ProductCategoryController@massDestroy')->name('product-categories.massDestroy');
    Route::post('product-categories/media', 'ProductCategoryController@storeMedia')->name('product-categories.storeMedia');
    Route::resource('product-categories', 'ProductCategoryController');

    // Products
    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');
    Route::post('products/media', 'ProductsController@storeMedia')->name('products.storeMedia');
    Route::resource('products', 'ProductsController');

    // Addspace Stores
    Route::delete('addspace-stores/destroy', 'AddspaceStoresController@massDestroy')->name('addspace-stores.massDestroy');
    Route::resource('addspace-stores', 'AddspaceStoresController');

    // Add Space Products
    Route::delete('add-space-products/destroy', 'AddSpaceProductsController@massDestroy')->name('add-space-products.massDestroy');
    Route::resource('add-space-products', 'AddSpaceProductsController');

    // Banners
    Route::delete('banners/destroy', 'BannerController@massDestroy')->name('banners.massDestroy');
    Route::post('banners/media', 'BannerController@storeMedia')->name('banners.storeMedia');
    Route::resource('banners', 'BannerController');

    // Networks
    Route::delete('networks/destroy', 'NetworkController@massDestroy')->name('networks.massDestroy');
    Route::resource('networks', 'NetworkController');

    // Subscribers
    // Route::resource('subscribers', 'SubscribersController', ['except' => ['create', 'store', 'edit', 'update', 'show', 'destroy']]);
    Route::resource('subscribers', 'SubscribersController');
    Route::resource('contacts', 'ContactController');

    Route::post('unique_slug', 'SlugController@uniqueSlug');
    Route::post('filter_records', 'FilterController@records');
});
