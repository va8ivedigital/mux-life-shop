<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');

            $table->longText('short_description')->nullable();

            $table->longText('long_description')->nullable();

            $table->string('rating')->nullable();

            $table->string('price')->nullable();

            $table->string('old_price')->nullable();

            $table->integer('sort')->nullable();

            $table->boolean('sale')->default(0)->nullable();

            $table->boolean('latest')->default(0)->nullable();

            $table->boolean('featured')->default(0)->nullable();

            $table->boolean('popular')->default(0)->nullable();

            $table->boolean('publish')->default(0)->nullable();

            $table->string('image')->nullable();

            $table->string('additional_image')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
