<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_reviews', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();

            $table->string('user_review')->nullable();

            $table->boolean('publish')->default(0)->nullable();

            $table->integer('sort')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
