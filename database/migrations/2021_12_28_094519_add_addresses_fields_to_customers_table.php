<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressesFieldsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('shipping_address')->nullable();
            $table->foreign('shipping_address', 'shipping_address_id_fk_738352')->references('id')->on('shipping_addresses')->onDelete('cascade');
            $table->integer('billing_address')->nullable();
            $table->foreign('billing_address', 'billing_address_id_fk_738352')->references('id')->on('billing_addresses')->onDelete('cascade');
        });
    }
}
