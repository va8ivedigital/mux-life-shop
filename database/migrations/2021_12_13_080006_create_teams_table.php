<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();

            $table->string('designation')->nullable();

            $table->string('member_image')->nullable();

            $table->string('facebook_url')->nullable();

            $table->string('twitter_url')->nullable();

            $table->string('instagram_url')->nullable();

            $table->boolean('publish')->default(0)->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
