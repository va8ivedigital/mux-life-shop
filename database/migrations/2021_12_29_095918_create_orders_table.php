<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('customer_id')->nullable();
            $table->foreign('customer_id', 'customer_id_fk_738352')->references('id')->on('customers')->onDelete('cascade');
            $table->longText('billing_address')->nullable();
            // $table->foreign('billing_address_id', 'billing_address_id_fk_738352')->references('id')->on('billing_addresses')->onDelete('cascade');
            $table->integer('shipping_address_id')->nullable();
            // $table->foreign('shipping_address_id', 'shipping_address_id_fk_738352')->references('id')->on('shipping_addresses')->onDelete('cascade');
            $table->longText('order_items')->nullable();
            $table->string('delivery_option')->nullable();
            $table->string('payment_mode')->nullable();
            $table->integer('order_total')->nullable();
            $table->longText('order_notes')->nullable();
            $table->string('status')->nullable();
            $table->string('discount_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
