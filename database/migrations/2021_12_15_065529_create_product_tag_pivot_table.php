<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTagPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_tag', function (Blueprint $table) {
            $table->unsignedInteger('product_id');

            $table->foreign('product_id', 'product_id_fk_691683')->references('id')->on('products')->onDelete('cascade');

            $table->unsignedInteger('tag_id');

            $table->foreign('tag_id', 'tag_id_fk_691683')->references('id')->on('tags')->onDelete('cascade');
        });
    }
}
