<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable();
            $table->foreign('customer_id', 'customer_id_fk_738352')->references('id')->on('customers')->onDelete('cascade');
            // $table->integer('product_id')->nullable();
            // $table->foreign('product_id', 'product_id_fk_738352')->references('id')->on('products')->onDelete('cascade');
            $table->longText('products')->nullable();
            // $table->text('current_hash')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
