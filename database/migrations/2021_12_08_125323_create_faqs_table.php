<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();

            $table->longText('description')->nullable();

            $table->boolean('publish')->default(0)->nullable();

            $table->integer('sort')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
