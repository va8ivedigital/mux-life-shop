<?php

namespace App\Models;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'contact_us';

    protected $primaryKey   = 'id';
    protected $slug_prefix  = 'contact/';
    protected $page_type    = 'contacts';


    public function getBasicData()
    {
        $ret_arr = array();
        $ret_arr['table_name']  = $this->table;
        $ret_arr['primary_key'] = $this->primaryKey;
        $ret_arr['page_type']   = $this->page_type;
        $ret_arr['slug_prefix'] = $this->slug_prefix;
        return $ret_arr;
    }

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'embeded_map',
        'description',
        'address',
        'phone_number',
        'email',
        'publish',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function sites()
    {
        return $this->belongsToMany(Site::class);
    }
}
