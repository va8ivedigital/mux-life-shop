<?php

namespace App\Models;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Site extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia, Auditable;

    public $table = 'sites';

    protected $appends = [
        'flag',
        'logo',
        'favicon',
        'home_banner_image',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'url',
        'name',
        'twitter',
        'youtube',
        'publish',
        'facebook',
        'instagram',
        'pinterest',
        'html_tags',
        'linked_in',
        'created_at',
        'updated_at',
        'deleted_at',
        'country_name',
        'country_code',
        'language_code',
        'javascript_tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'site_flag',
        'site_favicon',
        'site_logo',
        'top_header_text',
        'home_banner_image',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function siteCategories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function sitePages()
    {
        return $this->belongsToMany(Page::class);
    }

    public function siteTags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function siteProductCategories()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function siteBanners()
    {
        return $this->belongsToMany(Banner::class);
    }

    public function siteClients()
    {
        return $this->belongsToMany(Client::class);
    }

    public function siteNetworks()
    {
        return $this->belongsToMany(Network::class);
    }

    public function siteBlogs()
    {
        return $this->belongsToMany(Blog::class);
    }

    public function siteProducts()
    {
        return $this->belongsToMany(Product::class);
    }

    public function getFlagAttribute()
    {
        $file = $this->getMedia('flag')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function getLogoAttribute()
    {
        $file = $this->getMedia('logo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function getFaviconAttribute()
    {
        $file = $this->getMedia('favicon')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function getHomeBannerImageAttribute()
    {
        $file = $this->getMedia('home_banner_image')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }
}
