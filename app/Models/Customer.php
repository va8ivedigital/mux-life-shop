<?php

namespace App\Models;

    use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable{

    use Notifiable;

    protected $guarded = ['customer'];

    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'dob',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAuthPassword(){
     return $this->password;
    }
}
