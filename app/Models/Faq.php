<?php

namespace App\Models;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'faqs';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'description',
        'publish',
        'sort',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function sites()
    {
        return $this->belongsToMany(Site::class);
    }
}
