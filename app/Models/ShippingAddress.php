<?php

namespace App\Models;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    use SoftDeletes, HasFactory, Auditable;

    protected $fillable = [
        'first_name',
        'last_name',
        'company',
        'phone_number',
        'address_1',
        'address_2',
        'zip_code',
        'city',
        'state',
        'country',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
}
