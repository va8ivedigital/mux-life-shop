<?php

namespace App\Models;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Slug;

class Tag extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'tags';

    protected $primaryKey   = 'id';
    protected $slug_prefix  = 'tag/';
    protected $page_type    = 'tags';

    public function getBasicData()
    {
        $ret_arr = array();
        $ret_arr['table_name']  = $this->table;
        $ret_arr['primary_key'] = $this->primaryKey;
        $ret_arr['page_type']   = $this->page_type;
        $ret_arr['slug_prefix'] = $this->slug_prefix;
        return $ret_arr;
    }

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'slug',
        'update_by',
        'created_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function scopeCustomWhereBasedData($query,$siteid=null) {
		return $query
			->where('publish', 1)
			->has('slugs')
			//->with(['slugs','sites'])
            ->with(['slugs' => function($slugQuery){
                $slugQuery->select(['id','obj_id','slug','old_slug']);
            }])->with(['sites' => function($siteQuery){
                $siteQuery->select(['id','name']);
            }])
            ->whereHas('sites', function($q) use ($siteid){
            $q->where('site_id',$siteid);
            });
	}
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function slugs() {
        return $this->hasOne(Slug::class, 'obj_id', $this->primaryKey)->where('table_name', $this->table);
    }

    public function tagBlogs()
    {
        return $this->belongsToMany(Blog::class);
    }

    public function tagProducts()
    {
        return $this->belongsToMany(Product::class);
    }

    public function sites()
    {
        return $this->belongsToMany(Site::class);
    }
}
