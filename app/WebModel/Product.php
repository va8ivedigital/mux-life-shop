<?php

namespace App\WebModel;

use App\Models\Tag;
use App\Traits\Auditable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'products';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'slug',
        'short_description',
        'long_description',
        'rating',
        'price',
        'old_price',
        'sort',
        'sale',
        'latest',
        'featured',
        'popular',
        'publish',
        'image',
        'additional_image',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public function scopeCustomWhereBasedData($query,$siteid=null) {
		return $query
			->where('publish', 1)
			->with('sites')
            ->whereHas('sites', function($q) use ($siteid){
            $q->where('site_id',$siteid);
            });
	}

    public function slugs() {
        return $this->hasOne(Slug::class, 'obj_id', $this->primaryKey)->where('table_name', $this->table);
    }

    public function sites()
    {
        return $this->belongsToMany(Site::class);
    }

    public function product_categories()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
