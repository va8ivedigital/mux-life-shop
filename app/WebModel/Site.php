<?php

namespace App\WebModel;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'sites';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'url',
        'name',
        'twitter',
        'youtube',
        'publish',
        'facebook',
        'instagram',
        'pinterest',
        'html_tags',
        'linked_in',
        'created_at',
        'updated_at',
        'deleted_at',
        'country_name',
        'country_code',
        'language_code',
        'javascript_tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'site_flag',
        'site_favicon',
        'site_logo',
        'top_header_text',
        'home_banner_image',
    ];

    public function siteCategories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function sitePages()
    {
        return $this->belongsToMany(Page::class);
    }

    public function siteTags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function siteProductCategories()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function siteBanners()
    {
        return $this->belongsToMany(Banner::class);
    }

    public function siteNetworks()
    {
        return $this->belongsToMany(Network::class);
    }

    public function siteBlogs()
    {
        return $this->belongsToMany(Blog::class);
    }

    public function siteProducts()
    {
        return $this->belongsToMany(Product::class);
    }
}
