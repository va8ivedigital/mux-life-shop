<?php

namespace App\WebModel;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'teams';

    protected $primaryKey   = 'id';
    protected $slug_prefix  = 'team/';
    protected $page_type    = 'teams';


    public function getBasicData()
    {
        $ret_arr = array();
        $ret_arr['table_name']  = $this->table;
        $ret_arr['primary_key'] = $this->primaryKey;
        $ret_arr['page_type']   = $this->page_type;
        $ret_arr['slug_prefix'] = $this->slug_prefix;
        return $ret_arr;
    }

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'designation',
        'member_image',
        'facebook_url',
        'twitter_url',
        'instagram_url',
        'publish',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
