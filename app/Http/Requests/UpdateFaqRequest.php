<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFaqRequest extends FormRequest
{
    // public function authorize()
    // {
    //     abort_if(Gate::denies('blog_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

    //     return true;
    // }

    public function rules()
    {
        return [
            // 'sites.*'           => [
            //     'integer',
            // ],
            // 'sites'             => [
            //     'required',
            //     'array',
            // ],
            'title'             => [
                'required',
            ],
            'description'       => [
                'required',
            ],
            // 'slug'              => [
            //     'required',
            // ],
            // 'short_description' => [
            //     'required',
            // ],
            // 'long_description'  => [
            //     'required',
            // ],
            'sort'              => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ]
        ];
    }
}
