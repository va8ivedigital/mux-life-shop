<?php

namespace App\Http\Requests;

use App\Models\ProductCategory;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreProductCategoryRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('product_category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'              => [
                'required',
            ],
            'slug'              => [
                'required',
            ],
            'meta_title'        => [
                'max:150',
                'required',
            ],
            'meta_keywords'     => [
                'max:200',
                'required',
            ],
            'meta_description'  => [
                'max:500',
                'required',
            ],
            /*
            'description'       => [
                'required',
            ],
            'about_description' => [
                'required',
            ],
            'sort'              => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'image'             => [
                'required',
            ],*/
        ];
    }
}
