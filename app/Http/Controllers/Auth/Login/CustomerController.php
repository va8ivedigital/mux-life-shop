<?php

namespace App\Http\Controllers\Auth\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CustomerController extends DefaultLoginController
{
    protected $redirectTo = '/customer/my-account';

    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }
    public function showLoginForm()
    {
        $data = [];
        try{
            $data['pageCss']   = 'login_process';
            return view('auth.login.customer')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
    // public function username()
    // {
    //     return 'id';
    // }


    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            Session::put("SITE_ID", 1);
            Session::put("IS_CUSTOMER", 1);
            // return Redirect::to('/my-account'); <= UNCOMMENT IT BACK
            return Redirect::to('/customer/my-account');
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        Session::flush();

        return $this->loggedOut($request) ?: redirect('/customer/login');
    }

    protected function guard()
    {
        return Auth::guard('customer');
    }
}
