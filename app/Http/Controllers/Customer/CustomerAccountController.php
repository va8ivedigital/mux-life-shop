<?php
namespace App\Http\Controllers\Customer;
use App\Http\Controllers\Controller;
use App\Models\BillingAddress;
use App\Models\Customer;
use App\Models\ShippingAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer');
    }
    public function index(Request $request)
    {
        $data = [];
        try{
            $data['pageCss'] = 'my_profile';
            $data['allloggedinn'] = $request->session()->all();
            return view('web.customer.my-account')->with($data);
        } catch (\Exception $e) {
            abort(404);
        }
    }

    public function billingAddressPage(){
        $data = [];
        try{
            $data['pageCss']   = 'my_profile';
            return view('web.customer.billing-address')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function addBillingAddress(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();

            //Add new billing address
            $BillingAddress                 = new BillingAddress();
            $BillingAddress->customer_id    = Auth::guard('customer')->user()->id;
            $BillingAddress->first_name     = $data['first_name'];
            $BillingAddress->last_name      = $data['last_name'];
            $BillingAddress->company        = $data['company_name'];
            $BillingAddress->phone_number   = $data['phone_number'];
            $BillingAddress->address_1      = $data['address_1'];
            $BillingAddress->address_2      = $data['address_2'];
            $BillingAddress->city           = $data['city'];
            $BillingAddress->zip_code       = $data['zip_code'];
            $BillingAddress->state          = $data['state'];
            $BillingAddress->country        = $data['country'];
            $BillingAddress->save();

            return redirect()->back()->with('address_added_successfully', 'New Billing Address has been added successfully!');
        }
    }

    public function shippingAddressPage(){
        $data = [];
        try{
            $data['pageCss']   = 'my_profile';
            return view('web.customer.shipping-address')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function addShippingAddress(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();

            //Add new shipping address
            $ShippingAddress                 = new ShippingAddress();
            $ShippingAddress->customer_id    = Auth::guard('customer')->user()->id;
            $ShippingAddress->first_name     = $data['first_name'];
            $ShippingAddress->last_name      = $data['last_name'];
            $ShippingAddress->company        = $data['company_name'];
            $ShippingAddress->phone_number   = $data['phone_number'];
            $ShippingAddress->address_1      = $data['address_1'];
            $ShippingAddress->address_2      = $data['address_2'];
            $ShippingAddress->city           = $data['city'];
            $ShippingAddress->zip_code       = $data['zip_code'];
            $ShippingAddress->state          = $data['state'];
            $ShippingAddress->country        = $data['country'];
            $ShippingAddress->save();

            return redirect()->back()->with('address_added_successfully', 'New Shipping Address has been added successfully!');
        }
    }

    public function manageAddresses(){
        $data = [];
        try{
            $data['pageCss']   = 'my_profile';
            $customer_id = Auth::guard('customer')->user()->id;
            $data['billingAddress']  = BillingAddress::where('customer_id', $customer_id)->with('customer')->get()->toArray();
            $data['shippingAddress'] = ShippingAddress::where('customer_id', $customer_id)->with('customer')->get()->toArray();
            return view('web.customer.manage-addresses')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function assignShippingAddress(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $ship_id = $data['shipping_address_id'];
            $customer_id = Auth::guard('customer')->user()->id;
            Customer::where('id', $customer_id)->update(['shipping_address' => $ship_id]);
            return redirect()->back()->with('address_assigned_successfully', 'Shipping Address assigned successfully!');
        }
    }

    public function assignBillingAddress(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $billing_id = $data['billing_address_id'];
            $customer_id = Auth::guard('customer')->user()->id;
            Customer::where('id', $customer_id)->update(['billing_address' => $billing_id]);
            return redirect()->back()->with('address_assigned_successfully', 'Billing Address assigned successfully!');
        }
    }

}
