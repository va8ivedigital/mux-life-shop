<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Slug;
use App\Models\Site;
use App\Models\Tag;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyBlogRequest;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use Illuminate\Support\Facades\DB;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function __construct() {
        $this->slug = new Slug;
    }

    protected $table   = 'blogs';
    protected $primaryKey   = 'id';
    protected $slug_prefix  = 'blog/';
    protected $page_type    = 'categories';

    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('blog_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $blogs = DB::table('blogs')->where('deleted_at',NULL)->get();
        return view('admin.blogs.index', compact('blogs'));
    }

    public function create(Request $request)
    {
        abort_if(Gate::denies('blog_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $users = User::all()->pluck('name', 'id');

        $sites = Site::all()->pluck('name', 'id');

        $categories = Category::all()->pluck('title', 'id');

        $tags = Tag::all()->pluck('title', 'id');

        return view('admin.blogs.create', compact('sites', 'categories', 'users', 'tags'));
    }

    public function store(StoreBlogRequest $request)
    {
        $blog = Blog::create($request->all());
        $blog->sites()->sync($request->input('sites', []));
        $blog->categories()->sync($request->input('categories', []));
        $blog->tags()->sync($request->input('tags', []));

        $last_id    = $blog->id;
        $slug       = $blog->slug;

        $return = $this->slug->insertSlug($last_id, $this->slug_prefix . $slug, $this->table, $request->input('sites', []));
        if (isset($return['status']) && $return['status'] === false) {
            return $return;
        }

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                $blog->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                    'ACL' => 'public-read'
                ])->toMediaCollection('image','s3');
            }
        } else {
            if ($request->input('image', false)) {
                $blog->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        }

        $blogUpdate = Blog::select('id','title','blog_image')->where('id',$last_id)->first();
        $img = $blogUpdate['image'] ? $blogUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['blog_image'] = $img;
        Blog::where('id', $last_id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.blogs.index') . $request->test_id;
        } else {
            $url = route('admin.blogs.index');
        }

        return redirect($url);
    }

    public function edit(Blog $blog)
    {
        abort_if(Gate::denies('blog_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $sites = Site::all()->pluck('name', 'id');

        $categories = Category::all()->pluck('title', 'id');

        $users = User::all()->pluck('name', 'id');

        $tags = Tag::all()->pluck('title', 'id');

        $blog->load('sites', 'categories', 'tags','user');

        return view('admin.blogs.edit', compact('sites', 'categories', 'tags', 'blog', 'users'));
    }

    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        $blog->update($request->all());
        $blog->sites()->sync($request->input('sites', []));
        $blog->categories()->sync($request->input('categories', []));
        $blog->tags()->sync($request->input('tags', []));

        $last_id    = $blog->id;
        $slug       = $blog->slug;

        $return = $this->slug->updateSlug($last_id, $this->slug_prefix . $slug, $this->table, $request->input('sites', []));

        if (isset($return['status']) && $return['status'] === false) {
            return $return;
        }

        // if ($request->input('image', false)) {
        //     if (!$blog->image || $request->input('image') !== $blog->image->file_name) {
        //         $blog->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
        //             'ACL' => 'public-read'
        //         ])->toMediaCollection('image','s3');
        //     }
        // } elseif ($blog->image) {
        //     $blog->image->delete();
        // }

        // if ($request->input('banner_image', false)) {
        //     if (!$blog->banner_image || $request->input('banner_image') !== $blog->banner_image->file_name) {
        //         $blog->addMedia(storage_path('tmp/uploads/' . $request->input('banner_image')))->addCustomHeaders([
        //             'ACL' => 'public-read'
        //         ])->toMediaCollection('banner_image','s3');
        //     }
        // } elseif ($blog->banner_image) {
        //     $blog->banner_image->delete();
        // }

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                if (!$blog->image || $request->input('image') !== $blog->image->file_name) {
                    $blog->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                        'ACL' => 'public-read'
                    ])->toMediaCollection('image','s3');
                }
            } elseif ($blog->image) {
                $blog->image->delete();
            }
        } else {
            if ($request->input('image', false)) {
                if (!$blog->image || $request->input('image') !== $blog->image->file_name) {
                    $blog->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
                }
            } elseif ($blog->image) {
                $blog->image->delete();
            }
        }

        $blogUpdate = Blog::select('id','title','blog_image')->where('id',$last_id)->first();
        $img = $blogUpdate['image'] ? $blogUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['blog_image'] = $img;
        Blog::where('id', $last_id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.blogs.index') . $request->test_id;
        } else {
            $url = route('admin.blogs.index');
        }

        return redirect($url);

    }

    public function show(Blog $blog)
    {
        abort_if(Gate::denies('blog_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $blog->load('sites', 'categories', 'tags');

        return view('admin.blogs.show', compact('blog'));
    }

    public function destroy(Blog $blog)
    {
        abort_if(Gate::denies('blog_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $blog->delete();
        $this->slug->deleteSlug($blog->id, $this->table);
        return back();
    }

    public function massDestroy(MassDestroyBlogRequest $request)
    {
        Blog::whereIn('id', request('ids'))->delete();
        $this->slug->massdeleteSlug(request('ids'), $this->table);
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
