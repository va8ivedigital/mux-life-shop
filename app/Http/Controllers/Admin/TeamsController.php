<?php

namespace App\Http\Controllers\Admin;

use App\Models\Team;
use App\Models\Site;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyTeamRequest;
use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TeamsController extends Controller
{
    protected $table   = 'teams';
    protected $primaryKey   = 'id';
    protected $slug_prefix  = 'team/';

    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('team_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $teams = DB::table('teams')->where('deleted_at',NULL)->get();
        return view('admin.teams.index', compact('teams'));
    }

    public function create(Request $request)
    {
        abort_if(Gate::denies('team_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        return view('admin.teams.create');
    }

    public function store(StoreTeamRequest $request)
    {
        $team = Team::create($request->all());

        $last_id    = $team->id;

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                $team->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                    'ACL' => 'public-read'
                ])->toMediaCollection('image','s3');
            }
        } else {
            if ($request->input('image', false)) {
                $team->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
            }
        }

        $teamUpdate = Team::select('id','title','member_image')->where('id',$last_id)->first();
        $img = $teamUpdate['image'] ? $teamUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['member_image'] = $img;
        Team::where('id', $last_id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.teams.index') . $request->test_id;
        } else {
            $url = route('admin.teams.index');
        }

        return redirect($url);
    }

    public function edit(Team $team)
    {
        abort_if(Gate::denies('team_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        return view('admin.teams.edit', compact('team'));
    }

    public function update(UpdateTeamRequest $request, Team $team)
    {
        $team->update($request->all());

        $last_id    = $team->id;
        // $slug       = $team->slug;

        // $return = $this->slug->updateSlug($last_id, $this->slug_prefix . $slug, $this->table, $request->input('sites', []));

        // if (isset($return['status']) && $return['status'] === false) {
        //     return $return;
        // }

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                if (!$team->image || $request->input('image') !== $team->image->file_name) {
                    $team->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                        'ACL' => 'public-read'
                    ])->toMediaCollection('image','s3');
                }
            } elseif ($team->image) {
                $team->image->delete();
            }
        } else {
            if ($request->input('image', false)) {
                if (!$team->image || $request->input('image') !== $team->image->file_name) {
                    $team->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->toMediaCollection('image');
                }
            } elseif ($team->image) {
                $team->image->delete();
            }
        }

        $teamUpdate = Team::select('id','title','member_image')->where('id',$last_id)->first();
        $img = $teamUpdate['image'] ? $teamUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['member_image'] = $img;
        Team::where('id', $last_id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.teams.index') . $request->test_id;
        } else {
            $url = route('admin.teams.index');
        }

        return redirect($url);
    }

    public function show(Team $team)
    {
        abort_if(Gate::denies('team_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        return view('admin.teams.show', compact('team'));
    }

    public function destroy(Team $team)
    {
        abort_if(Gate::denies('team_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $team->delete();
        return back();
    }

    public function massDestroy(MassDestroyTeamRequest $request)
    {
        Team::whereIn('id', request('ids'))->delete();
        // $this->slug->massdeleteSlug(request('ids'), $this->table);
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
