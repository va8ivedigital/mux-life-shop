<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyFaqRequest;
use App\Http\Requests\StoreFaqRequest;
use App\Http\Requests\UpdateFaqRequest;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class FaqController extends Controller
{
    use MediaUploadingTrait;

        public function index(Request $request)
        {
            abort_if(Gate::denies('banner_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
            if($status) return redirect('/admin');

            // $banners = Banner::with('sites');

            // if(isset(request()->bid)) {
            //     $banners = $banners->where('id', request()->bid);
            // }
            // $banners = $banners->whereHas('sites', function($q) use($request) {
                //     if($request->siteId != 'all') {
                //         if(!empty($request->siteId)) {
                //             $q->where('site_id', $request->siteId);
                //         } elseif (isset(request()->test_id)) {
                //             $q->where('site_id', request()->test_id);
                //         } else {
                //             $q->where('site_id', getSiteID('SITE_ID'));
                //         }
                //     }
            // })->get();

            // return view('admin.banners.index', compact('banners'));

            $faqs = DB::table('faqs')->where('deleted_at',NULL)->get();
            return view('admin.faqs.index', compact('faqs'));
        }


    public function create(Request $request)
    {
        abort_if(Gate::denies('banner_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0  : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // return view('admin.banners.create', compact('sites'));
        return view('admin.faqs.create');
    }

    public function store(StoreFaqRequest $request)
    {
        Faq::create($request->all());

        if(isset($request->test_id)) {
            $url = route('admin.faqs.index') . $request->test_id;
        } else {
            $url = route('admin.faqs.index');
        }
        return redirect($url);
    }

    public function edit(Faq $faq)
    {
        abort_if(Gate::denies('banner_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        // if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // $banner->load('sites');

        return view('admin.faqs.edit', compact('faq'));
    }

    public function update(UpdateFaqRequest $request, Faq $faq)
    {
        $faq->update($request->all());

        if(isset($request->test_id)) {
            $url = route('admin.faqs.index') . $request->test_id;
        } else {
            $url = route('admin.faqs.index');
        }

        return redirect($url);
    }

    public function show(Request $request, Faq $faq)
    {
        abort_if(Gate::denies('banner_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        //$faq->load('sites');

        return view('admin.faqs.show', compact('faq'));
    }

    public function destroy(Request $request, Faq $faq)
    {
        abort_if(Gate::denies('banner_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 :!getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $faq->delete();

        return back();
    }

    public function massDestroy(MassDestroyFaqRequest $request)
    {
        Faq::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
