<?php

namespace App\Http\Controllers\Admin;

use App\Models\About;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyAboutRequest;
use App\Http\Requests\StoreAboutRequest;
use App\Http\Requests\UpdateAboutRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class AboutController extends Controller
{
    use MediaUploadingTrait;

        public function index()
        {
            abort_if(Gate::denies('banner_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
            // if($status) return redirect('/admin');

            $about = DB::table('about')->where('deleted_at',NULL)->get();
            return view('admin.about.index', compact('about'));
        }


    public function create(Request $request)
    {
        abort_if(Gate::denies('banner_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        // if($status) return redirect('/admin');

        return view('admin.about.create');
    }

    public function store(StoreAboutRequest $request)
    {
        $client = About::create($request->all());
        // $banner->sites()->sync($request->input('sites', []));
        $id    = $client->id;

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                $client->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                    'ACL' => 'public-read'
                ])->toMediaCollection('image','s3');
            }
        } else {
            if ($request->input('image', false)) {
                $client->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                    'ACL' => 'public-read'
                ])->toMediaCollection('image');
            }
        }

        $aboutUpdate = About::select('id','title','image')->where('id',$id)->first();
        $img = $aboutUpdate['image'] ? $aboutUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['image'] = $img;
        About::where('id', $id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.about.index') . $request->test_id;
        } else {
            $url = route('admin.about.index');
        }

        return redirect($url);
    }

    public function edit(About $about)
    {
        abort_if(Gate::denies('banner_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        // if($status) return redirect('/admin');
        return view('admin.about.edit', compact('about'));
    }

    public function update(UpdateAboutRequest $request, About $about)
    {
        $about->update($request->all());
        // $about->sites()->sync($request->input('sites', []));
        $id    = $about->id;

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                if (!$about->image || $request->input('image') !== $about->image->file_name) {
                    $about->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                        'ACL' => 'public-read'
                    ])->toMediaCollection('image','s3');
                }
            } elseif ($about->image) {
                $about->image->delete();
            }
        } else {
            if ($request->input('image', false)) {
                if (!$about->image || $request->input('image') !== $about->image->file_name) {
                    $about->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                        'ACL' => 'public-read'
                    ])->toMediaCollection('image');
                }
            } elseif ($about->image) {
                $about->image->delete();
            }
        }

        $aboutUpdate = About::select('id','title','image')->where('id',$id)->first();
        $img = $aboutUpdate['image'] ? $aboutUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['image'] = $img;
        About::where('id', $id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.about.index') . $request->test_id;
        } else {
            $url = route('admin.about.index');
        }

        return redirect($url);
    }

    public function show(Request $request, About $about)
    {
        abort_if(Gate::denies('banner_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        //$client->load('sites');

        return view('admin.about.show', compact('about'));
    }

    public function destroy(Request $request, About $about)
    {
        abort_if(Gate::denies('banner_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 :!getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $about->delete();

        return back();
    }

    public function massDestroy(MassDestroyAboutRequest $request)
    {
        About::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
