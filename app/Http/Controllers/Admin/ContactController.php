<?php
namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyContactRequest;
use App\Http\Requests\StoreContactRequest;
use App\Http\Requests\UpdateContactRequest;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{

    public function index(Request $request)
    {
            abort_if(Gate::denies('banner_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
            if($status) return redirect('/admin');

            // $banners = Banner::with('sites');

            // if(isset(request()->bid)) {
                //     $banners = $banners->where('id', request()->bid);
            // }
            // $banners = $banners->whereHas('sites', function($q) use($request) {
                //     if($request->siteId != 'all') {
                //         if(!empty($request->siteId)) {
                //             $q->where('site_id', $request->siteId);
                //         } elseif (isset(request()->test_id)) {
                //             $q->where('site_id', request()->test_id);
                //         } else {
                //             $q->where('site_id', getSiteID('SITE_ID'));
                //         }
                //     }
            // })->get();

            // return view('admin.banners.index', compact('banners'));

            $contacts = DB::table('contact_us')->where('deleted_at',NULL)->get();
            return view('admin.contacts.index', compact('contacts'));
    }


    public function create(Request $request)
    {
        abort_if(Gate::denies('banner_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0  : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // return view('admin.banners.create', compact('sites'));
        return view('admin.contacts.create');
    }

    public function store(StoreContactRequest $request)
    {
        Contact::create($request->all());

        if(isset($request->test_id)) {
            $url = route('admin.contacts.index') . $request->test_id;
        } else {
            $url = route('admin.contacts.index');
        }
        return redirect($url);
    }

    public function edit(Contact $contact)
    {
        abort_if(Gate::denies('banner_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        // if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // $banner->load('sites');

        return view('admin.contacts.edit', compact('contact'));
    }

    public function update(UpdateContactRequest $request, Contact $contact)
    {
        $contact->update($request->all());

        if(isset($request->test_id)) {
            $url = route('admin.contacts.index') . $request->test_id;
        } else {
            $url = route('admin.contacts.index');
        }

        return redirect($url);
    }

    public function show(Request $request, Contact $contact)
    {
        abort_if(Gate::denies('banner_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        //$faq->load('sites');

        return view('admin.contacts.show', compact('contact'));
    }

    public function destroy(Request $request, Contact $contact)
    {
        abort_if(Gate::denies('banner_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 :!getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $contact->delete();

        return back();
    }

    public function massDestroy(MassDestroyContactRequest $request)
    {
        Contact::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
