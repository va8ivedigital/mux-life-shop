<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyClientRequest;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\UpdateClientRequest;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class ClientController extends Controller
{
    use MediaUploadingTrait;

        public function index()
        {
            abort_if(Gate::denies('banner_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
            // if($status) return redirect('/admin');

            // $banners = Banner::with('sites');

            // if(isset(request()->bid)) {
            //     $banners = $banners->where('id', request()->bid);
            // }
            // $banners = $banners->whereHas('sites', function($q) use($request) {
            //     if($request->siteId != 'all') {
            //         if(!empty($request->siteId)) {
            //             $q->where('site_id', $request->siteId);
            //         } elseif (isset(request()->test_id)) {
            //             $q->where('site_id', request()->test_id);
            //         } else {
            //             $q->where('site_id', getSiteID('SITE_ID'));
            //         }
            //     }
            // })->get();

            // return view('admin.banners.index', compact('banners'));

            $clients = DB::table('clients')->where('deleted_at',NULL)->get();
            return view('admin.clients.index', compact('clients'));
        }


    public function create(Request $request)
    {
        abort_if(Gate::denies('banner_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        // if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // return view('admin.banners.create', compact('sites'));
        return view('admin.clients.create');
    }

    public function store(StoreClientRequest $request)
    {
        $client = Client::create($request->all());
        // $banner->sites()->sync($request->input('sites', []));
        $id    = $client->id;

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                $client->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                    'ACL' => 'public-read'
                ])->toMediaCollection('image','s3');
            }
        } else {
            if ($request->input('image', false)) {
                $client->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                    'ACL' => 'public-read'
                ])->toMediaCollection('image');
            }
        }

        $clientUpdate = Client::select('id','title','client_logo')->where('id',$id)->first();
        $img = $clientUpdate['image'] ? $clientUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['client_logo'] = $img;
        Client::where('id', $id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.clients.index') . $request->test_id;
        } else {
            $url = route('admin.clients.index');
        }

        return redirect($url);
    }

    public function edit(Client $client)
    {
        abort_if(Gate::denies('banner_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        // if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // $banner->load('sites');

        return view('admin.clients.edit', compact('client'));
    }

    public function update(UpdateClientRequest $request, Client $client)
    {
        $client->update($request->all());
        // $client->sites()->sync($request->input('sites', []));
        $id    = $client->id;

        if (\App::environment('production')) {
            if ($request->input('image', false)) {
                if (!$client->image || $request->input('image') !== $client->image->file_name) {
                    $client->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                        'ACL' => 'public-read'
                    ])->toMediaCollection('image','s3');
                }
            } elseif ($client->image) {
                $client->image->delete();
            }
        } else {
            if ($request->input('image', false)) {
                if (!$client->image || $request->input('image') !== $client->image->file_name) {
                    $client->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
                        'ACL' => 'public-read'
                    ])->toMediaCollection('image');
                }
            } elseif ($client->image) {
                $client->image->delete();
            }
        }

        $clientUpdate = Client::select('id','title','client_logo')->where('id',$id)->first();
        $img = $clientUpdate['image'] ? $clientUpdate['image']['url'] : '';
        // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        $path['client_logo'] = $img;
        Client::where('id', $id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.clients.index') . $request->test_id;
        } else {
            $url = route('admin.clients.index');
        }

        return redirect($url);
    }

    public function show(Request $request, Client $client)
    {
        abort_if(Gate::denies('banner_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        //$client->load('sites');

        return view('admin.clients.show', compact('client'));
    }

    public function destroy(Request $request, Client $client)
    {
        abort_if(Gate::denies('banner_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 :!getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $client->delete();

        return back();
    }

    public function massDestroy(MassDestroyClientRequest $request)
    {
        Client::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
