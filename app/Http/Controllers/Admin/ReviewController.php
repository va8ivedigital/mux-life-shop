<?php

namespace App\Http\Controllers\Admin;

use App\Models\Review;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyReviewRequest;
use App\Http\Requests\StoreReviewRequest;
use App\Http\Requests\UpdateReviewRequest;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class ReviewController extends Controller
{
    use MediaUploadingTrait;

        public function index(Request $request)
        {
            abort_if(Gate::denies('banner_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
            if($status) return redirect('/admin');

            // $banners = Banner::with('sites');

            // if(isset(request()->bid)) {
            //     $banners = $banners->where('id', request()->bid);
            // }
            // $banners = $banners->whereHas('sites', function($q) use($request) {
                //     if($request->siteId != 'all') {
                //         if(!empty($request->siteId)) {
                //             $q->where('site_id', $request->siteId);
                //         } elseif (isset(request()->test_id)) {
                //             $q->where('site_id', request()->test_id);
                //         } else {
                //             $q->where('site_id', getSiteID('SITE_ID'));
                //         }
                //     }
            // })->get();

            // return view('admin.banners.index', compact('banners'));

            $reviews = DB::table('user_reviews')->where('deleted_at',NULL)->get();
            return view('admin.reviews.index', compact('reviews'));
        }


    public function create(Request $request)
    {
        abort_if(Gate::denies('banner_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0  : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // return view('admin.banners.create', compact('sites'));
        return view('admin.reviews.create');
    }

    public function store(StoreReviewRequest $request)
    {
        $review = Review::create($request->all());
        // $banner->sites()->sync($request->input('sites', []));
        // $id    = $review->id;

        // if (\App::environment('production')) {
        //     if ($request->input('image', false)) {
        //         $review->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
        //             'ACL' => 'public-read'
        //         ])->toMediaCollection('image','s3');
        //     }
        // } else {
        //     if ($request->input('image', false)) {
        //         $review->addMedia(storage_path('tmp/uploads/' . $request->input('image')))->addCustomHeaders([
        //             'ACL' => 'public-read'
        //         ])->toMediaCollection('image');
        //     }
        // }

        // $reviewUpdate = Review::select('id','title','client_logo')->where('id',$id)->first();
        // $img = $reviewUpdate['image'] ? $reviewUpdate['image']['url'] : '';
        // // $imagePath = str_replace('https://va8ive-cms.s3.amazonaws.com/', $website->cdn_path ?? '', $img);
        // $path['review_logo'] = $img;
        // Review::where('id', $id)->update($path);

        if(isset($request->test_id)) {
            $url = route('admin.reviews.index') . $request->test_id;
        } else {
            $url = route('admin.reviews.index');
        }

        return redirect($url);
    }

    public function edit(Review $review)
    {
        abort_if(Gate::denies('banner_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        // if($status) return redirect('/admin');

        // $sites = Site::all()->pluck('name', 'id');

        // $banner->load('sites');

        return view('admin.reviews.edit', compact('review'));
    }

    public function update(UpdateReviewRequest $request, Review $review)
    {
        $review->update($request->all());

        if(isset($request->test_id)) {
            $url = route('admin.reviews.index') . $request->test_id;
        } else {
            $url = route('admin.reviews.index');
        }

        return redirect($url);
    }

    public function show(Request $request, Review $review)
    {
        abort_if(Gate::denies('banner_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 : !getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        //$review->load('sites');

        return view('admin.reviews.show', compact('review'));
    }

    public function destroy(Request $request, Review $review)
    {
        abort_if(Gate::denies('banner_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $status = (isset(request()->test_id) ? !request()->test_id > 0 :!getSiteID('SITE_ID') > 0);
        if($status) return redirect('/admin');

        $review->delete();

        return back();
    }

    public function massDestroy(MassDestroyReviewRequest $request)
    {
        Review::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
