<?php

namespace App\Http\Controllers\Web;
use App\Models\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){
        $data['pageCss']   = 'home';
        $data['pageCss'] = 'contact';

        $data['contact_details']   = Contact::select('address','phone_number','description','embeded_map')->get()->toArray();
        return view('web.contact.index')->with($data);
    }
}
