<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB as DB;

class ProductSearchController extends Controller
{
    public function search(Request $request) {
        if($request->ajax()) {
            $output="";
            $products = DB::table('products')->where('title','LIKE','%'.$request->search."%")->get();
            if($products){
                foreach ($products as $key => $product) {
                    $i=0;
                    $output.= "<div class='product-card'>
                    <div class='product-card-inner'>";
                    if($product->sale != 0){
                    $output .= "<div class='sale'>SALE</div>";
                    }
                    $output .= "<div class='wishlist'>
                        <a href='javascript:;' class='view'>
                            <i class='x_eye'></i>
                        </a>
                        <a href='javascript:;' class='add-to-fav'>
                            <i class='x_favorite'></i>
                        </a>
                    </div>
                    <a class='product-image-wrap' href='javascript:;'>
                        <picture>
                            <img src='$product->image' data-src='$product->image' alt='' width='170' height='170'>
                        </picture>
                    </a>
                    <div class='product-detail'>
                        <div class='rating-cart'>
                            <div class='rating'>";
                            while($i < $product->rating){
                                $output .= "<input type='radio' name='georgina' value='$i'>
                                <label class='full RateActive'></label>";
                                $i++;
                            }
                            $output .= "</div>";
                        if(null !== session('cart')){
                            if(array_key_exists($product->id,session('cart'))){
                                $output .= "<a href='javascript:;' class='add-to-cart'>ALREADY IN CART !</a>";
                            } else {
                                $output .= "<a href='add-to-cart/$product->id' class='add-to-cart'>ADD TO CART</a>";
                            }
                        }else{
                            if(Auth::guard('customer')->user()){
                                // LOGGED IN USER WILL ADD TO CART
                                $output .= "<a href='add-to-cart/$product->id' class='add-to-cart'>ADD TO CART</a>";
                            } else {
                                // LOGIN FIRST SHOW PLEASE SIGN IN POP UP HERE
                                $output .= "<a href='javascript:;' class='add-to-cart'>ADD TO CART</a>";
                            }
                        }
                        $output.= "</div>
                        <a class='product-title' href='product/$product->slug'>$product->title</a>
                        <div class='product-pricing'>";
                        if(!empty($product->old_price) || $product->old_price !=0 || $product->old_price != '' || $product->old_price != NULL){
                            $output .= "<span class='discount'>Rs." . number_format($product->old_price, 0) . "</span>";
                        }
                        $output .= "<span>Rs." . number_format($product->price, 0) . "</span>";

                    $output.= "</div>
                            </div>
                        </div>
                    </div>";
                }
                return response($output);
            }
        }
    }
}
