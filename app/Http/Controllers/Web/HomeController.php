<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\WebModel\Banner;
use App\WebModel\Client;
use App\Models\Review;
use App\WebModel\Blog;
use App\WebModel\ProductCategory;
use App\WebModel\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request) {
        $data = [];
        try{
            $siteid = config('app.siteid');
            $data['pageCss']   = 'home';
            $data['banners']   = Banner::select('id','title','link','banner_image')->orderBy('sort', 'asc')->get()->toArray();
            $data['clients']   = Client::select('id','title','client_logo')->orderBy('sort', 'asc')->get()->toArray();
            $data['reviews']   = Review::select('id','title','user_review')->orderBy('sort', 'asc')->get()->toArray();
            $data['singleRandomPopularBlog'] = Blog::select('id','title','slug','blog_image','created_at')->where('popular',1)->orderBy('sort', 'asc')->get()->random(1)->toArray();
            $data['popularBlogs'] = Blog::select('id','title','slug','blog_image','created_at')->where('popular',1)->orderBy('sort', 'asc')->get()->take(2)->toArray();

            //Featured Products
            $data['featuredProducts'] = Product::select('id','slug','title','image','sale','price','old_price')->where('featured',1)->get()->toArray();

            //Smart Switches
            $data['fetchProductCategories'] = ProductCategory::select('id','slug','name','product_category_image')->where('deleted_at','=',NULL)->orderBy('sort','asc')->get()->take(3)->toArray();

            //Smart Switches
            $data['fetchsmartSwitchCategory'] = Product::with('product_categories')->whereHas('product_categories', function ($query) {
                $query->where('slug', '=', 'smart-switch');
            })->get();

            //Smart Locks
            $data['fetchsmartLockCategory'] = Product::with('product_categories')->whereHas('product_categories', function ($query) {
                $query->where('slug', '=', 'smart-locks');
            })->get();
            return view('web.home.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
    public function _404() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss'] = '404_error';
            return view('web.home.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
}
