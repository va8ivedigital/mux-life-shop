<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\WebModel\Team;
use App\WebModel\About;

class AboutController extends Controller
{
    public function index() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss']   = 'about_us';

            //Team Members
            $data['team_members']  = Team::select('id','title','designation','member_image','facebook_url','twitter_url','instagram_url')->get()->toArray();
            $data['about_sections'] = About::select('id','title','description','image')->get()->toArray();

            return view('web.about.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
    public function _404() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss'] = '404_error';
            return view('web.home.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
}
