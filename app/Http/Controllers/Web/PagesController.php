<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\WebModel\Slug;
use App\WebModel\Page;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class PagesController extends Controller
{
    public function detail($slug){
    	$data = [];
        try{
            $data['pageCss'] = 'content_pages';
            $data['pageCss']   = 'home';
            $data['pageRecord'] = Page::where('slug', '=', $slug)->first();
            if($data['pageRecord']) $data['pageRecord']=$data['pageRecord']->toArray(); else abort(404);
            $meta['title']=$data['pageRecord']['meta_title'];
            $meta['keywords']=$data['pageRecord']['meta_keywords'];
            $meta['description']=$data['pageRecord']['meta_description'];
            $data['meta']=$meta;

            return view('web.pages.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
}
