<?php

namespace App\Http\Controllers\Web;
use App\WebModel\Product;
use App\WebModel\ProductCategory;
use App\Http\Controllers\Controller;

class SalesController extends Controller
{
    public function index() {
        $data = [];
        try{
            $data['pageCss']            = 'home';
            $data['pageCss']            = 'product_listing';
            $data['allSaleProducts']    = Product::select('id','title','slug','image','sale','price','old_price','rating')->where('sale',1)->paginate(15);
            $data['productsList']       = Product::select('id','title','slug','image','sale','price','old_price','rating')->orderBy('sort', 'asc')->take(1)->get()->toArray();
            $data['productsCategory']   = ProductCategory::select('name','slug')->orderBy('sort', 'desc')->take(10)->get()->toArray();
            $data['latestProducts']     = Product::select('id','title','slug','price','image','rating')->orderBy('id', 'desc')->take(3)->get()->toArray();
            // $data['productTags']     = Product::select('id')->with('tags')->get()->toArray();

            return view('web.sale.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function _404() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss'] = '404_error';
            return view('web.home.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
}
