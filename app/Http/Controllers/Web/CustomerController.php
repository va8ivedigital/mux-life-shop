<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Session;
use Auth;

class CustomerController extends Controller
{

    public function registerPage(){
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss']   = 'login_process';
            // $data['faqs']   = Faq::select('id','title','description')->orderBy('sort', 'asc')->get()->toArray();

            return view('web.customer.register')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function registerForm(Request $request){
        $customCSS      = [];
        $customer_data  = [];
        if($request->isMethod('post')){
            $data = $request->all();
            // echo "<pre>"; print_r($data); die();

            //Check if customer already exists
            $customerCount = Customer::where('email', $data['email'])->count();
            if($customerCount>0){
                $message = "Customer already exists!";
                session::flash('error_message', $message);
                return redirect()->back();
            } else {
                //Add new customer
                $dob = $data['day']."-".$data['month']."-".$data['year'];
                $customer_data['customer_name'] = $data['first_name']." ".$data['last_name'];
                $customer_data['customer_email'] = $data['email'];
                $customer = new Customer;
                $customer->name = $data['first_name']." ".$data['last_name'];
                $customer->first_name = $data['first_name'];
                $customer->last_name = $data['last_name'];
                $customer->email = $data['email'];
                $customer->dob = $dob;
                $customer->password = bcrypt($data['password']);
                $customer->save();
                // $message = "Customer registered successfully..!";
                // session::flash('success_message', $message);
                // if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']])){
                    // echo "<pre>"; print_r(Auth::customer()); die();
                // }
                $customCSS['pageCss']   = 'login_process';
                return view('web.customer.thank-you')->with($customCSS)->with($customer_data);
            }
        }
    }

}
