<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\BillingAddress;
use App\Models\Cart;
use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    function checkout(){
        $data = [];
        try {
            $data['pageCss']   = 'home';
            $data['pageCss'] = 'checkout';
            $customer_id = Auth::guard('customer')->user()->id;
            $data['getCustomer'] = Customer::where('id', $customer_id)->get()->toArray();
            $billing_address_id = $data['getCustomer'][0]['billing_address'];
            $data['billingAddress']  = BillingAddress::where('id', $billing_address_id)->with('customer')->get()->toArray();
            return view('web.customer.checkout')->with($data);
        } catch (\Exception $e) {
            return redirect('/customer/login')->with('from_cart_when_not_loggedin', 'Please login to add items in cart..!');
        }
    }

    function processCheckout(Request $request){
        if($request->isMethod('post')){
            $customer_id = Auth::guard('customer')->user()->id;
            $createOrder = new Order();

            //Combining address into an array
            $address_array = [
                "first_name"    => $request->first_name,
                "last_name"     => $request->last_name,
                "company"       => $request->company,
                "country"       => $request->country,
                "city"          => $request->city,
                "zip_code"      => $request->zip_code,
                "phone_number"  => $request->phone_number,
                "email"         => $request->email,
            ];
            $address = json_encode($address_array);

            //Saving into orders table
            $createOrder->customer_id     = $customer_id;
            $createOrder->billing_address = $address;
            $createOrder->order_items     = $request->order_items;
            $createOrder->order_total     = $request->order_total;
            $createOrder->payment_mode    = $request->payment_method;
            $createOrder->order_notes     = $request->order_notes;
            $createOrder->status          = "Pending";
            $createOrder->save();

            //Remove from cart as well
            Cart::where('customer_id', $customer_id)->delete();
            $request->session()->forget('cart');

            return redirect('/customer/my-account')->with('order_created', 'Order created successfully!');
        }
    }
}
