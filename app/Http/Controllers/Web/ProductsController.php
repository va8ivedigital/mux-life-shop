<?php

namespace App\Http\Controllers\Web;
use App\WebModel\Product;
use App\Models\Tag;
use App\WebModel\ProductCategory;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function index() {
        $data = [];
        try{
            $data['pageCss']            = 'home';
            $data['pageCss']            = 'product_listing';
            $data['allProducts']        = Product::paginate(15);
            $data['productsList']       = Product::select('id','title','slug','image','sale','price','old_price','rating')->orderBy('sort', 'asc')->take(1)->get()->toArray();
            $data['productsCategory']   = ProductCategory::select('name','slug')->orderBy('sort', 'desc')->take(10)->get()->toArray();
            $data['latestProducts']     = Product::select('id','title','slug','price','image','rating')->orderBy('id', 'desc')->take(3)->get()->toArray();
            $data['productTags']        = Product::with('tags')->get();

            return view('web.product.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function detail() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            // $data['details']   = Banner::select('id','title','link','banner_image')->orderBy('sort', 'asc')->get()->toArray();

            return view('web.product.detail')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function categoryWiseProducts($slug){
        $data['pageCss']   = 'home';
        $data['pageCss'] = "product_listing";

        $data['productsList']       = Product::select('id','title','slug','image','sale','price','old_price','rating')->orderBy('sort', 'asc')->take(1)->get()->toArray();
        $data['productsCategory']   = ProductCategory::select('name','slug')->orderBy('sort', 'desc')->take(10)->get()->toArray();
        $data['latestProducts']     = Product::select('id','title','slug','price','image','rating')->orderBy('id', 'desc')->take(3)->get()->toArray();
        $data['productTags']        = Product::with('tags')->get();

        $categoryProducts = ProductCategory::select('id','name')->with(['products'=> function($q1){
            $q1->with(['slugs','product_categories']);
        }])->where('slug', '=', $slug)->first();

        if(empty($categoryProducts)){
            abort(404);
        }

        $data['category_wise_products'] = $categoryProducts->toArray();
        return view('web.product.product_category')->with($data);
    }

    public function tagWiseProducts($slug){
        $data['pageCss']   = 'home';
        $data['pageCss'] = "product_listing";

        $data['productsList']       = Product::select('id','title','slug','image','sale','price','old_price','rating')->orderBy('sort', 'asc')->take(1)->get()->toArray();
        $data['productsCategory']   = ProductCategory::select('name','slug')->orderBy('sort', 'desc')->take(10)->get()->toArray();
        $data['latestProducts']     = Product::select('id','title','slug','price','image','rating')->orderBy('id', 'desc')->take(3)->get()->toArray();
        $data['productTags']        = Product::with('tags')->get();

        $tagProducts = Tag::select('id','title')->with(['products'=> function($q1){
            $q1->with(['slugs','tags'])->select('id','title','slug','image','sale','price','old_price','rating');
        }])->where('slug', '=', $slug)->first();

        if(empty($tagProducts)){
            abort(404);
        }

        $data['tag_wise_products'] = $tagProducts->toArray();
        return view('web.product.product_tag')->with($data);
    }

    /*
        function addToCart(Request $request){

            // $data['abc'] = $request->session()->all();
            $cart = new Cart;
            if(isset(Auth::guard('customer')->user()->id)){
                $cart->customer_id = Auth::guard('customer')->user()->id;
                $cart->product_id = $request->product_id;
                $cart->save();

                $message = "Product has been added to cart Successfully!";
                session::flash('added_to_cart_message', $message);
                return redirect()->back();
            }else{
                return redirect('/customer/login');
            }
        }
    */

    public function cart()
    {
        $data = [];
        // $cart = null;
        try{
            $data['pageCss']   = 'home';
            $data['pageCss']   = 'cart';
            $customer_id = Auth::guard('customer')->user()->id;
            $cart_items = Cart::select('id','customer_id','products')->where('customer_id',$customer_id)->get()->toArray();
            if($cart_items){
                // $cart = session()->get('cart');

                // if(!isset($cart)):
                    $cart_products = json_decode($cart_items[0]['products'], true);
                    foreach($cart_products as $item_id => $cart_product){
                        $product = Product::find($item_id);
                        $cart[$item_id] = [
                            "name" => $product['title'],
                            "quantity" => $cart_product['quantity'],
                            "price" => $product['price'],
                            "photo" => $product['image'],
                        ];
                    }
                    session()->put('cart', $cart);
                // endif;
            }

            return view('web.customer.cart')->with($data);
        }catch (\Exception $e) {
            return redirect('/customer/login')->with('from_cart_when_not_loggedin', 'Please login to add items in cart..!');
        }

    }

    public function addToCart($id)
    {
        $product = Product::find($id);

        if(!$product) {

            abort(404);

        }

        $cart = session()->get('cart');

        $cartDB = new Cart;

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                    $id => [
                        "name" => $product->title,
                        "quantity" => 1,
                        "price" => $product->price,
                        "photo" => $product->image
                    ]
            ];

            session()->put('cart', $cart);
            $cartDB->customer_id = Auth::guard('customer')->user()->id;
            $cartDB->products = json_encode($cart);
            $cartDB->save();

            // Add also in Cart table
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {

            $cart[$id]['quantity']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');

        }

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $product->title,
            "quantity" => 1,
            "price" => $product->price,
            "photo" => $product->image
        ];

        session()->put('cart', $cart);

        $customer_id = Auth::guard('customer')->user()->id;
        $cart_products = json_encode($cart);

        DB::update('UPDATE `cart` SET `products` = ? WHERE `customer_id` = ?',[$cart_products,$customer_id]);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if($request->id && $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            $customer_id = Auth::guard('customer')->user()->id;
            $cart_products = json_encode($cart);

            DB::update('UPDATE `cart` SET `products` = ? WHERE `customer_id` = ?',[$cart_products,$customer_id]);

            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);

                if(count(session('cart')) < 1){
                    $customer_id = Auth::guard('customer')->user()->id;
                    DB::table('cart')->where('customer_id', '=', $customer_id)->delete();
                } else {
                    $customer_id = Auth::guard('customer')->user()->id;
                    $cart_products = json_encode($cart);
                    DB::update('UPDATE `cart` SET `products` = ? WHERE `customer_id` = ?',[$cart_products,$customer_id]);
                }

            }

            session()->flash('success', 'Product removed successfully');
        }
    }

    public function _404() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss'] = '404_error';
            return view('web.home.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
}
