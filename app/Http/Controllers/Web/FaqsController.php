<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Models\Faq;

class FaqsController extends Controller
{
    public function index() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss']   = 'faqs';
            $data['faqs']   = Faq::select('id','title','description')->orderBy('sort', 'asc')->get()->toArray();

            return view('web.faq.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
    public function _404() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss'] = '404_error';
            return view('web.home.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }
}
