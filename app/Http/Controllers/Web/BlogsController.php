<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\WebModel\Category;
use App\WebModel\Page;
use App\WebModel\Blog;
use App\WebModel\Product;
use App\SiteSetting;
use App\WebModel\Slug;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
class BlogsController extends Controller {
    public function index() {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss'] = "blog";
            $data['allBlogs']          = Blog::paginate(6);
            $data['recentBlogs']       = Blog::select('title','slug')->take(5)->get()->toArray();
            $data['recentBlogsByYear'] = Blog::select('title','slug','created_at')->orderBy('created_at', 'desc')->take(5)->get()->toArray();
            $data['blogCategory']      = Category::select('id','title','slug')->take(7)->get()->toArray();
            $data['latestProducts']    = Product::select('id','title','slug','price','image','rating')->orderBy('id', 'desc')->take(3)->get()->toArray();
            return view('web.blog.index')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function detail($slug) {
        $data = [];
        try{
            $data['pageCss']   = 'home';
            $data['pageCss'] = "blog_detail";
            $data['detail'] = Blog::where('slug', '=', $slug)->with('categories.slugs')->with('user')->first();
            if($data['detail']) $data['detail']=$data['detail']->toArray(); else abort(404);

            $data['recentBlogs']       = Blog::select('title','slug')->take(5)->get()->toArray();
            $data['recentBlogsByYear'] = Blog::select('title','slug','created_at')->orderBy('created_at', 'desc')->take(5)->get()->toArray();
            $data['blogCategory']      = Category::select('id','title','slug')->take(7)->get()->toArray();
            $data['latestProducts']    = Product::select('id','title','slug','price','image','rating')->orderBy('id', 'desc')->take(3)->get()->toArray();

            return view('web.blog.detail')->with($data);
        } catch (\Exception $e) {
                abort(404);
        }

    }

    public function categoryWiseBlogs($slug){
        $data['pageCss']   = 'home';
        $data['pageCss'] = "blog";

        $data['recentBlogs']       = Blog::select('title','slug')->take(5)->get()->toArray();
        $data['recentBlogsByYear'] = Blog::select('title','slug','created_at')->orderBy('created_at', 'desc')->take(5)->get()->toArray();
        $data['blogCategory']      = Category::select('id','title','slug')->take(7)->get()->toArray();
        $data['latestProducts']    = Product::select('id','title','slug','price','image','rating')->orderBy('id', 'desc')->take(3)->get()->toArray();

        $categoryBlogs = Category::select('id','title','short_description')->with(['blogs'=> function($q1){
            $q1->with(['slugs','user','categories']);
        }])->where('slug', '=', $slug)->first();

        if(empty($categoryBlogs)){
            abort(404);
        }

        $data['category_wise_blogs'] = $categoryBlogs->toArray();
        return view('web.blog.blog_category')->with($data);
    }

    public function blogCategories() {
        $data = [];
        try{
            $siteid = config('app.siteid');
            $data['pageCss']   = 'home';
            $data['pageCss'] = 'categories';

            $data['popular'] = Category::select('id','title')->where('popular',1)->orderBy('title')->limit(8)->get()->toArray();

            $data['blogCategory'] = Category::select('id','title','slug','category_image')->with(['blogs'=> function($q1){
                    $q1->select('id','title')->with(['slugs'])->limit(3);
                }])->get()->toArray();

            return view('web.blog.blog_category_listing')->with($data);
        }catch (\Exception $e) {
            abort(404);
        }
    }

    public function load_data(Request $request){
      $siteid = config('app.siteid');
      if($request->ajax()){
        if($request['data_id'] > 0){
          $output = '';
          $last_id = '';
          $category_id = $request['category_id'];
          $data = Blog::where('id', '>', $request['data_id'])->orderBy('id', 'ASC')->limit(2)->get()->toArray();

          if(!empty($data)){
            foreach($data as $record){
                $url = config("app.app_path")."/".$record['slugs']['slug'];
                $image = '';
                if(isset($record['blog_image']) || !empty($record['blog_image'])):
                    $image = $record['blog_image'];
                else:
                    $image = config("app.app_image").'/build/images/blog/imagePlaceHolder336.png';
                endif;
                $output .= '
                  <div class="col-1 standard-post horizontal" >
                      <div class="inner">
                          <div class="post-image">
                              <a href="'.$url.'" class="image">
                                  <img src="'.$image.'" alt="">
                              </a>
                          </div>
                          <div class="post-details">
                              <a href="javascript:;">
                                  <div class="category-details">
                                      <div class="category-tags">
                                          <span >'.$record["title"].'</span>
                                      </div>
                                  </div>
                                  <div class="post-title">
                                      <h2>'.$record["short_description"].'</h2>
                                  </div>
                              </a>
                          </div>
                          <span class="btm-line"></span>
                      </div>
                  </div>
                  ';
                $last_id = $record['id'];
              }

             $output .= '
             <div id="load_more">
              <button type="button" name="load_more_button" class="blgLoadMore form-control " blog-category-id="'.$category_id.'" data-id="'.$last_id.'" id="load_more_button">Load More</button>
             </div>
           ';

           echo $output;

          }else{

            $output .= '
             <div id="load_more">
              <button type="button" name="load_more_button" class="blgLoadMore form-control">No More Blogs</button>
             </div>
           ';

           echo $output ;

          }


        }else{

        }

      }
    }

    public function blogAuthor($slug){
      $data = [];
      $siteid = config('app.siteid');
      $data['pageCss'] = "blog_author";
      $data['pageCss']   = 'home';
      $user_id = User::where('name', $slug)->first();
      if($user_id) $user_id=$user_id->id; else abort(404);
      // $data['list'] = Category::with('blogs.slugs')->orderBy('title')->get()->toArray();
      $data['list'] = Category::select('id','title','category_image')->with('blogs.slugs')->with(['blogs' => function($blogCustomWhereBasedDataQuery){
        $blogCustomWhereBasedDataQuery->select('id','title')->CustomWhereBasedData($siteid);
      }])->orderBy('title')->get()->toArray();
      $data['blogListing'] = Blog::select('id','title','user_id','blog_image')->with('user')->with(['categories' => function($categoryQuery){
        $categoryQuery->select('id','title','category_image')->CustomWhereBasedData($siteid);
      }])->orderBy('id', 'DESC')->where('user_id', $user_id)->get()->toArray();
      return view('web.blog.author')->with($data);

    }

    public function authorLoadMoreData(Request $request){
        $siteid = config('app.siteid');
        if($request->ajax()){
          if($request['data_id'] > 0){

            $output = '';
            $last_id = '';

            $data = Blog::where('id', '<', $request['data_id'])->with('categories')->orderBy('id', 'DESC')->take(3)->get()->toArray();

            if(!empty($data)){
            foreach($data as $blog){
                $url = config("app.app_path")."/".$blog['slugs']['slug'] ;
                $image = '';
                if(isset($blog['blog_image']) || !empty($blog['blog_image'])):
                    $image = $blog['blog_image'];
                else:
                    $image = config("app.app_image").'/build/images/blog/imagePlaceHolder336.png';
                endif;
                $blogImage = $blog['categories'][0]['category_image'];
                $blogCategoryTitle = $blog['categories'][0]['title'];
                $blogTitle = "";

                $postTitle = substr($blog['title'], 0, 68);
                $postTitleLength = strlen($blog['title']);

                if($postTitleLength > 68){
                   $blogTitle = $postTitle." ... ";
                }else{
                    $blogTitle = $blog['title'];
                }

                $output .= '
                  <div class="col-3 standard-post">
                    <div class="inner">
                        <div class="post-image">
                            <a href="'.$url.'" class="image">
                                <img src="'.$image.'" alt="">
                            </a>
                        </div>
                        <div class="post-details">
                            <a href="'.$url.'">
                                <div class="category-details">
                                    <span class="cat-icon">
                                        <img src="'.$blogImage.'" data-src="'.$blogImage.'">
                                    </span>
                                    <div class="category-title">
                                        <span>'.$blogCategoryTitle.'</span>
                                    </div>
                                </div>
                                <div class="post-title">
                                    <h2>'.$blogTitle.'</h2>
                                </div>
                            </a>
                            <span class="btm-line"></span>
                        </div>
                    </div>
                </div>
                  ';
                $last_id = $blog['id'];
              }

             $output .= '
             <div id="load_more" style="width: 100%;">
              <button type="button" id="blog_load_more_button" blog-author-id="1" data-id="'.$last_id.'" class="blgLoadMore">LOAD MORE</button>
            </div>
           ';

           echo $output;

          }else{

            $output .= '
             <div id="load_more" style="width: 100%;">
              <button type="button" name="blog_load_more_button" class="blgLoadMore form-control">No More Blogs</button>
             </div>
           ';

           echo $output ;

          }

          }else{



          }

        }
    }

}
