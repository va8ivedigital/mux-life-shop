<?php
namespace App\ViewComposers;
use Illuminate\View\View;
use Illuminate\Http\Request;
use \App\WebModel\Site as Site;
use App\Models\Contact;

class SiteWideData
{
	public function compose(View $view)
	{
		$data = [];

		$data['site_wide_data'] = Site::select('id','name','country_name','country_code','url','html_tags','javascript_tags','twitter','instagram','facebook','youtube','top_header_text','meta_title','meta_keywords','meta_description','site_logo','site_favicon','site_flag','home_banner_image')->whereId('1')->first()->toArray();

        $data['contacts']   = Contact::select('address','phone_number','email')->get()->toArray();
		// $data['pages'] = Page::select('id','title','top','bottom','meta_title','meta_keywords','meta_description')->with(['slugs' => function($slugQuery){
		// 	$slugQuery->select(['id','obj_id','slug','old_slug']);
		// }])->whereHas('sites', function($page) use ($siteid){
		// 	$page->where('site_id', $siteid);
		// })->take(3)->get()->toArray();
		$view->with($data);


	}
}
