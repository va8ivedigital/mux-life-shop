@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.faqs.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.faqs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.faqs.fields.id') }}
                        </th>
                        <td>
                            {{ $faq->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.faqs.fields.title') }}
                        </th>
                        <td>
                            {{ $faq->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.faqs.fields.description') }}
                        </th>
                        <td>
                            {!! $faq->description !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.faqs.fields.sort') }}
                        </th>
                        <td>
                            {{ $faq->sort }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.faqs.fields.created_at') }}
                        </th>
                        <td>
                            {{ $faq->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.faqs.fields.update_by') }}
                        </th>
                        <td>
                            {{ $faq->update_at }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.faqs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
