@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.faqs.title_singular') }}
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route("admin.faqs.store") }}" enctype="multipart/form-data" id="userFaqsForm">
                @csrf
                <div class="form-group">
                    <label class="required" for="title">{{ trans('cruds.faqs.fields.title') }}</label>
                    <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" data-target_controller="userfaqs" id="title" value="{{ old('title', '') }}" required>
                    @if($errors->has('title'))
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.faqs.fields.title_helper') }}</span>
                </div>
                <div class="form-group" id="UserReview">
                    <label for="description">{{ trans('cruds.faqs.fields.description') }}</label>
                    <textarea class="form-control ckeditor {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{!! old('description') !!}</textarea>
                    @if($errors->has('description'))
                        <div class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.faqs.fields.description_helper') }}</span>
                </div>
                <div class="form-group">
                    <label class="required" for="sort">{{ trans('cruds.faqs.fields.sort') }}</label>
                    <input class="form-control {{ $errors->has('sort') ? 'is-invalid' : '' }}" type="number" name="sort" id="sort" value="{{ old('sort') }}" step="1" required>
                    @if($errors->has('sort'))
                        <div class="invalid-feedback">
                            {{ $errors->first('sort') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.faqs.fields.sort_helper') }}</span>
                </div>
                <div class="form-group">
                    <div class="form-check {{ $errors->has('publish') ? 'is-invalid' : '' }}">
                        <input type="hidden" name="publish" value="0">
                        <input class="form-check-input" type="checkbox" name="publish" id="publish" value="1" {{ old('publish', 0) == 1 || old('publish') === null ? 'checked' : '' }}>
                        <label class="form-check-label" for="publish">{{ trans('cruds.faqs.fields.publish') }}</label>
                    </div>
                    @if($errors->has('publish'))
                        <div class="invalid-feedback">
                            {{ $errors->first('publish') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.faqs.fields.publish_helper') }}</span>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('scripts')
@endsection
