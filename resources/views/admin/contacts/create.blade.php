@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.contacts.title_singular') }}
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route("admin.contacts.store") }}" enctype="multipart/form-data" id="usercontactsForm">
                @csrf
                <div class="form-group" id="embeded_map">
                    <label for="embeded_map">{{ trans('cruds.contacts.fields.embeded_map') }}</label>
                    <textarea class="form-control ckeditor {{ $errors->has('embeded_map') ? 'is-invalid' : '' }}" name="embeded_map" id="embeded_map">{!! old('embeded_map') !!}</textarea>
                    @if($errors->has('embeded_map'))
                        <div class="invalid-feedback">
                            {{ $errors->first('embeded_map') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.contacts.fields.description_helper') }}</span>
                </div>
                <div class="form-group" id="description">
                    <label for="description">{{ trans('cruds.contacts.fields.description') }}</label>
                    <textarea class="form-control ckeditor {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{!! old('description') !!}</textarea>
                    @if($errors->has('description'))
                        <div class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.contacts.fields.description_helper') }}</span>
                </div>
                <div class="form-group" id="address">
                    <label for="address">{{ trans('cruds.contacts.fields.address') }}</label>
                    <textarea class="form-control ckeditor {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{!! old('address') !!}</textarea>
                    @if($errors->has('address'))
                        <div class="invalid-feedback">
                            {{ $errors->first('address') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.contacts.fields.address_helper') }}</span>
                </div>
                <div class="form-group" id="phone_number">
                    <label for="phone_number">{{ trans('cruds.contacts.fields.phone_number') }}</label>
                    <textarea class="form-control ckeditor {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" name="phone_number" id="phone_number">{!! old('phone_number') !!}</textarea>
                    @if($errors->has('phone_number'))
                        <div class="invalid-feedback">
                            {{ $errors->first('phone_number') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.contacts.fields.phone_number_helper') }}</span>
                </div>
                <div class="form-group" id="email">
                    <label for="email">{{ trans('cruds.contacts.fields.email') }}</label>
                    <textarea class="form-control ckeditor {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" id="email">{!! old('email') !!}</textarea>
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.contacts.fields.email_helper') }}</span>
                </div>
                <div class="form-group">
                    <label class="required" for="sort">{{ trans('cruds.contacts.fields.sort') }}</label>
                    <input class="form-control {{ $errors->has('sort') ? 'is-invalid' : '' }}" type="number" name="sort" id="sort" value="{{ old('sort') }}" step="1" required>
                    @if($errors->has('sort'))
                        <div class="invalid-feedback">
                            {{ $errors->first('sort') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.contacts.fields.sort_helper') }}</span>
                </div>
                <div class="form-group">
                    <div class="form-check {{ $errors->has('publish') ? 'is-invalid' : '' }}">
                        <input type="hidden" name="publish" value="0">
                        <input class="form-check-input" type="checkbox" name="publish" id="publish" value="1" {{ old('publish', 0) == 1 || old('publish') === null ? 'checked' : '' }}>
                        <label class="form-check-label" for="publish">{{ trans('cruds.contacts.fields.publish') }}</label>
                    </div>
                    @if($errors->has('publish'))
                        <div class="invalid-feedback">
                            {{ $errors->first('publish') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.contacts.fields.publish_helper') }}</span>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('scripts')
@endsection
