@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.contacts.title_singular') }}
    </div>

    <div class="card-body">
        <input type="hidden" data-name="edit_id" value="{{ $contact->id }}" class="edit_id">
        <form method="POST" action='{{ route("admin.contacts.update", [$contact->id]) }}' enctype="multipart/form-data" id="clientForm">
            @method('PUT')
            @csrf
            <div class="form-group" id="embeded_map">
                <label for="embeded_map">{{ trans('cruds.contacts.fields.embeded_map') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('embeded_map') ? 'is-invalid' : '' }}" name="embeded_map" id="embeded_map">{!! old('embeded_map', $contact->embeded_map) !!}</textarea>
                @if($errors->has('embeded_map'))
                    <div class="invalid-feedback">
                        {{ $errors->first('embeded_map') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.contacts.fields.embeded_map_helper') }}</span>
            </div>
            <div class="form-group" id="description">
                <label for="description">{{ trans('cruds.contacts.fields.description') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{!! old('description', $contact->description) !!}</textarea>
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.contacts.fields.description_helper') }}</span>
            </div>
            <div class="form-group" id="address">
                <label for="address">{{ trans('cruds.contacts.fields.address') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{!! old('address', $contact->address) !!}</textarea>
                @if($errors->has('address'))
                    <div class="invalid-feedback">
                        {{ $errors->first('address') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.contacts.fields.address_helper') }}</span>
            </div>
            <div class="form-group" id="phone_number">
                <label for="phone_number">{{ trans('cruds.contacts.fields.phone_number') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" name="phone_number" id="phone_number">{!! old('phone_number', $contact->phone_number) !!}</textarea>
                @if($errors->has('phone_number'))
                    <div class="invalid-feedback">
                        {{ $errors->first('phone_number') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.contacts.fields.phone_number_helper') }}</span>
            </div>
            <div class="form-group" id="email">
                <label for="email">{{ trans('cruds.contacts.fields.email') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" id="email">{!! old('email', $contact->email) !!}</textarea>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.contacts.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
@endsection
