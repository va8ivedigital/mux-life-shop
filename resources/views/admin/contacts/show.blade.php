@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.contacts.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.contacts.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.id') }}
                        </th>
                        <td>
                            {{ $contact->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.embeded_map') }}
                        </th>
                        <td>
                            {!! $contact->embeded_map !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.description') }}
                        </th>
                        <td>
                            {!! $contact->description !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.address') }}
                        </th>
                        <td>
                            {!! $contact->address !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.phone_number') }}
                        </th>
                        <td>
                            {!! $contact->phone_number !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.email') }}
                        </th>
                        <td>
                            {!! $contact->email !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.created_at') }}
                        </th>
                        <td>
                            {{ $contact->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contacts.fields.updated_at') }}
                        </th>
                        <td>
                            {{ $contact->updated_at }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.contacts.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
