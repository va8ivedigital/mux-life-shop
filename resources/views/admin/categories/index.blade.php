@extends('layouts.admin')
@section('content')
    {{-- @can('category_create') --}}
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-8">
                <a class="btn btn-success" href="{{ route("admin.categories.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.category.title_singular') }}
                </a>
            </div>
            <div class="col-lg-4">
            </div>
        </div>
    {{-- @endcan --}}
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.category.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive" id="datatable-Category">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Category">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            {{-- <th>
                                {{ trans('cruds.category.fields.site') }}
                            </th> --}}
                            <th>
                                {{ trans('cruds.category.fields.title') }}
                            </th>
                            <th>
                                {{ trans('cruds.category.fields.sort') }}
                            </th>
                            <th>
                                {{ trans('cruds.category.fields.publish') }}
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr data-entry-id="{{ $category->id }}">
                                <td>

                                </td>
                                <td>
                                    {{ $category->title ?? '' }}
                                </td>
                                <td>
                                    {{ $category->sort ?? '' }}
                                </td>
                                <td>
                                    <span style="display:none">{{ $category->publish ?? '' }}</span>
                                    <input type="checkbox" disabled="disabled" {{ $category->publish ? 'checked' : '' }}>
                                </td>
                                <td>
                                    {{-- @can('category_show') --}}
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.categories.show', $category->id) }}">
                                            {{ trans('global.view') }}
                                        </a>
                                    {{-- @endcan --}}

                                    {{-- @can('category_edit') --}}
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.categories.edit', $category->id) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    {{-- @endcan --}}

                                    {{-- @can('category_delete') --}}
                                        @php
                                            $url = isset(request()->test_id) ? route('admin.categories.destroy', $category->id) . '?test_id=1' : route('admin.categories.destroy', $category->id);
                                        @endphp
                                        <form action="{{ $url }}" id="{{ 'categories' . $category->id }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        </form>
                                    {{-- @endcan --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
@section('scripts')
    @parent
    @php
        $url = isset(request()->test_id) ? route('admin.categories.index') . '?test_id=1' : route('admin.categories.index');
        if(isset(request()->cid) && isset(request()->test_id)) {
            $url = $url . "&cid=" . request()->cid;
        } elseif (isset(request()->cid)) {
            $url = $url . "?cid=" . request()->cid;
        }
    @endphp
    <script>

        var url = "{{ $url }}";
        url = url.replace(/&amp;/g, '&');

        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            @can('banner_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('admin.categories.massDestroy') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                        headers: {'x-csrf-token': "{{ csrf_token() }}"},
                        method: 'POST',
                        url: config.url,
                        data: { ids: ids, _method: 'DELETE' }})
                        .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            //@endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 2, 'asc' ]],
                pageLength: 10,
            });
            $('.datatable-Category').DataTable({ buttons: dtButtons })
                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                    $($.fn.dataTable.tables(true)).DataTable()
                        .columns.adjust();
            });
        })

    </script>
@endsection
