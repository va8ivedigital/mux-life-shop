@extends('layouts.admin')
@section('content')
{{-- @can('blog_create') --}}
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-8">
            <a class="btn btn-success" href="{{ route("admin.about.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.about_us.title_singular') }} Section
            </a>
        </div>
    </div>
{{-- @endcan --}}
<div class="card">
    <div class="card-header">
        {{ trans('cruds.about_us.title_singular') }} Sections {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive" id="datatable-About">
            <table class=" table table-bordered table-striped table-hover datatable datatable-About">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.about_us.fields.title') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($about as $key => $about)
                        <tr data-entry-id="{{ $about->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $about->title ?? '' }}
                            </td>
                            <td>
                                {{-- @can('about_show') --}}
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.about.show', $about->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('about_edit') --}}
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.about.edit', $about->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('about_delete') --}}
                                    @php
                                        $url = isset(request()->test_id) ? route('admin.about.destroy', $about->id) . '?test_id=1' : route('admin.about.destroy', $about->id);
                                    @endphp
                                    <form action="{{ $url }}" id="{{ 'about' . $about->id }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                {{-- @endcan --}}

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
@php
    $url = isset(request()->test_id) ? route('admin.about.index') . '?test_id=1' : route('admin.about.index');
    if(isset(request()->bid) && isset(request()->test_id)) {
        $url = $url . "&bid=" . request()->bid;
    } elseif (isset(request()->bid)) {
        $url = $url . "?bid=" . request()->bid;
    }
@endphp
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        @can('banner_delete')
        let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        let deleteButton = {
            text: deleteButtonTrans,
            url: "{{ route('admin.about.massDestroy') }}",
            className: 'btn-danger',
            action: function (e, dt, node, config) {
                var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                    return $(entry).data('entry-id')
                });

                if (ids.length === 0) {
                    alert('{{ trans('global.datatables.zero_selected') }}')

                    return
                }

                if (confirm('{{ trans('global.areYouSure') }}')) {
                    $.ajax({
                    headers: {'x-csrf-token': "{{ csrf_token() }}"},
                    method: 'POST',
                    url: config.url,
                    data: { ids: ids, _method: 'DELETE' }})
                    .done(function () { location.reload() })
                }
            }
        }
        dtButtons.push(deleteButton)
        //@endcan

        $.extend(true, $.fn.dataTable.defaults, {
            order: [[ 2, 'asc' ]],
            pageLength: 10,
        });
        $('.datatable-About:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
        });
    });
</script>
@endsection
