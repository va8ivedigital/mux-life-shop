@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.about_us.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.about.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.id') }}
                        </th>
                        <td>
                            {{ $about->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.title') }}
                        </th>
                        <td>
                            {{ $about->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.description') }}
                        </th>
                        <td>
                            {!! $about->description !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.image') }}
                        </th>
                        <td>
                            @if($about->image)
                                <a href="{{ $about->image->getUrl() }}" target="_blank">
                                    <img src="{{ $about->image->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.created_at') }}
                        </th>
                        <td>
                            {{ $about->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.updated_at') }}
                        </th>
                        <td>
                            {{ $about->updated_at }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.about.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
