@extends('layouts.admin')
@section('content')
{{-- @can('blog_create') --}}
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-8">
            <a class="btn btn-success" href="{{ route("admin.blogs.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.blog.title_singular') }}
            </a>
        </div>
        <div class="col-lg-4">
        </div>
    </div>
{{-- @endcan --}}
<div class="card">
    <div class="card-header">
        {{ trans('cruds.blog.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive" id="datatable-Blog">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Blog">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.blog.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.blog.fields.sort') }}
                        </th>
                        <th>
                            {{ trans('cruds.blog.fields.publish') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($blogs as $blog)
                        <tr data-entry-id="{{ $blog->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $blog->title ?? '' }}
                            </td>
                            <td>
                                {{ $blog->sort ?? '' }}
                            </td>
                            <td>
                                {{-- @can('blog_show') --}}
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.blogs.show', $blog->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('blog_edit') --}}
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.blogs.edit', $blog->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('blog_delete') --}}
                                    @php
                                        $url = isset(request()->test_id) ? route('admin.blogs.destroy', $blog->id) . '?test_id=1' : route('admin.blogs.destroy', $blog->id);
                                    @endphp
                                    <form action="{{ $url }}" id="{{ 'blogs' . $blog->id }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                {{-- @endcan --}}

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
@php
    $url = isset(request()->test_id) ? route('admin.blogs.index') . '?test_id=1' : route('admin.blogs.index');
    if(isset(request()->bid) && isset(request()->test_id)) {
        $url = $url . "&bid=" . request()->bid;
    } elseif (isset(request()->bid)) {
        $url = $url . "?bid=" . request()->bid;
    }
@endphp
<script>

    var url = "{{ $url }}";
    url = url.replace(/&amp;/g, '&');

    $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            @can('banner_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('admin.blogs.massDestroy') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                        headers: {'x-csrf-token': "{{ csrf_token() }}"},
                        method: 'POST',
                        url: config.url,
                        data: { ids: ids, _method: 'DELETE' }})
                        .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            //@endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 2, 'asc' ]],
                pageLength: 10,
            });
            $('.datatable-Blog').DataTable({ buttons: dtButtons })
                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                    $($.fn.dataTable.tables(true)).DataTable()
                        .columns.adjust();
            });
    });
</script>
@endsection
