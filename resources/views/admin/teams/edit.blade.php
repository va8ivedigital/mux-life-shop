@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            {{ trans('global.edit') }} {{ trans('cruds.about_us.team_member_title_singular') }}
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route("admin.teams.update", [$team->id]) }}" enctype="multipart/form-data" id="teamForm">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="title">{{ trans('cruds.about_us.fields.member_name') }}</label>
                    <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', $team->title) }}">
                    @if($errors->has('title'))
                        <div class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.about_us.fields.member_name_helper') }}</span>
                </div>
                <div class="form-group">
                    <label class="required" for="designation">{{ trans('cruds.about_us.fields.member_designation') }}</label>
                    <input class="form-control {{ $errors->has('designation') ? 'is-invalid' : '' }}" type="text" name="designation" id="designation" value="{{ old('designation', $team->designation) }}" required>
                    @if($errors->has('designation'))
                        <div class="invalid-feedback">
                            {{ $errors->first('designation') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.about_us.fields.member_designation_helper') }}</span>
                </div>
                <div class="form-group">
                    <label class="required" for="image">{{ trans('cruds.about_us.fields.member_image') }}</label>
                    <div class="needsclick dropzone {{ $errors->has('image') ? 'is-invalid' : '' }}" id="image-dropzone">
                    </div>
                    @if($errors->has('image'))
                        <div class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.about_us.fields.member_image_helper') }}</span>
                </div>
                <div class="form-group">
                    <label for="facebook_url">{{ trans('cruds.about_us.fields.member_social_fb') }}</label>
                    <input class="form-control {{ $errors->has('facebook_url') ? 'is-invalid' : '' }}" name="facebook_url" id="facebook_url" value="{{ old('facebook_url', $team->facebook_url) }}">
                    @if($errors->has('facebook_url'))
                        <div class="invalid-feedback">
                            {{ $errors->first('facebook_url') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.about_us.fields.member_social_ins_helper') }}</span>
                </div>
                <div class="form-group">
                    <label for="instagram_url">{{ trans('cruds.about_us.fields.member_social_ins') }}</label>
                    <input class="form-control {{ $errors->has('instagram_url') ? 'is-invalid' : '' }}" name="instagram_url" id="instagram_url" value="{{ old('instagram_url', $team->instagram_url) }}">
                    @if($errors->has('instagram_url'))
                        <div class="invalid-feedback">
                            {{ $errors->first('instagram_url') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.about_us.fields.member_social_ins_helper') }}</span>
                </div>
                <div class="form-group">
                    <label for="twitter_url">{{ trans('cruds.about_us.fields.member_social_twt') }}</label>
                    <input class="form-control {{ $errors->has('twitter_url') ? 'is-invalid' : '' }}" name="twitter_url" id="twitter_url" value="{{ old('twitter_url', $team->twitter_url) }}">
                    @if($errors->has('twitter_url'))
                        <div class="invalid-feedback">
                            {{ $errors->first('twitter_url') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.about_us.fields.member_social_twt_helper') }}</span>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    Dropzone.options.imageDropzone = {
    url: '{{ route('admin.teams.storeMedia') }}',
    maxFilesize: 1, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif,.webp',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 1,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="image"]').remove()
      $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="image"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
        @if(isset($team) && $team->image)
            var file = {!! json_encode($team->image) !!}
            this.options.addedfile.call(this, file)
            this.options.thumbnail.call(this, file, '{{ $team->image->getUrl('thumb') }}')
            file.previewElement.classList.add('dz-complete')
            $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
            this.options.maxFiles = this.options.maxFiles - 1
        @endif
        },
        error: function (file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }
            return _results
        }
    }
</script>
@endsection
