@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.about_us.team_member_title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.teams.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.id') }}
                        </th>
                        <td>
                            {{ $team->id }}
                        </td>
                    </tr>
                    {{-- <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.site') }}
                        </th>
                        <td>
                            @foreach($team->sites as $key => $site)
                                <span class="label label-info">{{ $site->name }}</span>
                            @endforeach
                        </td>
                    </tr> --}}
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.member_name') }}
                        </th>
                        <td>
                            {{ $team->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.member_designation') }}
                        </th>
                        <td>
                            {{ $team->designation }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.member_image') }}
                        </th>
                        <td>
                            @if($team->image)
                                <a href="{{ $team->image->getUrl() }}" target="_blank">
                                    <img src="{{ $team->image->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.member_social_fb') }}
                        </th>
                        <td>
                            {{ $team->facebook_url }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.member_social_ins') }}
                        </th>
                        <td>
                            {{ $team->twitter_url }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.member_social_twt') }}
                        </th>
                        <td>
                            {{ $team->twitter_url }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.created_at') }}
                        </th>
                        <td>
                            {{ $team->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.about_us.fields.updated_at') }}
                        </th>
                        <td>
                            {{ $team->updated_at }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.teams.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
