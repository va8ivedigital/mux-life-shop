@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.clients.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.clients.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.clients.fields.id') }}
                        </th>
                        <td>
                            {{ $client->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.clients.fields.title') }}
                        </th>
                        <td>
                            {{ $client->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.clients.fields.image') }}
                        </th>
                        <td>
                            @if($client->image)
                                <a href="{{ $client->image->getUrl() }}" target="_blank">
                                    <img src="{{ $client->image->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.clients.fields.sort') }}
                        </th>
                        <td>
                            {{ $client->sort }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.clients.fields.created_at') }}
                        </th>
                        <td>
                            {{ $client->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.clients.fields.update_by') }}
                        </th>
                        <td>
                            {{ $client->update_at }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.clients.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
