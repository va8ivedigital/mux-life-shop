@extends('layouts.admin')
@section('content')
    {{-- @can('product_create') --}}
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.products.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.product.title_singular') }}
                </a>
            </div>
        </div>
    {{-- @endcan --}}
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.product.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body" id="datatable-Product">
            <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Product">
                <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.product.fields.title') }}
                    </th>
                    <th>
                        {{ trans('cruds.product.fields.sort') }}
                    </th>
                    <th>
                        {{ trans('cruds.product.fields.publish') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr data-entry-id="{{ $product->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $product->title ?? '' }}
                            </td>
                            <td>
                                {{ $product->sort ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $product->publish ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $product->publish ? 'checked' : '' }}>
                            </td>
                            <td>
                                {{-- @can('product_show') --}}
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.products.show', $product->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('product_edit') --}}
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.products.edit', $product->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('product_delete') --}}
                                    @php
                                        $url = isset(request()->test_id) ? route('admin.products.destroy', $product->id) . '?test_id=1' : route('admin.products.destroy', $product->id);
                                    @endphp
                                    <form action="{{ $url }}" id="{{ 'products' . $product->id }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                {{-- @endcan --}}

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>



@endsection
@section('scripts')
    @parent
    @php
        $url = isset(request()->test_id) ? route('admin.products.index') . '?test_id=1' : route('admin.products.index');
        if(isset(request()->pid) && isset(request()->test_id)) {
            $url = $url . "&pid=" . request()->pid;
        } elseif (isset(request()->pcid)) {
            $url = $url . "?pid=" . request()->pid;
        }
    @endphp
<script>

    var url = "{{ $url }}";
    url = url.replace(/&amp;/g, '&');

    $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            @can('banner_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('admin.products.massDestroy') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                        headers: {'x-csrf-token': "{{ csrf_token() }}"},
                        method: 'POST',
                        url: config.url,
                        data: { ids: ids, _method: 'DELETE' }})
                        .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            //@endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 2, 'asc' ]],
                pageLength: 10,
            });
            $('.datatable-Product').DataTable({ buttons: dtButtons })
                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                    $($.fn.dataTable.tables(true)).DataTable()
                        .columns.adjust();
            });
    });
</script>
@endsection
