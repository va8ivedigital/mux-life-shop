@extends('layouts.admin')
@section('content')
{{-- @can('blog_create') --}}
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-8">
            <a class="btn btn-success" href="{{ route("admin.reviews.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.reviews.title_singular') }}
            </a>
        </div>
    </div>
{{-- @endcan --}}
<div class="card">
    <div class="card-header">
        {{ trans('cruds.reviews.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive" id="datatable-UserReview">
            <table class=" table table-bordered table-striped table-hover datatable datatable-UserReview">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.reviews.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.reviews.fields.sort') }}
                        </th>
                        <th>
                            {{ trans('cruds.reviews.fields.publish') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($reviews as $key => $review)
                        <tr data-entry-id="{{ $review->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $review->title ?? '' }}
                            </td>
                            <td>
                                {{ $review->sort ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $review->publish ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $review->publish ? 'checked' : '' }}>
                            </td>
                            <td>
                                {{-- @can('client_show') --}}
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.reviews.show', $review->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('client_edit') --}}
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.reviews.edit', $review->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                {{-- @endcan --}}

                                {{-- @can('client_delete') --}}
                                    @php
                                        $url = isset(request()->test_id) ? route('admin.reviews.destroy', $review->id) . '?test_id=1' : route('admin.reviews.destroy', $review->id);
                                    @endphp
                                    <form action="{{ $url }}" id="{{ 'reviews' . $review->id }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                {{-- @endcan --}}

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
@php
    $url = isset(request()->test_id) ? route('admin.reviews.index') . '?test_id=1' : route('admin.reviews.index');
    if(isset(request()->bid) && isset(request()->test_id)) {
        $url = $url . "&bid=" . request()->bid;
    } elseif (isset(request()->bid)) {
        $url = $url . "?bid=" . request()->bid;
    }
@endphp
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        @can('banner_delete')
        let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        let deleteButton = {
            text: deleteButtonTrans,
            url: "{{ route('admin.reviews.massDestroy') }}",
            className: 'btn-danger',
            action: function (e, dt, node, config) {
                var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                    return $(entry).data('entry-id')
                });

                if (ids.length === 0) {
                    alert('{{ trans('global.datatables.zero_selected') }}')

                    return
                }

                if (confirm('{{ trans('global.areYouSure') }}')) {
                    $.ajax({
                    headers: {'x-csrf-token': "{{ csrf_token() }}"},
                    method: 'POST',
                    url: config.url,
                    data: { ids: ids, _method: 'DELETE' }})
                    .done(function () { location.reload() })
                }
            }
        }
        dtButtons.push(deleteButton)
        //@endcan

        $.extend(true, $.fn.dataTable.defaults, {
            order: [[ 2, 'asc' ]],
            pageLength: 10,
        });
        $('.datatable-UserReview:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
        });
    });
    /*
        var url = "{{ $url }}";
        url = url.replace(/&amp;/g, '&');

        $(function () {
                let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            @can('blog_delete')
                let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
                let deleteButton = {
                    text: deleteButtonTrans,
                    url: "{{ route('admin.reviews.massDestroy') }}",
                    className: 'btn-danger',
                    action: function (e, dt, node, config) {
                        var ids = $.map(dt.rows({selected: true}).data(), function (entry) {
                            return entry.id
                        });

                        if (ids.length === 0) {
                            alert('{{ trans('global.datatables.zero_selected') }}')

                            return
                        }

                        if (confirm('{{ trans('global.areYouSure') }}')) {
                            $.ajax({
                                headers: {'x-csrf-token': "{{ csrf_token() }}"},
                                method: 'POST',
                                url: config.url,
                                data: {ids: ids, _method: 'DELETE'}
                            })
                                .done(function () {
                                    location.reload()
                                })
                        }
                    }
                }
                dtButtons.push(deleteButton)
            //@endcan

            let dtOverrideGlobals = {
                buttons: dtButtons,
                processing: true,
                serverSide: true,
                retrieve: false,
                aaSorting: [],
                ajax: url,
                columns: [
                    {data: 'placeholder', name: 'placeholder'},
                    {data: 'title', name: 'title'},
                    {data: 'sort', name: 'sort'},
                    {data: 'actions', name: '{{ trans('global.actions') }}'}
                ],
                order: [[ 2, 'asc' ]],
                pageLength: 10,
                destroy: true
            };

            $('.datatable-UserReview').DataTable(dtOverrideGlobals);
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });


                document.getElementById('siteOpt').onchange = function () {
                    $("#datatable-UserReview").html("");
                    $("#datatable-UserReview").html(`
                                <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-UserReview">
                                    <thead>
                                        <tr>
                                            <th width="10">

                                            </th>
                                            <th>
                                                {{ trans('cruds.reviews.fields.title') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.reviews.fields.sort') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.reviews.fields.publish') }}
                                            </th>
                                            <th>

                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            `);

                    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                    @can('blog_delete')
                        let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
                        let deleteButton = {
                            text: deleteButtonTrans,
                            url: "{{ route('admin.reviews.massDestroy') }}",
                            className: 'btn-danger',
                            action: function (e, dt, node, config) {
                                var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
                                    return entry.id
                                });

                                if (ids.length === 0) {
                                    alert('{{ trans('global.datatables.zero_selected') }}')

                                    return
                                }

                                if (confirm('{{ trans('global.areYouSure') }}')) {
                                    $.ajax({
                                        headers: {'x-csrf-token': "{{ csrf_token() }}"},
                                        method: 'POST',
                                        url: config.url,
                                        data: { ids: ids, _method: 'DELETE' }})
                                        .done(function () { location.reload() })
                                }
                            }
                        }
                        dtButtons.push(deleteButton)
                    //@endcan

                    let dtOverrideGlobals = {
                            buttons: dtButtons,
                            processing: true,
                            serverSide: true,
                            retrieve: false,
                            aaSorting: [],
                            ajax: {
                                "url": url,
                                "data": {
                                    "siteId": $(this).val()
                                }
                            },
                            columns: [
                                {data: 'placeholder', name: 'placeholder'},
                                {data: 'site', name: 'sites.name'},
                                {data: 'title', name: 'title'},
                                {data: 'sort', name: 'sort'},
                                {data: 'publish', name: 'publish'},
                                {data: 'actions', name: '{{ trans('global.actions') }}'}
                            ],
                            order: [[ 2, 'asc' ]],
                            pageLength: 10,
                            destroy: true
                        };

                    $('.datatable-UserReview').DataTable(dtOverrideGlobals);
                    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                        $($.fn.dataTable.tables(true)).DataTable()
                            .columns.adjust();
                    });
                }
        });
    */
</script>
@endsection
