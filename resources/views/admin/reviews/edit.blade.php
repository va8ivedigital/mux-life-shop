@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.reviews.title_singular') }}
    </div>

    <div class="card-body">
        <input type="hidden" data-name="edit_id" value="{{ $review->id }}" class="edit_id">
        <form method="POST" action='{{ route("admin.reviews.update", [$review->id]) }}' enctype="multipart/form-data" id="clientForm">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="title">{{ trans('cruds.reviews.fields.title') }}</label>
                <input class="form-control auto_slug {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" data-target_controller="client" value="{!! old('title', $review->title) !!}" required>
                @if($errors->has('title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.reviews.fields.title_helper') }}</span>
            </div>
                <div class="form-group" id="UserReview">
                    <label for="user_review">{{ trans('cruds.reviews.fields.user_review') }}</label>
                    <textarea class="form-control ckeditor {{ $errors->has('user_review') ? 'is-invalid' : '' }}" name="user_review" id="user_review">{!! old('user_review', $review->user_review) !!}</textarea>
                    @if($errors->has('user_review'))
                        <div class="invalid-feedback">
                            {{ $errors->first('user_review') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.reviews.fields.user_review_helper') }}</span>
                </div>
            <div class="form-group">
                <label class="required" for="sort">{{ trans('cruds.reviews.fields.sort') }}</label>
                <input class="form-control {{ $errors->has('sort') ? 'is-invalid' : '' }}" type="number" name="sort" id="sort" value="{{ old('sort', $review->sort) }}" step="1" required>
                @if($errors->has('sort'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sort') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.reviews.fields.sort_helper') }}</span>
            </div>
            {{-- <div class="form-group">
                <div class="form-check {{ $errors->has('publish') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="publish" value="0">
                    <input class="form-check-input" type="checkbox" name="publish" id="publish" value="1" {{ $review->publish || old('publish', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="publish">{{ trans('cruds.reviews.fields.publish') }}</label>
                </div>
                @if($errors->has('publish'))
                    <div class="invalid-feedback">
                        {{ $errors->first('publish') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.reviews.fields.publish_helper') }}</span>
            </div> --}}
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
@endsection
