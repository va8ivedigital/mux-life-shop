@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.reviews.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.reviews.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.reviews.fields.id') }}
                        </th>
                        <td>
                            {{ $review->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.reviews.fields.title') }}
                        </th>
                        <td>
                            {{ $review->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.reviews.fields.user_review') }}
                        </th>
                        <td>
                            {{ $review->user_review }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.reviews.fields.sort') }}
                        </th>
                        <td>
                            {{ $review->sort }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.reviews.fields.created_at') }}
                        </th>
                        <td>
                            {{ $review->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.reviews.fields.update_by') }}
                        </th>
                        <td>
                            {{ $review->update_at }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.reviews.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
