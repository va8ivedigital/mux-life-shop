@extends('web.layouts.app')
@section('content')
<div class="main">
    <div class="breadcrumb">
        <div class="container">
            <h1 class="title">LOGIN</h1>
            <ul>
                <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                <li><span>Login</span></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="section">
            <div class="heading-wrapper">
                <h2 class="top-heading left">LOGIN</h2>
                @if(session('from_cart_when_not_loggedin'))
                    <div class="alert alert-success" role="alert">
                        {{ session('from_cart_when_not_loggedin') }}
                    </div>
                @endif
            </div>
                <form action="{{ route('customer.login') }}" method="post" class="form-wrap">
                @csrf
                <div class="inputWrapper column-1">
                    <input type="text" name="email" value="" placeholder="Email Address" id="input-email" required="">
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
                <div class="inputWrapper column-1">
                    <input type="password" name="password" value="" placeholder="Password" id="input-name" required="">
                    @if($errors->has('password'))
                        <div class="invalid-feedback">
                            {{ $errors->first('password') }}
                        </div>
                    @endif
                </div>
                <div class="inputWrapper column-1">
                    <button type="submit" class="black">Sign In</button>
                </div>
                <div class="inputWrapper column-1">
                    <a href="forgot_password.php" class="sub-heading left">Forgot your Password ?</a>
                </div>
                <div class="inputWrapper column-1">
                    <p class="sub-heading left"><strong>If you don't have an account, please <a href="{{ url('customer/register') }}">click here</a> Signup now</strong></p>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- <div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card mx-4">
            <div class="card-body p-4">
                @if(session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('customer.login') }}">
                    @csrf

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>

                        <input id="email" name="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">

                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-lock"></i></span>
                        </div>

                        <input id="password" name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}">

                        @if($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>

                    <div class="input-group mb-4">
                        <div class="form-check checkbox">
                            <input class="form-check-input" name="remember" type="checkbox" id="remember" style="vertical-align: middle;" />
                            <label class="form-check-label" for="remember" style="vertical-align: middle;">
                                {{ trans('global.remember_me') }}
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary px-4">
                                {{ trans('global.login') }}
                            </button>
                        </div>
                        <div class="col-6 text-right">
                            @if(Route::has('password.request'))
                                <a class="btn btn-link px-0" href="{{ route('password.request') }}">
                                    {{ trans('global.forgot_password') }}
                                </a><br>
                            @endif

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> --}}
@endsection
