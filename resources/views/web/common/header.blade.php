<?php ob_start(); ?>
<?php //header('X-Robots-Tag: noindex, nofollow'); ?>
    <!DOCTYPE html>
<html lang="en">
<head>
    @php
        //Meta Keywords, Title and Meta Description
        $title = (isset($meta['title'])) ? $meta['title'] : $site_wide_data['meta_title'];
        $description = (isset($meta['keywords'])) ? $meta['keywords'] : $site_wide_data['meta_description'];
        $keywords = (isset($meta['description'])) ? $meta['description'] : $site_wide_data['meta_keywords'];
    @endphp
    <title>{!! $title !!}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, initial-scale=1.0" />
    <meta name="description" content="{{ $description }}" />
    <meta name="keywords" content="{{ $keywords }}">

      <!-- html tags for site -->
    {!! isset($site_wide_data['html_tags']) ? $site_wide_data['html_tags'] : '' !!}

    <link rel="icon" href="{{ isset($site_wide_data['site_favicon']) ? $site_wide_data['site_favicon'] : config('app.app_image').'/build/images/favicon.png' }}" type="image/x-icon">
    <link rel="preload" as="style" href="{{ asset("build/css/fonts.css") }}" onload="this.rel='stylesheet'" crossorigin="anonymous" />

    <style>
        <?php
            if(isset($pageCss)){
                $css = asset("build/css/$pageCss.css");
                readfile("build/css/$pageCss.css");
            }else{
                $css = asset("build/css/main.css");
                readfile("build/css/main.css");
            }
        ?>
    </style>

</head>
    <main class="mainWrapper">
        <div class="overlay"></div>
        <header class="header">
            @if( isset($site_wide_data['top_header_text']) )
                <div class="middlebar">
                    <div class="flex Vhc container">
                        <p>{!! $site_wide_data['top_header_text'] !!}</p>
                    </div>
                </div>
            @endif
            <div class="main-header-bar">
                <div class="container">
                    <div class="flex rowbar10 main-navigation-bar">
                        <i class="x_menu hidden menu-icon"></i>
                        <a href="{{config('app.app_path')}}/" class="logo">
                            <picture>
                                <img src="{{ config('app.image_path') . '/build/images/logo.png' }}" data-src="{{ isset($site_wide_data['site_logo']) ? $site_wide_data['site_logo'] : config('app.image_path') . '/build/images/logo.png' }}" width="141" height="43" alt="logo" />
                            </picture>
                        </a>
                        <div class="main-navigation">
                            <ul class="responsive-nav-items">
                                <i class="x_close close-sidenav hidden"></i>
                                <li><a href="{{config('app.app_path')}}/shop">Shop</a></li>
                                <li><a href="{{config('app.app_path')}}/shop">Category</a></li>
                                <li><a href="{{config('app.app_path')}}/sale">Sale</a></li>
                                <li><a href="{{ config('app.app_path') }}/blog">Blogs</a></li>
                                <li><a href="{{config('app.app_path')}}/about">About us</a></li>
                                <li><a href="{{ config('app.app_path') }}/faqs">FAQS</a></li>
                            </ul>
                            <div class="account hidden">
                                <div class="head">
                                    <span>My Account</span>
                                    <i class="x_up account-icon"></i>
                                </div>
                                <div class="list-wrp">
                                    <ul class="list">
                                        <li><a href="{{ url('cart') }}">Cart</a></li>
                                        <li><a href="{{ config('app.app_path') }}/wishlist">Wishlist</a></li>
                                        <li><a href="{{ config('app.app_path') }}/checkout">Checkout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="right-navigation">
                            <ul>
                                <li class="search">
                                    <a href="javascript:;" class="searchbar-open">
                                        <i class="x_search"></i>
                                    </a>
                                </li>
                                <li class="add-to-cart">
                                    <a href="{{ url('cart') }}">
                                        <i class="x_cart"></i>
                                    </a>
                                </li>
                                @if(Auth::guard('customer')->user())
                                    <li class="user-login">
                                        <a href="{{ config('app.app_path') }}/customer/my-account">
                                            <i class="x_user1"></i>
                                        </a>
                                    </li>
                                @else
                                    <li class="user-login">
                                        <a href="{{ config('app.app_path') }}/customer/login">
                                            <i class="x_user1"></i>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="searchbar hidden">
                <div class="container flex">
                    <i class="x_search"></i>
                    <input type="text" placeholder="Search..." class="searchbar-input">
                    <i class="x_close searchbar-close"></i>
                </div>
                <div class="search-result hidden">
                    <div class="container">
                        <div class="heading">
                            <p class="number">6 results</p>
                            <a href="{{config('app.app_path')}}/shop" class="view-all">view all</a>
                        </div>
                        <div class="results">
                            <div class="grid-column-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
