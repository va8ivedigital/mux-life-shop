<footer class="footer">
    <div class="container">
        <div class="footer-main">
            <div class="about">
                <a href="{{config('app.app_path')}}/" class="logo">
                    <picture>
                        <img src="{{ config('app.image_path') . '/build/images/logo.png' }}" data-src="{{ isset($site_wide_data['site_logo']) ? $site_wide_data['site_logo'] : config('app.image_path') . '/build/images/logo.png' }}" width="141" height="43" alt="logo" />
                    </picture>
                </a>
                <p>{{ trans('sentence.footer_text') }}</p>
            </div>

            <div class="contact-info">
                <ul class="footer-accordian grid-column-3">
                    <li>
                        <a href="javascript:;" class="accordian-toggle"> Social Media
                            <i class="x_down"></i>
                        </a>
                        <div class="social-listing inner">
                            <ul>
                                <li><a href="{{ isset($site_wide_data['facebook']) ? $site_wide_data['facebook'] : 'javascript:;' }}"><i class="x_facebook"></i></a></li>
                                <li><a href="{{ isset($site_wide_data['twitter']) ? $site_wide_data['twitter'] : 'javascript:;' }}"><i class="x_twitter"></i></a></li>
                                <li><a href="{{ isset($site_wide_data['instagram']) ? $site_wide_data['instagram'] : 'javascript:;' }}"><i class="x_instagram"></i></a></li>
                                <li><a href="{{ isset($site_wide_data['linked_in']) ? $site_wide_data['linked_in'] : 'javascript:;' }}"><i class="x_linkedin"></i></a></li>
                            </ul>

                            <img src="build/images/placeholder.webp" data-src="build/images/webp/payments.webp" alt="">
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" class="accordian-toggle"> Contact Us
                            <i class="x_down"></i>
                        </a>
                        <div class="inner">
                            @if(isset($contacts))
                                @foreach($contacts as $contact)
                                    @if(!empty($contact['address']))
                                        {!! $contact['address'] !!}
                                    @endif
                                    @if(!empty($contact['phone_number']))
                                        <p>Phone : {!! $contact['phone_number'] !!}</p>
                                    @endif
                                    @if(!empty($contact['email']))
                                        <p>E-mail : {!! $contact['email'] !!}</p>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" class="accordian-toggle">{{ trans('sentence.signup_newsletter'); }}
                            <i class="x_down"></i>
                        </a>
                        <div class="inner">
                            <form class="footer-subscribe">
                                <div class="inptWrpr btnWrpr">
                                    <input type="email" placeholder="Email Adress">
                                </div>
                                <div class="inptWrpr btnWrpr">
                                    <button type="submit" class="sbmtBtn">{{ trans('sentence.subscribe_button'); }}</button>
                                </div>
                                <!-- <div class="info">Please enter your valid email</div> -->
                            </form>

                        </div>
                    </li>
                </ul>
            </div>

            <div class="footer-links">
                <ul>
                    <li><a href="{{config('app.app_path')}}/">Home</a></li>
                    <li><a href="{{config('app.app_path')}}/shop">Shop</a></li>
                    <li><a href="{{config('app.app_path')}}/blog">Blog</a></li>
                    <li><a href="{{config('app.app_path')}}/">Media</a></li>
                    <li><a href="{{config('app.app_path')}}/contact">Contact Us</a></li>
                    <li><a href="{{config('app.app_path')}}/">Features</a></li>
                    <li><a href="{{config('app.app_path')}}/about">About us</a></li>
                    <li><a href="{{config('app.app_path')}}/page/privacy-policy">Privacy Policy</a></li>
                    <li><a href="{{config('app.app_path')}}/page/terms-and-condition">Terms & Conditions</a></li>
                    <li><a href="{{config('app.app_path')}}/page/return-policy">Return Policy</a></li>
                    <li><a href="{{config('app.app_path')}}/advertise-here">Advertise With Us</a></li>
                </ul>
                <p class="copyRight">{!! trans('sentence.footer_copyright'); !!}</p>
            </div>
        </div>
    </div>
</footer>
</main>

</body>
</html>
<?php
    $ob_get_clean_css = ob_get_clean();
    $cssmain  = preg_replace(array('/ {2,}/', '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'), array(' ', ''), $ob_get_clean_css);
    echo $cssmain;
?>

@if (\App::environment('production'))
    <script src="{{ secure_asset('build/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ secure_asset('build/js/all.js')}}"></script>
    <script src="{{ secure_asset('build/js/swiper.min.js')}}"></script>
    <script src="{{ secure_asset('build/js/outside.js')}}"></script>
@else
    <script src="{{ asset('build/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('build/js/all.js')}}"></script>
    <script src="{{ asset('build/js/swiper.min.js')}}"></script>
    <script src="{{ asset('build/js/outside.js')}}"></script>
@endif


    <script type="text/javascript">

        $(document).ready(function(){
            $(".update-cart").click(function (e) {
                e.preventDefault();

                var ele = $(this);

                if(ele.parents(".cart-item").find(".quantity").val()<=0){
                    alert("Invalid Quantity");
                } else {
                    $.ajax({
                        url: '{{ url('update-cart') }}',
                        method: "patch",
                        data: {
                            _token: '{{ csrf_token() }}',
                            id: ele.attr("data-id"),
                            quantity: ele.parents(".cart-item").find(".quantity").val()
                        },
                        success: function (response) {
                            window.location.reload();
                        }
                    });
                }
            });

            $(".remove-from-cart").click(function (e) {
                e.preventDefault();

                var ele = $(this);

                if(confirm("Are you sure")) {
                    $.ajax({
                        url: '{{ url('remove-from-cart') }}',
                        method: "DELETE",
                        data: {
                            _token: '{{ csrf_token() }}',
                            id: ele.attr("data-id")
                        },
                        success: function (response) {
                            window.location.reload();
                        }
                    });
                }
            });

            $(".product-qty").blur(function (e) {
                e.preventDefault();

                var ele = $(this);

                if(ele.parents(".cart-item").find(".quantity").val()<=0){
                    alert("Invalid Quantity");
                } else {
                    $.ajax({
                        url: '{{ url('update-cart') }}',
                        method: "patch",
                        data: {
                            _token: '{{ csrf_token() }}',
                            id: ele.attr("data-id"),
                            quantity: ele.parents(".cart-item").find(".quantity").val()
                        },
                        success: function (response) {
                            window.location.reload();
                        }
                    });
                }
            });

            $('.searchbar-input').on('keyup',function(){
                $value=$(this).val();
                $.ajax({
                    url : '{{ URL('search') }}',
                    method : 'get',
                    data:{
                        'search':$value
                    },
                    success:function(data){
                        $('.search-result .grid-column-3').html(data);
                    }
                });
            });
        });

        const handleBlur = ({ target }) => {
            if(!+target.value || +target.value < 0) {
                target.value = 0
            }
        }
    </script>
