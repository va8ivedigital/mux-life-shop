@extends('web.layouts.app')
@section('content')
    <div class="main">
        @if(isset($banners))
            <div class="section padding-top-none">
                <div class="container-fluid">
                    <!-- Home Top Slider Section starts Here: home_top_slider.scss -->
                        <div class="swiper mySwiper home-top-slider">
                            <div class="swiper-wrapper">
                                @foreach($banners as $banner)
                                    <div class="swiper-slide slide" data-img-url="{{config('app.image_path')}}/{{ isset($banner['image']['url']) ? $banner['image']['url'] : '' }}">
                                        <a href="{{ $banner['link'] }}" target="_blank" title="{{ isset($banner['title']) ? $banner['title'] : '' }}">
                                            <picture>
                                                <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($banner['banner_image']) ? $banner['banner_image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="{{ isset($banner['title']) ? $banner['title'] : ''}}"/>
                                            </picture>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    <!-- Home Top Slider Section Ends Here: home_top_slider.scss -->
                </div>
            </div>
        @else
            <div class="section">
                <div class="error-main-wrap">
                    <h2 class="title">404</h2>
                    <p class="sub-title">UH OH! You're lost.</p>
                    <p class="caption">The page you are looking for does not exist. How you got here is a mystery. But you can click the button below to go back to the homepage.
                    </p>
                    <div class="flex Vhc">
                        <a href="{{config('app.app_path')}}/" class="custom-btn blue">
                            HOME
                        </a>
                    </div>
                </div>
            </div>
        @endif

        <div class="container">
            @if(isset($featuredProducts))
                <div class="section">
                    <div class="heading-wrapper">
                        <h2 class="top-heading">{{ trans( 'sentence.home_weekly_heading' ) }}</h2>
                        <p class="sub-heading">{{ trans( 'sentence.home_weekly_sub' ) }}</p>
                    </div>

                    <!-- global product Slider Styling starts Here: global-slider-js.scss -->
                    <div class="globalSlider productSliderJs ">
                        <div class="swiper product-slider ">
                            <div class="swiper-wrapper">
                                @foreach($featuredProducts as $product)
                                    <div class="swiper-slide slide">
                                        <!-- product card style starts Here: product_card.scss-->
                                        <div class="product-card">
                                            <div class="product-card-inner">
                                                @if($product['sale']!=0)
                                                    <div class="sale">SALE</div>
                                                @endif
                                                <div class="wishlist">
                                                    <a href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}" class="view">
                                                        <i class="x_eye"></i>
                                                    </a>
                                                    {{-- Favourite will show after login --}}
                                                    <a href="javaScript:;" class="add-to-fav">
                                                        <i class="x_favorite"></i>
                                                    </a>
                                                </div>
                                                <a class="product-image-wrap" href="javascript:;">
                                                    <picture>
                                                        <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($product['image']) ? $product['image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="170" height="170"/>
                                                    </picture>
                                                </a>
                                                <div class="product-detail">
                                                    <div class="rating-cart">
                                                        <div class="rating">
                                                            <input type="radio" name="georgina" value="1">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="2">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="3">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="4">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="5">
                                                            <label class="full"></label>
                                                        </div>
                                                        <a href="{{ config('app.app_path') }}/?id={{ $product['id'] }}" class="add-to-cart">ADD TO CART</a>
                                                    </div>
                                                    <a class="product-title" href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}">{{ $product['title'] }}</a>
                                                    <div class="product-pricing">
                                                        @if(!empty($product['old_price']) || $product['old_price']!=0 || $product['old_price'] != '' || $product['old_price'] != NULL)
                                                            <span class="discount">Rs. {{ number_format($product['old_price'], 2) }}</span>
                                                        @endif
                                                        <span>Rs. {{ number_format($product['price'], 0) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product card style Ends Here-->
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                    <!-- global product Slider Styling Ends Here-->
                </div>
            @endif

            @if(isset($fetchProductCategories))
                <div class="section">
                    <div class="grid-column-3 home-product-listing">
                        <!--fancy product page CTA starts Here: call_to_action.scss -->
                        @foreach($fetchProductCategories as $productCategory)
                            <div class="product-cta">
                                <div class="image-container">
                                    <picture>
                                        <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($productCategory['product_category_image']) ? $productCategory['product_category_image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="366" height="261">
                                    </picture>
                                </div>
                                <a href="{{ config('app.app_path') }}/product-categories/{{ $productCategory['slug'] }}" class="content">
                                    <h3 class="title">{{ $productCategory['name'] }}</h3>
                                    <div class="view-all-btn">
                                        View Collection <i class="x_right"></i>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        <!--fancy product page CTA Ends Here-->
                    </div>
                </div>
            @endif

            @if(isset($fetchsmartSwitchCategory))
                <div class="section">
                    <div class="heading-wrapper">
                        <h2 class="top-heading">Smart Switches</h2>
                        <p class="sub-heading">Make your lights and fans smart!</p>
                    </div>

                    <!-- global product Slider Styling starts Here: global-slider-js.scss -->
                    <div class="globalSlider productSliderJs ">
                        <div class="swiper product-slider ">
                            <div class="swiper-wrapper">
                                @foreach($fetchsmartSwitchCategory as $product)
                                    <div class="swiper-slide slide">
                                        <!-- product card style starts Here: product_card.scss-->
                                        <div class="product-card">
                                            <div class="product-card-inner">
                                                @if($product['sale']!=0)
                                                    <div class="sale">SALE</div>
                                                @endif
                                                <div class="wishlist">
                                                    <a href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}" class="view">
                                                        <i class="x_eye"></i>
                                                    </a>
                                                    {{-- Favourite will show after login --}}
                                                    <a href="javaScript:;" class="add-to-fav">
                                                        <i class="x_favorite"></i>
                                                    </a>
                                                </div>
                                                <a class="product-image-wrap" href="javascript:;">
                                                    <picture>
                                                        <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($product['image']) ? $product['image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="170" height="170"/>
                                                    </picture>
                                                </a>
                                                <div class="product-detail">
                                                    <div class="rating-cart">
                                                        <div class="rating">
                                                            <input type="radio" name="georgina" value="1">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="2">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="3">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="4">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="5">
                                                            <label class="full"></label>
                                                        </div>
                                                        <a href="{{ config('app.app_path') }}/?id={{ $product['id'] }}" class="add-to-cart">ADD TO CART</a>
                                                    </div>
                                                    <a class="product-title" href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}">{{ $product['title'] }}</a>
                                                    <div class="product-pricing">
                                                        @if(!empty($product['old_price']) || $product['old_price']!=0 || $product['old_price'] != '' || $product['old_price'] != NULL)
                                                            <span class="discount">Rs. {{ number_format($product['old_price'], 2) }}</span>
                                                        @endif
                                                        <span>Rs. {{ number_format($product['price'], 0) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product card style Ends Here-->
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                    <!-- global product Slider Styling Ends Here-->
                </div>
            @endif
        </div>

        @if(isset($site_wide_data['home_banner_image']))
            <div class="section">
                <div class="container-fluid">
                    <!--Call to action banner starts Here: call_to_action.scss -->
                    <div class="call-to-action">
                        <a href="javascript:;" class="flex">
                            <picture>
                                <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($site_wide_data['home_banner_image']) ? $site_wide_data['home_banner_image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="Call to action banner" width="1920" height="410">
                            </picture>
                        </a>
                    </div>
                    <!--Call to action banner Ends Here -->
                </div>
            </div>
        @endif

        <div class="container">

            <!-- Smart Locks Section -->
            @if(isset($fetchsmartLockCategory))
                <div class="section">
                    <div class="heading-wrapper">
                        <h2 class="top-heading">Smart Locks</h2>
                        <p class="sub-heading">Automate your doors smartly!</p>
                    </div>

                    <!-- global product Slider Styling starts Here: global-slider-js.scss -->
                    <div class="globalSlider productSliderJs ">
                        <div class="swiper product-slider ">
                            <div class="swiper-wrapper">
                                @foreach($fetchsmartLockCategory as $product)
                                    <div class="swiper-slide slide">
                                        <!-- product card style starts Here: product_card.scss-->
                                        <div class="product-card">
                                            <div class="product-card-inner">
                                                @if($product['sale']!=0)
                                                    <div class="sale">SALE</div>
                                                @endif
                                                <div class="wishlist">
                                                    <a href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}" class="view">
                                                        <i class="x_eye"></i>
                                                    </a>
                                                    {{-- Favourite will show after login --}}
                                                    <a href="javaScript:;" class="add-to-fav">
                                                        <i class="x_favorite"></i>
                                                    </a>
                                                </div>
                                                <a class="product-image-wrap" href="javascript:;">
                                                    <picture>
                                                        <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($product['image']) ? $product['image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="170" height="170"/>
                                                    </picture>
                                                </a>
                                                <div class="product-detail">
                                                    <div class="rating-cart">
                                                        <div class="rating">
                                                            <input type="radio" name="georgina" value="1">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="2">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="3">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="4">
                                                            <label class="full RateActive"></label>
                                                            <input type="radio" name="georgina" value="5">
                                                            <label class="full"></label>
                                                        </div>
                                                        <a href="{{ config('app.app_path') }}/?id={{ $product['id'] }}" class="add-to-cart">ADD TO CART</a>
                                                    </div>
                                                    <a class="product-title" href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}">{{ $product['title'] }}</a>
                                                    <div class="product-pricing">
                                                        @if(!empty($product['old_price']) || $product['old_price']!=0 || $product['old_price'] != '' || $product['old_price'] != NULL)
                                                            <span class="discount">Rs. {{ number_format($product['old_price'], 2) }}</span>
                                                        @endif
                                                        <span>Rs. {{ number_format($product['price'], 0) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product card style Ends Here-->
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                    <!-- global product Slider Styling Ends Here-->
                </div>
            @endif
            <!-- Smart Locks Section -->

            @if(isset($reviews))
                <!-- User Reviews Section -->
                    <div class="section">
                        <div class="heading-wrapper">
                            <h2 class="top-heading">{{ trans('sentence.user_reviews_heading') }}</h2>
                            <p class="sub-heading">{{ trans('sentence.user_reviews_sub') }}</p>
                        </div>
                        <!-- global product Slider Styling starts Here: global-slider-js.scss -->
                        <div class="globalSlider userSliderJs">
                            <div class="swiper product-slider ">
                                <div class="swiper-wrapper">
                                    @foreach($reviews as $review)
                                        <!--User review Home starts Here: home_user-reviews.scss-->
                                        <div class="swiper-slide slide user-review-card">
                                            <?php include('build/images/quotes.svg'); ?>
                                            <div class="content">
                                                <p>{!! $review['user_review'] !!}</p>
                                                {{-- <span>Keep it up</span> --}}
                                            </div>
                                            <div class="user-name">
                                                {{ $review['title'] }}
                                            </div>
                                        </div>
                                        <!--User review Home Ends Here-->
                                    @endforeach
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                        <!-- global product Slider Styling Ends Here-->
                    </div>
                <!-- User Reviews Section -->
            @endif
            @if(isset($singleRandomPopularBlog) || !empty($singleRandomPopularBlog))
                <!-- Our Latest News  Section -->
                    <div class="section">
                        <div class="heading-wrapper">
                            <h2 class="top-heading">{{ trans('sentence.latest_news_heading') }}</h2>
                            <p class="sub-heading">{{ trans('sentence.latest_news_sub') }}</p>
                        </div>

                        <div class="flex column-offset home-latest-news">
                            <div class="wide-column large">
                                @foreach($singleRandomPopularBlog as $singleRandomBlog)
                                    <!--Home Blog Section starts Here :home_latest_news_cta.scss -->
                                        <div class="latest-news-cta">
                                            <div class="image-container">
                                                <picture>
                                                    <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($singleRandomBlog['blog_image']) ? $singleRandomBlog['blog_image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="366" height="261">
                                                </picture>
                                            </div>
                                            <a href="{{ config('app.app_path') }}/blog/{{ $singleRandomBlog['slug'] }}" class="content">
                                                <h3 class="title">{!! $singleRandomBlog['title'] !!}</h3>
                                                <ul>
                                                    <li>@php echo date("M j, Y", strtotime($singleRandomBlog['created_at'])); @endphp</li>
                                                    <li>No Comments</li>
                                                </ul>
                                            </a>
                                        </div>
                                    <!--Home Blog Section Ends Here -->
                                @endforeach
                            </div>
                            <div class="short-column large">
                                @foreach ($popularBlogs as $popularBlog)
                                    <!--Home Blog Section starts Here :home_latest_news_cta.scss -->
                                        <div class="latest-news-cta small">
                                            <div class="image-container">
                                                <picture>
                                                    <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($popularBlog['blog_image']) ? $popularBlog['blog_image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="366" height="261">
                                                </picture>
                                            </div>
                                            <a href="javascript:;" class="content">
                                                <a href="{{ config('app.app_path') }}/blog/{{ $popularBlog['slug'] }}" class="content">
                                                    <h3 class="title">{!! $popularBlog['title'] !!}</h3>
                                                    <ul>
                                                        <li>@php echo date("M j, Y", strtotime($popularBlog['created_at'])); @endphp</li>
                                                        <li>No Comments</li>
                                                    </ul>
                                                </a>
                                            </a>
                                        </div>
                                    <!--Home Blog Section Ends Here -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                <!-- Our Latest News Section -->
            @endif

            <!-- Our Clients Section  -->
                @if(!empty($clients))
                    <div class="section">
                        <div class="heading-wrapper">
                            <h2 class="top-heading">{{ trans('sentence.our_clients_heading'); }}</h2>
                            <p class="sub-heading">{{ trans('sentence.our_clients_sub'); }}</p>
                        </div>
                        <div class="ourClient ourClientJs">
                            <div class="swiper product-slider ">
                                <div class="swiper-wrapper">
                                    @foreach($clients as $client)
                                        <div class="swiper-slide slide" data-img-url="{{config('app.image_path')}}/{{ isset($client['image']['url']) ? $client['image']['url'] : '' }}">
                                            <div class="client-logo-card">
                                                <div class="image-container">
                                                    <picture>
                                                        <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($client['client_logo']) ? $client['client_logo'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="{{ isset($client['title']) ? $client['title'] : ''}}" title="{{ isset($client['title']) ? $client['title'] : ''}}" width="182" height="182"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach;
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                @endif
            <!-- Our Clients Section  -->
        </div>
    </div>

@endsection
