@extends('web.layouts.app')
<meta name="_token" content="{{csrf_token()}}" />
@section('content')
    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">cart</h1>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><span>Product</span></li>
                </ul>
            </div>
        </div>

            <section class="section">
                <div class="container">
                    @if(session('cart'))
                        <div class="cart-box">
                            <div class="products-wrp">
                                <div class="cart-heading">
                                    <span>Product</span>
                                    <span>Item price</span>
                                    <span>Qty</span>
                                    <span>Total</span>
                                </div>
                                    <?php $total = 0; ?>
                                    <?php $total_items = 0; ?>
                                    @foreach(session('cart') as $id => $details)
                                        <?php $total += $details['price'] * $details['quantity'] ?>
                                        <?php $total_items++; ?>
                                        <div class="cart-item">
                                            <i class="x_close remove-item"></i>
                                            <div class="cart-item-image">
                                                <img src="{{ $details['photo'] }}" data-src="{{ $details['photo'] }}" alt="{!! $details['name'] !!}" width="90" height="90">
                                            </div>
                                            <span class="product" data-title="Product">
                                                <div class="product-inner">
                                                    <h4 class="product-title">{!! $details['name'] !!}</h4>
                                                    <p class="product-options">
                                                        {{-- <button>Update&nbsp;|&nbsp;</button> --}}
                                                        <button class="remove-from-cart" data-id="{{ $id }}">remove</button>
                                                    </p>
                                                </div>
                                            </span>
                                            <span class="price" data-title="Price">Rs.{{ number_format($details['price'], 0) }}</span>
                                            <span class="qty" data-title="Quantity">
                                                <div class="qty-adjust">
                                                    <button class="decrease update-cart" data-id="{{ $id }}">-</button>
                                                    <input class="product-qty quantity" type="number" value="{{ $details['quantity'] }}" data-id="{{ $id }}">
                                                    <button class="increase update-cart" data-id="{{ $id }}">+</button>
                                                </div>
                                            </span>
                                            <span class="total" data-title="Total">Rs.{{ number_format($details['price'] * $details['quantity'], 0) }}</span>
                                        </div>
                                    @endforeach

                            </div>
                            <div class="purchase">
                                <div class="overview">
                                    <div class="head">
                                        <h5 class="title">
                                            OVERVIEW
                                        </h5>
                                        <span class="item-no">{{ $total_items }} ITEM</span>
                                    </div>
                                    <div class="order-details">
                                        <div class="detail">
                                            <span class="option">Subtotal</span>
                                            <span class="value">Rs.{{ number_format($total ,0) }}</span>
                                        </div>
                                        <div class="detail">
                                            <span class="option">tax</span>
                                            <span class="value">Calculated at checkout</span>
                                        </div>
                                        <div class="detail">
                                            <span class="option">delivery cost</span>
                                            <span class="value">Rs. 200</span>
                                        </div>
                                        <div class="detail">
                                            <span class="option">delievery options</span>
                                            <span class="value">COD</span>
                                        </div>
                                    </div>
                                    <div class="foot">
                                        <h5 class="title">
                                            order total
                                        </h5>
                                        <span class="total">Rs.{{ number_format($total+200 ,0) }}</span>
                                    </div>
                                    <a href="{{ url('checkout') }}" class="checkout-btn black-default">
                                        proceed to checkout
                                    </a>
                                </div>
                                <div class="discount-code">
                                    <p class="title">Add discount code</p>
                                    <div class="code">
                                        <input type="text" placeholder="Enter your code" />
                                        <a href="javascript:;" class="apply-btn black-default">apply</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <h1>Add Items in cart first</h1>
                    @endif
                </div>
            </section>
    </div>
@endsection
