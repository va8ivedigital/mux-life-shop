@extends('web.layouts.app')
@section('content')
    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">Register Confirmation</h1>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><span>Register Confirmation</span></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="section">
                <div class="heading-wrapper">
                    <h2 class="top-heading">THANKS FOR REGISTERING {{ $customer_name }}</h2>
                    <br>
                    <p class="sub-heading grey">You have successfully created an account for <a href="mailto:{{ $customer_email }}">{{ $customer_email }}</a>.</p>
                    <br>
                    <p class="sub-heading grey">We handle your personal information with care. View our <a href="{{config('app.app_path')}}/page/privacy-policy">privacy statement</a> to learn how we handle your data</p>
                    <br>
                    <div class="flex Vhc rowbar10">
                        <a href="{{ url('/customer/login') }}" class="btn-secondary black">CONTINUE SHOPPING</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
