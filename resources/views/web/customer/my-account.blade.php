@extends('web.layouts.app')
@section('content')
{{-- @dd($allloggedinn) --}}

{{-- MY ACCOUNT PAGE
@if(Auth::guard('customer')->user())
<a href="{{ config('app.app_path') }}/customer/logout">Logout</a>
@else
<a href="{{ config('app.app_path') }}/customer/login">Login</a>
@endif --}}

<div class="main">
    <div class="breadcrumb">
        <div class="container">
            <h1 class="title">my profile</h1>
            <ul>
                <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                <li><span>My Profile</span></li>
            </ul>
        </div>
    </div>
    <section class="section">
        <div class="container">
            @if(Session::has('order_created'))
                <div class="inputWrapper column-1">
                    <p style="font-size: x-large; font-weight: 700; margin-bottom: 30px;">{{ Session::get('order_created') }}</p>
                </div>
            @endif
            <div class="tabs-wrapper">
                <div class="tabs">
                    <span><a href="{{ config('app.app_path') }}/customer/my-account">my profile</a></span>
                    <span>my profile</span>
                    <span>my profile</span>
                    <span>my profile</span>
                    <span>my profile</span>
                    <span><a href="{{ config('app.app_path') }}/customer/logout">Logout</a></span>
                </div>
                <div class="details">
                    <div class="head">my profile</div>
                    <div class="contact-info">
                        <h2 class="top-heading">CONTACT INFORMATION</h2>
                        <form action="">
                            <input type="text" placeholder="First Name">
                            <input type="text" placeholder="Last Name">
                            <input type="email" placeholder="Email">
                            <button class="black">change password</button>
                        </form>
                        <a href="{{ route('customer.add-new-billing') }}" class="black">Add New Billing Address</a>
                        <a href="{{ route('customer.add-new-shipping') }}" class="black">Add New Shipping Address</a>
                        <a href="{{ route('customer.manage-addresses') }}" class="black">Manage Addresses</a>
                    </div>
                    <form action="" class="birth">

                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
