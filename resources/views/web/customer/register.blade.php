@extends('web.layouts.app')
@section('content')
    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">Register Here</h1>
                <ul>
                    <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                    <li><span>Register Here</span></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="section">
                <div class="heading-wrapper">
                    <h2 class="top-heading left">Register Here</h2>
                </div>
                    @if(Session::has('error_message'))
                        <div class="inputWrapper column-1">
                            <p style="font-size: x-large; font-weight: 700; margin-bottom: 30px;">{{ Session::get('error_message') }}</p>
                        </div>
                    @endif
                    <form action="{{ url('/register') }}" method="post" class="form-wrap">
                        @csrf
                        <div class="inputWrapper column-3">
                            <input type="text" name="first_name" value="" placeholder="First name" required="">
                        </div>
                        <div class="inputWrapper column-3">
                            <input type="text" name="last_name" value="" placeholder="Last Name" required="">
                        </div>
                        <div class="inputWrapper column-1">
                            <input type="email" name="email" value="" placeholder="Email Address" required="">
                        </div>
                        <div class="inputWrapper column-1">
                            <input type="password" name="password" value="" placeholder="Password" required="">
                        </div>
                        <div class="inputWrapper column-1">
                            <input type="password" name="name" value="" placeholder="Confirm Password" required="">
                        </div>
                        <div class="inputWrapper column-1">
                            <label>Date of Birth</label>
                            <div class="flex offset10">
                                <div class="column-6">
                                    <select name="day">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                                <div class="column-4">
                                    <select name="month">
                                        <option value="Jan">January</option>
                                        <option value="Feb">February</option>
                                        <option value="Mar">March</option>
                                        <option value="Apr">April</option>
                                        <option value="May">May</option>
                                        <option value="June">June</option>
                                        <option value="July">July</option>
                                        <option value="Aug">August</option>
                                        <option value="Sept">September</option>
                                        <option value="Oct">October</option>
                                        <option value="Nov">November</option>
                                        <option value="Dec">December</option>
                                    </select>
                                </div>
                                <div class="column-6">
                                    <select name="year">
                                        <option value="1970">1970</option>
                                        <option value="1971">1971</option>
                                        <option value="1972">1972</option>
                                        <option value="1973">1973</option>
                                        <option value="1974">1974</option>
                                        <option value="1975">1975</option>
                                        <option value="1976">1976</option>
                                        <option value="1977">1977</option>
                                        <option value="1978">1978</option>
                                        <option value="1979">1979</option>
                                        <option value="1980">1980</option>
                                        <option value="1981">1981</option>
                                        <option value="1982">1982</option>
                                        <option value="1983">1983</option>
                                        <option value="1984">1984</option>
                                        <option value="1985">1985</option>
                                        <option value="1986">1986</option>
                                        <option value="1987">1987</option>
                                        <option value="1988">1988</option>
                                        <option value="1989">1989</option>
                                        <option value="1990">1990</option>
                                        <option value="1991">1991</option>
                                        <option value="1992">1992</option>
                                        <option value="1993">1993</option>
                                        <option value="1994">1994</option>
                                        <option value="1995">1995</option>
                                        <option value="1996">1996</option>
                                        <option value="1997">1997</option>
                                        <option value="1998">1998</option>
                                        <option value="1999">1999</option>
                                        <option value="2000">2000</option>
                                        <option value="2001">2001</option>
                                        <option value="2002">2002</option>
                                        <option value="2003">2003</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option>
                                        <option value="2008">2008</option>
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="chBxWr column-1">
                            <span class="checkBox">
                                <input class="checkbox" id="remember" type="checkbox" name="code" />
                                <label for="remember">I've read & accept the &nbsp; <a href="term_and_condition.php">Terms of Use</a>&nbsp; & &nbsp;<a href="privacy_policy.php">Privacy Policy</a></label>
                            </span>
                        </div>
                        <div class="inputWrapper column-1">
                            <p class="sub-heading left grey">Already have an account? <a href="{{ url('/customer/login') }}">Sign In</a></p>
                        </div>
                        <div class="inputWrapper column-1">
                            <button type="submit" class="black">REGISTER</button>
                        </div>
                        <div class="inputWrapper column-1">
                            <p class="sub-heading fz16 left grey">By registering, you agree to receive information about nutrition, recipes, training and special offers.You can unsubscribe at any time.
                                Further details can be found in our Privacy Policy.</p>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection
