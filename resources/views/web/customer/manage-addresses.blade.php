@extends('web.layouts.app')
@section('content')

    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">Manage Addresses</h1>
                <ul>
                <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                <li><span>Manage Addresses</span></li>
                </ul>
            </div>
        </div>
        <section class="section">
            <div class="container">
                <div class="tabs-wrapper">
                    <div class="tabs">
                        <span><a href="{{ config('app.app_path') }}/customer/my-account">my profile</a></span>
                        <span>my profile</span>
                        <span>my profile</span>
                        <span>my profile</span>
                        <span>my profile</span>
                        <span><a href="{{ config('app.app_path') }}/customer/logout">Logout</a></span>
                    </div>
                    <div class="details">
                        <div class="head">Addresses
                        @if(Session::has('address_assigned_successfully'))
                            <div>
                                <p style="font-size: x-large; font-weight: 700; margin-bottom: 30px;">{{ Session::get('address_assigned_successfully') }}</p>
                            </div>
                        @endif</div>
                        <div class="contact-info">
                            <h2 class="top-heading">BILLING ADDRESSES</h2>
                            <table width="100%">
                                <thead>
                                    <th>Sr.No.</th>
                                    <th>Full Name</th>
                                    <th>Company</th>
                                    <th>Phone #</th>
                                    <th>Address Line 1</th>
                                    <th>Address Line 2</th>
                                    <th>Zip Code</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($billingAddress as $address)
                                        <tr>
                                            <td>{{ $i++; }}.</td>
                                            <td>{{ $address['first_name'] }} {{ $address['last_name'] }}</td>
                                            <td>{{ $address['company'] }}</td>
                                            <td>{{ $address['phone_number'] }}</td>
                                            <td>{{ $address['address_1'] }}</td>
                                            <td>{{ $address['address_2'] }}</td>
                                            <td>{{ $address['zip_code'] }}</td>
                                            <td>{{ $address['city'] }}</td>
                                            <td>{{ $address['state'] }}</td>
                                            <td>{{ $address['country'] }}</td>
                                            <td>
                                                @if($address['id'] == $address['customer']['billing_address'])
                                                    <button type="button" class="black">Currently Active</button>
                                                @else
                                                    <form action="{{ route('customer.assign-billing-address') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" value="{{ $address['id'] }}" name="billing_address_id"/>
                                                    <button type="submit" class="black">Set Active</button>
                                                    </form>
                                                @endif
                                            </td>
                                        <tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="contact-info">
                            <h2 class="top-heading">SHIPPING ADDRESSES</h2>
                            <table width="100%">
                                <thead>
                                    <th>Sr.No.</th>
                                    <th>Full Name</th>
                                    <th>Company</th>
                                    <th>Phone #</th>
                                    <th>Address Line 1</th>
                                    <th>Address Line 2</th>
                                    <th>Zip Code</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($shippingAddress as $address)
                                        <tr>
                                            <td>{{ $i++; }}.</td>
                                            <td>{{ $address['first_name'] }} {{ $address['last_name'] }}</td>
                                            <td>{{ $address['company'] }}</td>
                                            <td>{{ $address['phone_number'] }}</td>
                                            <td>{{ $address['address_1'] }}</td>
                                            <td>{{ $address['address_2'] }}</td>
                                            <td>{{ $address['zip_code'] }}</td>
                                            <td>{{ $address['city'] }}</td>
                                            <td>{{ $address['state'] }}</td>
                                            <td>{{ $address['country'] }}</td>
                                            <td>
                                                @if($address['id'] == $address['customer']['shipping_address'])
                                                    <button type="button" class="black">Currently Active</button>
                                                @else
                                                    <form action="{{ route('customer.assign-shipping-address') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" value="{{ $address['id'] }}" name="shipping_address_id"/>
                                                    <button type="submit" class="black">Set Active</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
