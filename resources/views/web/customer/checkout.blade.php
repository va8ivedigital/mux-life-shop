@extends('web.layouts.app')
@section('content')

<section class="section">
    <div class="container">
        <form action="{{ url('/process-checkout') }}" method="post">
            <div class="flex rowbar">
                <div class="wide-column large">
                    <h3 class="top-heading left">Billing details</h3>
                    <div class="billing-detail">
                        @foreach($billingAddress as $address)
                                @csrf
                                <div class="name">
                                    <div class="input-wrp">
                                        <p class="title">first name</p>
                                        <input type="text" name="first_name" value="{{ $address['first_name'] }}" required>
                                    </div>
                                    <div class="input-wrp">
                                        <p class="title">last name</p>
                                        <input type="text" name="last_name" value="{{ $address['last_name'] }}" required>
                                    </div>
                                </div>
                                <div class="input-wrp">
                                    <p class="title opt">company name</p>
                                    <input type="text" name="company" value="{{ $address['company'] }}" id="">
                                </div>
                                <div class="input-wrp">
                                    <p class="title">country / region</p>
                                    @php
                                        $countries_list = array(
                                            "AF" => "Afghanistan",
                                            "AX" => "Aland Islands",
                                            "AL" => "Albania",
                                            "DZ" => "Algeria",
                                            "AS" => "American Samoa",
                                            "AD" => "Andorra",
                                            "AO" => "Angola",
                                            "AI" => "Anguilla",
                                            "AQ" => "Antarctica",
                                            "AG" => "Antigua and Barbuda",
                                            "AR" => "Argentina",
                                            "AM" => "Armenia",
                                            "AW" => "Aruba",
                                            "AU" => "Australia",
                                            "AT" => "Austria",
                                            "AZ" => "Azerbaijan",
                                            "BS" => "Bahamas",
                                            "BH" => "Bahrain",
                                            "BD" => "Bangladesh",
                                            "BB" => "Barbados",
                                            "BY" => "Belarus",
                                            "BE" => "Belgium",
                                            "BZ" => "Belize",
                                            "BJ" => "Benin",
                                            "BM" => "Bermuda",
                                            "BT" => "Bhutan",
                                            "BO" => "Bolivia",
                                            "BQ" => "Bonaire, Sint Eustatius and Saba",
                                            "BA" => "Bosnia and Herzegovina",
                                            "BW" => "Botswana",
                                            "BV" => "Bouvet Island",
                                            "BR" => "Brazil",
                                            "IO" => "British Indian Ocean Territory",
                                            "BN" => "Brunei Darussalam",
                                            "BG" => "Bulgaria",
                                            "BF" => "Burkina Faso",
                                            "BI" => "Burundi",
                                            "KH" => "Cambodia",
                                            "CM" => "Cameroon",
                                            "CA" => "Canada",
                                            "CV" => "Cape Verde",
                                            "KY" => "Cayman Islands",
                                            "CF" => "Central African Republic",
                                            "TD" => "Chad",
                                            "CL" => "Chile",
                                            "CN" => "China",
                                            "CX" => "Christmas Island",
                                            "CC" => "Cocos (Keeling) Islands",
                                            "CO" => "Colombia",
                                            "KM" => "Comoros",
                                            "CG" => "Congo",
                                            "CD" => "Congo, Democratic Republic of the Congo",
                                            "CK" => "Cook Islands",
                                            "CR" => "Costa Rica",
                                            "CI" => "Cote D'Ivoire",
                                            "HR" => "Croatia",
                                            "CU" => "Cuba",
                                            "CW" => "Curacao",
                                            "CY" => "Cyprus",
                                            "CZ" => "Czech Republic",
                                            "DK" => "Denmark",
                                            "DJ" => "Djibouti",
                                            "DM" => "Dominica",
                                            "DO" => "Dominican Republic",
                                            "EC" => "Ecuador",
                                            "EG" => "Egypt",
                                            "SV" => "El Salvador",
                                            "GQ" => "Equatorial Guinea",
                                            "ER" => "Eritrea",
                                            "EE" => "Estonia",
                                            "ET" => "Ethiopia",
                                            "FK" => "Falkland Islands (Malvinas)",
                                            "FO" => "Faroe Islands",
                                            "FJ" => "Fiji",
                                            "FI" => "Finland",
                                            "FR" => "France",
                                            "GF" => "French Guiana",
                                            "PF" => "French Polynesia",
                                            "TF" => "French Southern Territories",
                                            "GA" => "Gabon",
                                            "GM" => "Gambia",
                                            "GE" => "Georgia",
                                            "DE" => "Germany",
                                            "GH" => "Ghana",
                                            "GI" => "Gibraltar",
                                            "GR" => "Greece",
                                            "GL" => "Greenland",
                                            "GD" => "Grenada",
                                            "GP" => "Guadeloupe",
                                            "GU" => "Guam",
                                            "GT" => "Guatemala",
                                            "GG" => "Guernsey",
                                            "GN" => "Guinea",
                                            "GW" => "Guinea-Bissau",
                                            "GY" => "Guyana",
                                            "HT" => "Haiti",
                                            "HM" => "Heard Island and Mcdonald Islands",
                                            "VA" => "Holy See (Vatican City State)",
                                            "HN" => "Honduras",
                                            "HK" => "Hong Kong",
                                            "HU" => "Hungary",
                                            "IS" => "Iceland",
                                            "IN" => "India",
                                            "ID" => "Indonesia",
                                            "IR" => "Iran, Islamic Republic of",
                                            "IQ" => "Iraq",
                                            "IE" => "Ireland",
                                            "IM" => "Isle of Man",
                                            "IL" => "Israel",
                                            "IT" => "Italy",
                                            "JM" => "Jamaica",
                                            "JP" => "Japan",
                                            "JE" => "Jersey",
                                            "JO" => "Jordan",
                                            "KZ" => "Kazakhstan",
                                            "KE" => "Kenya",
                                            "KI" => "Kiribati",
                                            "KP" => "Korea, Democratic People's Republic of",
                                            "KR" => "Korea, Republic of",
                                            "XK" => "Kosovo",
                                            "KW" => "Kuwait",
                                            "KG" => "Kyrgyzstan",
                                            "LA" => "Lao People's Democratic Republic",
                                            "LV" => "Latvia",
                                            "LB" => "Lebanon",
                                            "LS" => "Lesotho",
                                            "LR" => "Liberia",
                                            "LY" => "Libyan Arab Jamahiriya",
                                            "LI" => "Liechtenstein",
                                            "LT" => "Lithuania",
                                            "LU" => "Luxembourg",
                                            "MO" => "Macao",
                                            "MK" => "Macedonia, the Former Yugoslav Republic of",
                                            "MG" => "Madagascar",
                                            "MW" => "Malawi",
                                            "MY" => "Malaysia",
                                            "MV" => "Maldives",
                                            "ML" => "Mali",
                                            "MT" => "Malta",
                                            "MH" => "Marshall Islands",
                                            "MQ" => "Martinique",
                                            "MR" => "Mauritania",
                                            "MU" => "Mauritius",
                                            "YT" => "Mayotte",
                                            "MX" => "Mexico",
                                            "FM" => "Micronesia, Federated States of",
                                            "MD" => "Moldova, Republic of",
                                            "MC" => "Monaco",
                                            "MN" => "Mongolia",
                                            "ME" => "Montenegro",
                                            "MS" => "Montserrat",
                                            "MA" => "Morocco",
                                            "MZ" => "Mozambique",
                                            "MM" => "Myanmar",
                                            "NA" => "Namibia",
                                            "NR" => "Nauru",
                                            "NP" => "Nepal",
                                            "NL" => "Netherlands",
                                            "AN" => "Netherlands Antilles",
                                            "NC" => "New Caledonia",
                                            "NZ" => "New Zealand",
                                            "NI" => "Nicaragua",
                                            "NE" => "Niger",
                                            "NG" => "Nigeria",
                                            "NU" => "Niue",
                                            "NF" => "Norfolk Island",
                                            "MP" => "Northern Mariana Islands",
                                            "NO" => "Norway",
                                            "OM" => "Oman",
                                            "PK" => "Pakistan",
                                            "PW" => "Palau",
                                            "PS" => "Palestinian Territory, Occupied",
                                            "PA" => "Panama",
                                            "PG" => "Papua New Guinea",
                                            "PY" => "Paraguay",
                                            "PE" => "Peru",
                                            "PH" => "Philippines",
                                            "PN" => "Pitcairn",
                                            "PL" => "Poland",
                                            "PT" => "Portugal",
                                            "PR" => "Puerto Rico",
                                            "QA" => "Qatar",
                                            "RE" => "Reunion",
                                            "RO" => "Romania",
                                            "RU" => "Russian Federation",
                                            "RW" => "Rwanda",
                                            "BL" => "Saint Barthelemy",
                                            "SH" => "Saint Helena",
                                            "KN" => "Saint Kitts and Nevis",
                                            "LC" => "Saint Lucia",
                                            "MF" => "Saint Martin",
                                            "PM" => "Saint Pierre and Miquelon",
                                            "VC" => "Saint Vincent and the Grenadines",
                                            "WS" => "Samoa",
                                            "SM" => "San Marino",
                                            "ST" => "Sao Tome and Principe",
                                            "SA" => "Saudi Arabia",
                                            "SN" => "Senegal",
                                            "RS" => "Serbia",
                                            "CS" => "Serbia and Montenegro",
                                            "SC" => "Seychelles",
                                            "SL" => "Sierra Leone",
                                            "SG" => "Singapore",
                                            "SX" => "Sint Maarten",
                                            "SK" => "Slovakia",
                                            "SI" => "Slovenia",
                                            "SB" => "Solomon Islands",
                                            "SO" => "Somalia",
                                            "ZA" => "South Africa",
                                            "GS" => "South Georgia and the South Sandwich Islands",
                                            "SS" => "South Sudan",
                                            "ES" => "Spain",
                                            "LK" => "Sri Lanka",
                                            "SD" => "Sudan",
                                            "SR" => "Suriname",
                                            "SJ" => "Svalbard and Jan Mayen",
                                            "SZ" => "Swaziland",
                                            "SE" => "Sweden",
                                            "CH" => "Switzerland",
                                            "SY" => "Syrian Arab Republic",
                                            "TW" => "Taiwan, Province of China",
                                            "TJ" => "Tajikistan",
                                            "TZ" => "Tanzania, United Republic of",
                                            "TH" => "Thailand",
                                            "TL" => "Timor-Leste",
                                            "TG" => "Togo",
                                            "TK" => "Tokelau",
                                            "TO" => "Tonga",
                                            "TT" => "Trinidad and Tobago",
                                            "TN" => "Tunisia",
                                            "TR" => "Turkey",
                                            "TM" => "Turkmenistan",
                                            "TC" => "Turks and Caicos Islands",
                                            "TV" => "Tuvalu",
                                            "UG" => "Uganda",
                                            "UA" => "Ukraine",
                                            "AE" => "United Arab Emirates",
                                            "GB" => "United Kingdom",
                                            "US" => "United States",
                                            "UM" => "United States Minor Outlying Islands",
                                            "UY" => "Uruguay",
                                            "UZ" => "Uzbekistan",
                                            "VU" => "Vanuatu",
                                            "VE" => "Venezuela",
                                            "VN" => "Viet Nam",
                                            "VG" => "Virgin Islands, British",
                                            "VI" => "Virgin Islands, U.s.",
                                            "WF" => "Wallis and Futuna",
                                            "EH" => "Western Sahara",
                                            "YE" => "Yemen",
                                            "ZM" => "Zambia",
                                            "ZW" => "Zimbabwe"
                                        );
                                    @endphp
                                    <select id="country" required name="country" class="form-control empty">
                                        <option class="option" class="disabled" value="" disabled selected>Country</option>
                                        @php
                                            foreach ($countries_list as $key => $value):
                                                $selected = ($key == $address['country'] ? ' selected' : '');
                                                echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                            endforeach;
                                        @endphp
                                    </select>
                                </div>
                                <div class="input-wrp">
                                    <p class="title">Street Address</p>
                                    <input type="text" placeholder="House number and street name" value="{{ $address['address_1'] }}">
                                    <input type="text" placeholder="Apartment, suite, unit, etc, (Optional)" value="{{ $address['address_2'] }}">
                                </div>
                                <div class="input-wrp">
                                    <p class="title">Town / City </p>
                                    <input type="text" name="city" value="{{ $address['city'] }}">
                                </div>
                                <div class="input-wrp">
                                    <p class="title">Postcode / ZIP </p>
                                    <input type="number" name="zip_code" value="{{ $address['zip_code'] }}">
                                </div>
                                <div class="input-wrp">
                                    <p class="title">Phone</p>
                                    <input type="text" name="phone_number" value="{{ $address['phone_number'] }}">
                                </div>
                                <div class="input-wrp">
                                    <p class="title">Email Address </p>
                                    <input type="email" name="email" value="{{ $address['customer']['email'] }}">
                                </div>

                                {{--
                                    <div class="input-wrp checkbox">
                                    <input type="checkbox" id="shipment" name="ship_to_different" id="">
                                    <label class="title shippment-title">Ship To A Different Address? </label>
                                    </div>
                                    <div class="shipment-address hidden">
                                        <div class="name">
                                            <div class="input-wrp">
                                                <p class="title">first name</p>
                                                <input type="text" name="first_name" required>
                                            </div>
                                            <div class="input-wrp">
                                                <p class="title">last name</p>
                                                <input type="text" name="last_name" required>
                                            </div>
                                        </div>
                                        <div class="input-wrp">
                                            <p class="title opt">company name</p>
                                            <input type="text" name="company_name" id="">
                                        </div>
                                        <div class="input-wrp">
                                            <p class="title">country / region</p>
                                            <select id="country" required name="country" class="form-control empty">
                                                <option class="option" class="disabled" value="" disabled selected>Country</option>
                                                <option class="option" value="Afghanistan">Afghanistan</option>
                                                <option class="option" value="Åland Islands">Åland Islands</option>
                                                <option class="option" value="Albania">Albania</option>
                                                <option class="option" value="Algeria">Algeria</option>
                                                <option class="option" value="American Samoa">American Samoa</option>
                                                <option class="option" value="Andorra">Andorra</option>
                                                <option class="option" value="Angola">Angola</option>
                                                <option class="option" value="Anguilla">Anguilla</option>
                                                <option class="option" value="Antarctica">Antarctica</option>
                                                <option class="option" value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                <option class="option" value="Argentina">Argentina</option>
                                                <option class="option" value="Armenia">Armenia</option>
                                                <option class="option" value="Aruba">Aruba</option>
                                                <option class="option" value="Australia">Australia</option>
                                                <option class="option" value="Austria">Austria</option>
                                                <option class="option" value="Azerbaijan">Azerbaijan</option>
                                                <option class="option" value="Bahamas">Bahamas</option>
                                                <option class="option" value="Bahrain">Bahrain</option>
                                                <option class="option" value="Bangladesh">Bangladesh</option>
                                                <option class="option" value="Barbados">Barbados</option>
                                                <option class="option" value="Belarus">Belarus</option>
                                                <option class="option" value="Belgium">Belgium</option>
                                                <option class="option" value="Belize">Belize</option>
                                                <option class="option" value="Benin">Benin</option>
                                                <option class="option" value="Bermuda">Bermuda</option>
                                                <option class="option" value="Bhutan">Bhutan</option>
                                                <option class="option" value="Bolivia">Bolivia</option>
                                                <option class="option" value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                <option class="option" value="Botswana">Botswana</option>
                                                <option class="option" value="Bouvet Island">Bouvet Island</option>
                                                <option class="option" value="Brazil">Brazil</option>
                                                <option class="option" value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                <option class="option" value="Brunei Darussalam">Brunei Darussalam</option>
                                                <option class="option" value="Bulgaria">Bulgaria</option>
                                                <option class="option" value="Burkina Faso">Burkina Faso</option>
                                                <option class="option" value="Burundi">Burundi</option>
                                                <option class="option" value="Cambodia">Cambodia</option>
                                                <option class="option" value="Cameroon">Cameroon</option>
                                                <option class="option" value="Canada">Canada</option>
                                                <option class="option" value="Cape Verde">Cape Verde</option>
                                                <option class="option" value="Cayman Islands">Cayman Islands</option>
                                                <option class="option" value="Central African Republic">Central African Republic</option>
                                                <option class="option" value="Chad">Chad</option>
                                                <option class="option" value="Chile">Chile</option>
                                                <option class="option" value="China">China</option>
                                                <option class="option" value="Christmas Island">Christmas Island</option>
                                                <option class="option" value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                <option class="option" value="Colombia">Colombia</option>
                                                <option class="option" value="Comoros">Comoros</option>
                                                <option class="option" value="Congo">Congo</option>
                                                <option class="option" value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                <option class="option" value="Cook Islands">Cook Islands</option>
                                                <option class="option" value="Costa Rica">Costa Rica</option>
                                                <option class="option" value="Cote D'ivoire">Cote D'ivoire</option>
                                                <option class="option" value="Croatia">Croatia</option>
                                                <option class="option" value="Cuba">Cuba</option>
                                                <option class="option" value="Cyprus">Cyprus</option>
                                                <option class="option" value="Czech Republic">Czech Republic</option>
                                                <option class="option" value="Denmark">Denmark</option>
                                                <option class="option" value="Djibouti">Djibouti</option>
                                                <option class="option" value="Dominica">Dominica</option>
                                                <option class="option" value="Dominican Republic">Dominican Republic</option>
                                                <option class="option" value="Ecuador">Ecuador</option>
                                                <option class="option" value="Egypt">Egypt</option>
                                                <option class="option" value="El Salvador">El Salvador</option>
                                                <option class="option" value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option class="option" value="Eritrea">Eritrea</option>
                                                <option class="option" value="Estonia">Estonia</option>
                                                <option class="option" value="Ethiopia">Ethiopia</option>
                                                <option class="option" value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                <option class="option" value="Faroe Islands">Faroe Islands</option>
                                                <option class="option" value="Fiji">Fiji</option>
                                                <option class="option" value="Finland">Finland</option>
                                                <option class="option" value="France">France</option>
                                                <option class="option" value="French Guiana">French Guiana</option>
                                                <option class="option" value="French Polynesia">French Polynesia</option>
                                                <option class="option" value="French Southern Territories">French Southern Territories</option>
                                                <option class="option" value="Gabon">Gabon</option>
                                                <option class="option" value="Gambia">Gambia</option>
                                                <option class="option" value="Georgia">Georgia</option>
                                                <option class="option" value="Germany">Germany</option>
                                                <option class="option" value="Ghana">Ghana</option>
                                                <option class="option" value="Gibraltar">Gibraltar</option>
                                                <option class="option" value="Greece">Greece</option>
                                                <option class="option" value="Greenland">Greenland</option>
                                                <option class="option" value="Grenada">Grenada</option>
                                                <option class="option" value="Guadeloupe">Guadeloupe</option>
                                                <option class="option" value="Guam">Guam</option>
                                                <option class="option" value="Guatemala">Guatemala</option>
                                                <option class="option" value="Guernsey">Guernsey</option>
                                                <option class="option" value="Guinea">Guinea</option>
                                                <option class="option" value="Guinea-bissau">Guinea-bissau</option>
                                                <option class="option" value="Guyana">Guyana</option>
                                                <option class="option" value="Haiti">Haiti</option>
                                                <option class="option" value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                <option class="option" value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                <option class="option" value="Honduras">Honduras</option>
                                                <option class="option" value="Hong Kong">Hong Kong</option>
                                                <option class="option" value="Hungary">Hungary</option>
                                                <option class="option" value="Iceland">Iceland</option>
                                                <option class="option" value="India">India</option>
                                                <option class="option" value="Indonesia">Indonesia</option>
                                                <option class="option" value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                <option class="option" value="Iraq">Iraq</option>
                                                <option class="option" value="Ireland">Ireland</option>
                                                <option class="option" value="Isle of Man">Isle of Man</option>
                                                <option class="option" value="Israel">Israel</option>
                                                <option class="option" value="Italy">Italy</option>
                                                <option class="option" value="Jamaica">Jamaica</option>
                                                <option class="option" value="Japan">Japan</option>
                                                <option class="option" value="Jersey">Jersey</option>
                                                <option class="option" value="Jordan">Jordan</option>
                                                <option class="option" value="Kazakhstan">Kazakhstan</option>
                                                <option class="option" value="Kenya">Kenya</option>
                                                <option class="option" value="Kiribati">Kiribati</option>
                                                <option class="option" value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                                <option class="option" value="Korea, Republic of">Korea, Republic of</option>
                                                <option class="option" value="Kuwait">Kuwait</option>
                                                <option class="option" value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option class="option" value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                <option class="option" value="Latvia">Latvia</option>
                                                <option class="option" value="Lebanon">Lebanon</option>
                                                <option class="option" value="Lesotho">Lesotho</option>
                                                <option class="option" value="Liberia">Liberia</option>
                                                <option class="option" value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                <option class="option" value="Liechtenstein">Liechtenstein</option>
                                                <option class="option" value="Lithuania">Lithuania</option>
                                                <option class="option" value="Luxembourg">Luxembourg</option>
                                                <option class="option" value="Macao">Macao</option>
                                                <option class="option" value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                <option class="option" value="Madagascar">Madagascar</option>
                                                <option class="option" value="Malawi">Malawi</option>
                                                <option class="option" value="Malaysia">Malaysia</option>
                                                <option class="option" value="Maldives">Maldives</option>
                                                <option class="option" value="Mali">Mali</option>
                                                <option class="option" value="Malta">Malta</option>
                                                <option class="option" value="Marshall Islands">Marshall Islands</option>
                                                <option class="option" value="Martinique">Martinique</option>
                                                <option class="option" value="Mauritania">Mauritania</option>
                                                <option class="option" value="Mauritius">Mauritius</option>
                                                <option class="option" value="Mayotte">Mayotte</option>
                                                <option class="option" value="Mexico">Mexico</option>
                                                <option class="option" value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                <option class="option" value="Moldova, Republic of">Moldova, Republic of</option>
                                                <option class="option" value="Monaco">Monaco</option>
                                                <option class="option" value="Mongolia">Mongolia</option>
                                                <option class="option" value="Montenegro">Montenegro</option>
                                                <option class="option" value="Montserrat">Montserrat</option>
                                                <option class="option" value="Morocco">Morocco</option>
                                                <option class="option" value="Mozambique">Mozambique</option>
                                                <option class="option" value="Myanmar">Myanmar</option>
                                                <option class="option" value="Namibia">Namibia</option>
                                                <option class="option" value="Nauru">Nauru</option>
                                                <option class="option" value="Nepal">Nepal</option>
                                                <option class="option" value="Netherlands">Netherlands</option>
                                                <option class="option" value="Netherlands Antilles">Netherlands Antilles</option>
                                                <option class="option" value="New Caledonia">New Caledonia</option>
                                                <option class="option" value="New Zealand">New Zealand</option>
                                                <option class="option" value="Nicaragua">Nicaragua</option>
                                                <option class="option" value="Niger">Niger</option>
                                                <option class="option" value="Nigeria">Nigeria</option>
                                                <option class="option" value="Niue">Niue</option>
                                                <option class="option" value="Norfolk Island">Norfolk Island</option>
                                                <option class="option" value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                <option class="option" value="Norway">Norway</option>
                                                <option class="option" value="Oman">Oman</option>
                                                <option class="option" value="Pakistan">Pakistan</option>
                                                <option class="option" value="Palau">Palau</option>
                                                <option class="option" value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                <option class="option" value="Panama">Panama</option>
                                                <option class="option" value="Papua New Guinea">Papua New Guinea</option>
                                                <option class="option" value="Paraguay">Paraguay</option>
                                                <option class="option" value="Peru">Peru</option>
                                                <option class="option" value="Philippines">Philippines</option>
                                                <option class="option" value="Pitcairn">Pitcairn</option>
                                                <option class="option" value="Poland">Poland</option>
                                                <option class="option" value="Portugal">Portugal</option>
                                                <option class="option" value="Puerto Rico">Puerto Rico</option>
                                                <option class="option" value="Qatar">Qatar</option>
                                                <option class="option" value="Reunion">Reunion</option>
                                                <option class="option" value="Romania">Romania</option>
                                                <option class="option" value="Russian Federation">Russian Federation</option>
                                                <option class="option" value="Rwanda">Rwanda</option>
                                                <option class="option" value="Saint Helena">Saint Helena</option>
                                                <option class="option" value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                <option class="option" value="Saint Lucia">Saint Lucia</option>
                                                <option class="option" value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                <option class="option" value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                <option class="option" value="Samoa">Samoa</option>
                                                <option class="option" value="San Marino">San Marino</option>
                                                <option class="option" value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                <option class="option" value="Saudi Arabia">Saudi Arabia</option>
                                                <option class="option" value="Senegal">Senegal</option>
                                                <option class="option" value="Serbia">Serbia</option>
                                                <option class="option" value="Seychelles">Seychelles</option>
                                                <option class="option" value="Sierra Leone">Sierra Leone</option>
                                                <option class="option" value="Singapore">Singapore</option>
                                                <option class="option" value="Slovakia">Slovakia</option>
                                                <option class="option" value="Slovenia">Slovenia</option>
                                                <option class="option" value="Solomon Islands">Solomon Islands</option>
                                                <option class="option" value="Somalia">Somalia</option>
                                                <option class="option" value="South Africa">South Africa</option>
                                                <option class="option" value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                <option class="option" value="Spain">Spain</option>
                                                <option class="option" value="Sri Lanka">Sri Lanka</option>
                                                <option class="option" value="Sudan">Sudan</option>
                                                <option class="option" value="Suriname">Suriname</option>
                                                <option class="option" value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                <option class="option" value="Swaziland">Swaziland</option>
                                                <option class="option" value="Sweden">Sweden</option>
                                                <option class="option" value="Switzerland">Switzerland</option>
                                                <option class="option" value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                <option class="option" value="Taiwan">Taiwan</option>
                                                <option class="option" value="Tajikistan">Tajikistan</option>
                                                <option class="option" value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                <option class="option" value="Thailand">Thailand</option>
                                                <option class="option" value="Timor-leste">Timor-leste</option>
                                                <option class="option" value="Togo">Togo</option>
                                                <option class="option" value="Tokelau">Tokelau</option>
                                                <option class="option" value="Tonga">Tonga</option>
                                                <option class="option" value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                <option class="option" value="Tunisia">Tunisia</option>
                                                <option class="option" value="Turkey">Turkey</option>
                                                <option class="option" value="Turkmenistan">Turkmenistan</option>
                                                <option class="option" value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                <option class="option" value="Tuvalu">Tuvalu</option>
                                                <option class="option" value="Uganda">Uganda</option>
                                                <option class="option" value="Ukraine">Ukraine</option>
                                                <option class="option" value="United Arab Emirates">United Arab Emirates</option>
                                                <option class="option" value="United Kingdom">United Kingdom</option>
                                                <option class="option" value="United States">United States</option>
                                                <option class="option" value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                <option class="option" value="Uruguay">Uruguay</option>
                                                <option class="option" value="Uzbekistan">Uzbekistan</option>
                                                <option class="option" value="Vanuatu">Vanuatu</option>
                                                <option class="option" value="Venezuela">Venezuela</option>
                                                <option class="option" value="Viet Nam">Viet Nam</option>
                                                <option class="option" value="Virgin Islands, British">Virgin Islands, British</option>
                                                <option class="option" value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                <option class="option" value="Wallis and Futuna">Wallis and Futuna</option>
                                                <option class="option" value="Western Sahara">Western Sahara</option>
                                                <option class="option" value="Yemen">Yemen</option>
                                                <option class="option" value="Zambia">Zambia</option>
                                                <option class="option" value="Zimbabwe">Zimbabwe</option>
                                            </select>
                                        </div>
                                        <div class="input-wrp">
                                            <p class="title">Street Address</p>
                                            <input type="text" placeholder="House number and street name">
                                            <input type="text" placeholder="Apartment, suite, unit, etc, (Optional)">
                                        </div>
                                        <div class="input-wrp">
                                            <p class="title">Town / City </p>
                                            <input type="text" name="town">
                                        </div>
                                        <div class="input-wrp">
                                            <p class="title">Postcode / ZIP </p>
                                            <input type="number" name="postcode">
                                        </div>
                                    </div>
                                --}}

                                <div class="input-wrp">
                                    <p class="title opt">order notes</p>
                                    <textarea name="order_notes" cols="50" rows="4" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                </div>
                        @endforeach
                    </div>
                </div>
                <div class="short-column large">
                    <div class="order-receipt">
                        <h3 class="top-heading left">Your order</h3>
                        @if(session('cart'))
                            <?php $total = 0; ?>
                            <?php $total_items = 0; ?>
                            @foreach(session('cart') as $id => $details)
                                <?php $total += $details['price'] * $details['quantity'] ?>
                            <div class="item-list">
                                <div class="item">
                                    <p class="title">{!! $details['name'] !!} &times; {{ $details['quantity'] }}</p>
                                    <span class="value"> Rs. {{ number_format($details['price'] * $details['quantity'], 0) }}</span>
                                </div>
                            </div>
                                <?php $total_items++; ?>
                            @endforeach
                        @endif

                        <div class="final">
                            <div class="subtotal item">
                                <p class="title">subtotal</p>
                                <span class="value"> Rs {{ number_format($total ,0) }}</span>
                            </div>
                            <div class="shipping item">
                                <p class="title">shipping</p>
                                <span class="value"> Enter your address to view shipping options.
                                </span>
                            </div>
                            <div class="total item">
                                <p class="title">total</p>
                                <span class="value"> Rs. {{ number_format($total+200 ,0) }}</span>
                            </div>
                        </div>
                        <div class="payment-method">
                            <div class="input-wrp checkbox">
                                <label class="title cod"><input type="radio" id="dbt" name="payment_method" value="cod" checked>Cash On Delivery </label>
                                <label class="title bank_transfer"><input type="radio" id="dbt" name="payment_method" value="Direct Bank Transfer">direct bank transfer </label>
                            </div>
                        </div>
                        {{-- GET DELIVERY COST --}}
                        @php $delivery_cost = 200; @endphp
                        {{-- GET DELIVERY COST --}}
                        <input type="hidden" name="order_items" value="{{ json_encode(session('cart')) }}"/>
                        <input type="hidden" name="order_total" value="{{ $total+$delivery_cost }}"/>
                        <button type="submit" class="place_order black-default">place order</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection
