@extends('web.layouts.app')
@section('content')

    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">Billing Address</h1>
                <ul>
                <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                <li><span>Add New Billing Address</span></li>
                </ul>
            </div>
        </div>
        <section class="section">
            <div class="container">
                <div class="tabs-wrapper">
                    <div class="tabs">
                        <span><a href="{{ config('app.app_path') }}/customer/my-account">my profile</a></span>
                        <span>my profile</span>
                        <span>my profile</span>
                        <span>my profile</span>
                        <span>my profile</span>
                        <span><a href="{{ config('app.app_path') }}/customer/logout">Logout</a></span>
                    </div>
                    <div class="details">
                        <div class="head">my profile
                        @if(Session::has('address_added_successfully'))
                            <div>
                                <p style="font-size: x-large; font-weight: 700; margin-bottom: 30px;">{{ Session::get('address_added_successfully') }}</p>
                            </div>
                        @endif</div>
                        <div class="contact-info">
                            <h2 class="top-heading">ADD NEW ADDRESS</h2>
                            <form action="{{ route('customer.post-new-billing') }}" method="post">
                            @csrf
                                <input type="text" name="first_name" placeholder="First Name">
                                <input type="text" name="last_name" placeholder="Last Name">
                                <input type="text" name="company_name" placeholder="Company Name">
                                <input type="text" name="phone_number" placeholder="Phone Number">
                                <input type="text" name="address_1" placeholder="Address 1">
                                <input type="text" name="address_2" placeholder="Address 2">
                                <input type="text" name="zip_code" placeholder="Zip Code">
                                <input type="text" name="city" placeholder="City">
                                <input type="text" name="state" placeholder="State">
                                @php
                                    $countries_list = array(
                                        "AF" => "Afghanistan",
                                        "AX" => "Aland Islands",
                                        "AL" => "Albania",
                                        "DZ" => "Algeria",
                                        "AS" => "American Samoa",
                                        "AD" => "Andorra",
                                        "AO" => "Angola",
                                        "AI" => "Anguilla",
                                        "AQ" => "Antarctica",
                                        "AG" => "Antigua and Barbuda",
                                        "AR" => "Argentina",
                                        "AM" => "Armenia",
                                        "AW" => "Aruba",
                                        "AU" => "Australia",
                                        "AT" => "Austria",
                                        "AZ" => "Azerbaijan",
                                        "BS" => "Bahamas",
                                        "BH" => "Bahrain",
                                        "BD" => "Bangladesh",
                                        "BB" => "Barbados",
                                        "BY" => "Belarus",
                                        "BE" => "Belgium",
                                        "BZ" => "Belize",
                                        "BJ" => "Benin",
                                        "BM" => "Bermuda",
                                        "BT" => "Bhutan",
                                        "BO" => "Bolivia",
                                        "BQ" => "Bonaire, Sint Eustatius and Saba",
                                        "BA" => "Bosnia and Herzegovina",
                                        "BW" => "Botswana",
                                        "BV" => "Bouvet Island",
                                        "BR" => "Brazil",
                                        "IO" => "British Indian Ocean Territory",
                                        "BN" => "Brunei Darussalam",
                                        "BG" => "Bulgaria",
                                        "BF" => "Burkina Faso",
                                        "BI" => "Burundi",
                                        "KH" => "Cambodia",
                                        "CM" => "Cameroon",
                                        "CA" => "Canada",
                                        "CV" => "Cape Verde",
                                        "KY" => "Cayman Islands",
                                        "CF" => "Central African Republic",
                                        "TD" => "Chad",
                                        "CL" => "Chile",
                                        "CN" => "China",
                                        "CX" => "Christmas Island",
                                        "CC" => "Cocos (Keeling) Islands",
                                        "CO" => "Colombia",
                                        "KM" => "Comoros",
                                        "CG" => "Congo",
                                        "CD" => "Congo, Democratic Republic of the Congo",
                                        "CK" => "Cook Islands",
                                        "CR" => "Costa Rica",
                                        "CI" => "Cote D'Ivoire",
                                        "HR" => "Croatia",
                                        "CU" => "Cuba",
                                        "CW" => "Curacao",
                                        "CY" => "Cyprus",
                                        "CZ" => "Czech Republic",
                                        "DK" => "Denmark",
                                        "DJ" => "Djibouti",
                                        "DM" => "Dominica",
                                        "DO" => "Dominican Republic",
                                        "EC" => "Ecuador",
                                        "EG" => "Egypt",
                                        "SV" => "El Salvador",
                                        "GQ" => "Equatorial Guinea",
                                        "ER" => "Eritrea",
                                        "EE" => "Estonia",
                                        "ET" => "Ethiopia",
                                        "FK" => "Falkland Islands (Malvinas)",
                                        "FO" => "Faroe Islands",
                                        "FJ" => "Fiji",
                                        "FI" => "Finland",
                                        "FR" => "France",
                                        "GF" => "French Guiana",
                                        "PF" => "French Polynesia",
                                        "TF" => "French Southern Territories",
                                        "GA" => "Gabon",
                                        "GM" => "Gambia",
                                        "GE" => "Georgia",
                                        "DE" => "Germany",
                                        "GH" => "Ghana",
                                        "GI" => "Gibraltar",
                                        "GR" => "Greece",
                                        "GL" => "Greenland",
                                        "GD" => "Grenada",
                                        "GP" => "Guadeloupe",
                                        "GU" => "Guam",
                                        "GT" => "Guatemala",
                                        "GG" => "Guernsey",
                                        "GN" => "Guinea",
                                        "GW" => "Guinea-Bissau",
                                        "GY" => "Guyana",
                                        "HT" => "Haiti",
                                        "HM" => "Heard Island and Mcdonald Islands",
                                        "VA" => "Holy See (Vatican City State)",
                                        "HN" => "Honduras",
                                        "HK" => "Hong Kong",
                                        "HU" => "Hungary",
                                        "IS" => "Iceland",
                                        "IN" => "India",
                                        "ID" => "Indonesia",
                                        "IR" => "Iran, Islamic Republic of",
                                        "IQ" => "Iraq",
                                        "IE" => "Ireland",
                                        "IM" => "Isle of Man",
                                        "IL" => "Israel",
                                        "IT" => "Italy",
                                        "JM" => "Jamaica",
                                        "JP" => "Japan",
                                        "JE" => "Jersey",
                                        "JO" => "Jordan",
                                        "KZ" => "Kazakhstan",
                                        "KE" => "Kenya",
                                        "KI" => "Kiribati",
                                        "KP" => "Korea, Democratic People's Republic of",
                                        "KR" => "Korea, Republic of",
                                        "XK" => "Kosovo",
                                        "KW" => "Kuwait",
                                        "KG" => "Kyrgyzstan",
                                        "LA" => "Lao People's Democratic Republic",
                                        "LV" => "Latvia",
                                        "LB" => "Lebanon",
                                        "LS" => "Lesotho",
                                        "LR" => "Liberia",
                                        "LY" => "Libyan Arab Jamahiriya",
                                        "LI" => "Liechtenstein",
                                        "LT" => "Lithuania",
                                        "LU" => "Luxembourg",
                                        "MO" => "Macao",
                                        "MK" => "Macedonia, the Former Yugoslav Republic of",
                                        "MG" => "Madagascar",
                                        "MW" => "Malawi",
                                        "MY" => "Malaysia",
                                        "MV" => "Maldives",
                                        "ML" => "Mali",
                                        "MT" => "Malta",
                                        "MH" => "Marshall Islands",
                                        "MQ" => "Martinique",
                                        "MR" => "Mauritania",
                                        "MU" => "Mauritius",
                                        "YT" => "Mayotte",
                                        "MX" => "Mexico",
                                        "FM" => "Micronesia, Federated States of",
                                        "MD" => "Moldova, Republic of",
                                        "MC" => "Monaco",
                                        "MN" => "Mongolia",
                                        "ME" => "Montenegro",
                                        "MS" => "Montserrat",
                                        "MA" => "Morocco",
                                        "MZ" => "Mozambique",
                                        "MM" => "Myanmar",
                                        "NA" => "Namibia",
                                        "NR" => "Nauru",
                                        "NP" => "Nepal",
                                        "NL" => "Netherlands",
                                        "AN" => "Netherlands Antilles",
                                        "NC" => "New Caledonia",
                                        "NZ" => "New Zealand",
                                        "NI" => "Nicaragua",
                                        "NE" => "Niger",
                                        "NG" => "Nigeria",
                                        "NU" => "Niue",
                                        "NF" => "Norfolk Island",
                                        "MP" => "Northern Mariana Islands",
                                        "NO" => "Norway",
                                        "OM" => "Oman",
                                        "PK" => "Pakistan",
                                        "PW" => "Palau",
                                        "PS" => "Palestinian Territory, Occupied",
                                        "PA" => "Panama",
                                        "PG" => "Papua New Guinea",
                                        "PY" => "Paraguay",
                                        "PE" => "Peru",
                                        "PH" => "Philippines",
                                        "PN" => "Pitcairn",
                                        "PL" => "Poland",
                                        "PT" => "Portugal",
                                        "PR" => "Puerto Rico",
                                        "QA" => "Qatar",
                                        "RE" => "Reunion",
                                        "RO" => "Romania",
                                        "RU" => "Russian Federation",
                                        "RW" => "Rwanda",
                                        "BL" => "Saint Barthelemy",
                                        "SH" => "Saint Helena",
                                        "KN" => "Saint Kitts and Nevis",
                                        "LC" => "Saint Lucia",
                                        "MF" => "Saint Martin",
                                        "PM" => "Saint Pierre and Miquelon",
                                        "VC" => "Saint Vincent and the Grenadines",
                                        "WS" => "Samoa",
                                        "SM" => "San Marino",
                                        "ST" => "Sao Tome and Principe",
                                        "SA" => "Saudi Arabia",
                                        "SN" => "Senegal",
                                        "RS" => "Serbia",
                                        "CS" => "Serbia and Montenegro",
                                        "SC" => "Seychelles",
                                        "SL" => "Sierra Leone",
                                        "SG" => "Singapore",
                                        "SX" => "Sint Maarten",
                                        "SK" => "Slovakia",
                                        "SI" => "Slovenia",
                                        "SB" => "Solomon Islands",
                                        "SO" => "Somalia",
                                        "ZA" => "South Africa",
                                        "GS" => "South Georgia and the South Sandwich Islands",
                                        "SS" => "South Sudan",
                                        "ES" => "Spain",
                                        "LK" => "Sri Lanka",
                                        "SD" => "Sudan",
                                        "SR" => "Suriname",
                                        "SJ" => "Svalbard and Jan Mayen",
                                        "SZ" => "Swaziland",
                                        "SE" => "Sweden",
                                        "CH" => "Switzerland",
                                        "SY" => "Syrian Arab Republic",
                                        "TW" => "Taiwan, Province of China",
                                        "TJ" => "Tajikistan",
                                        "TZ" => "Tanzania, United Republic of",
                                        "TH" => "Thailand",
                                        "TL" => "Timor-Leste",
                                        "TG" => "Togo",
                                        "TK" => "Tokelau",
                                        "TO" => "Tonga",
                                        "TT" => "Trinidad and Tobago",
                                        "TN" => "Tunisia",
                                        "TR" => "Turkey",
                                        "TM" => "Turkmenistan",
                                        "TC" => "Turks and Caicos Islands",
                                        "TV" => "Tuvalu",
                                        "UG" => "Uganda",
                                        "UA" => "Ukraine",
                                        "AE" => "United Arab Emirates",
                                        "GB" => "United Kingdom",
                                        "US" => "United States",
                                        "UM" => "United States Minor Outlying Islands",
                                        "UY" => "Uruguay",
                                        "UZ" => "Uzbekistan",
                                        "VU" => "Vanuatu",
                                        "VE" => "Venezuela",
                                        "VN" => "Viet Nam",
                                        "VG" => "Virgin Islands, British",
                                        "VI" => "Virgin Islands, U.s.",
                                        "WF" => "Wallis and Futuna",
                                        "EH" => "Western Sahara",
                                        "YE" => "Yemen",
                                        "ZM" => "Zambia",
                                        "ZW" => "Zimbabwe"
                                    );
                                @endphp
                                <select id="country" required name="country" class="form-control empty">
                                    <option class="option" class="disabled" value="" disabled selected>Country</option>
                                    @php
                                        foreach ($countries_list as $key => $value):
                                            echo '<option value="'.$key.'">'.$value.'</option>';
                                        endforeach;
                                    @endphp
                                </select>
                                <button type="submit" class="black">Add Address</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
