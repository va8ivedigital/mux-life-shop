@extends('web.layouts.app')
@section('content')

    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">ABOUT US</h1>
                <ul>
                    <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                    <li><span>About us</span></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="section">
                <div class="heading-wrapper">
                    <h2 class="top-heading">BEHIND THE SCENE</h2>
                    <p class="sub-heading fz16">Phasellus lorem malesuada ligula pulvinar commodo maecenas suscipit auctom.</p>
                </div>

                <ul class="about-us-main">
                    @foreach($about_sections as $about_section)
                        <li>
                            <div class="image-container">
                                <picture>
                                    <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{config('app.image_path')}}{{ !empty($about_section['image']) ? $about_section['image'] : '' }}" alt="" width="579" height="446">
                                </picture>
                            </div>
                            <div class="content">
                                <h3 class="title">{{ $about_section['title'] }}</h3>
                                <p class="description">{!! $about_section['description'] !!}</p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="section ">
                <div class="heading-wrapper">
                    <h2 class="top-heading">Meet Our Team</h2>
                    <p class="sub-heading fz16">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>

                <div class="grid-column-3">
                    @foreach($team_members as $member)
                        <div class="team-card">
                            <div class="image-container">
                                <picture>
                                    <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{config('app.image_path')}}{{ !empty($member['member_image']) ? $member['member_image'] : '' }}" alt="{{ $member['title'] }}" width="322" height="290">
                                </picture>
                            </div>
                            <div class="content">
                                <h3 class="name">{{ $member['title'] }}</h3>
                                <p class="designation">{{ $member['designation'] }}</p>
                                <ul class="social-links">
                                    <li><a href="{{ !empty($member['facebook_url']) ? $member['facebook_url'] : 'javascript:;' }}" class="facebook"><i class="x_facebook"></i></a></li>
                                    <li><a href="{{ !empty($member['twitter_url']) ? $member['twitter_url'] : 'javascript:;' }}" class="twitter"><i class="x_twitter"></i></a></li>
                                    <li><a href="{{ !empty($member['instagram_url']) ? $member['instagram_url'] : 'javascript:;' }}" class="instagram"><i class="x_instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
