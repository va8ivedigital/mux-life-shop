@extends('web.layouts.app')
@section('content')

@php
    $imgHolder = 'data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
@endphp
<div class="main">
    <div class="breadcrumb">
        <div class="container">
            <h1 class="title">PRODUCT</h1>
            <ul>
                <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                <li><span>Product</span></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="flex Rv rowbar">
            <div class="wide-column small">
                <div class="section ptn">
                </div>
                <div class="section">
                    <div class="grid-column-3">
                        @foreach($allProducts as $product)
                            <!-- product card style starts Here: product_card.scss-->
                           <div class="product-card">
                                <div class="product-card-inner">
                                    @if($product['sale']!=0)
                                        <div class="sale">SALE</div>
                                    @endif
                                    <div class="wishlist">
                                        <a href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}" class="view">
                                            <i class="x_eye"></i>
                                        </a>
                                        {{-- Favourite will show after login --}}
                                        <a href="javaScript:;" class="add-to-fav">
                                            <i class="x_favorite"></i>
                                        </a>
                                    </div>
                                    <a class="product-image-wrap" href="javascript:;">
                                        <picture>
                                            <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($product['image']) ? $product['image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="170" height="170"/>
                                        </picture>
                                    </a>
                                    <div class="product-detail">
                                        <div class="rating-cart">
                                            <div class="rating">
                                                @php $i=0; @endphp
                                                @while($i < $product['rating'])
                                                    <input type="radio" name="georgina" value="{{ $i }}">
                                                    <label class="full RateActive"></label>
                                                    @php $i++ @endphp
                                                @endwhile
                                            </div>

                                            @if(null !== session('cart'))
                                                @if(array_key_exists($product['id'],session('cart')))
                                                    <a href="javascript:;" class="add-to-cart">ALREADY IN CART !</a>
                                                @else
                                                    <a href="{{ url('add-to-cart/'.$product['id']) }}" class="add-to-cart">ADD TO CART</a>
                                                @endif
                                            @else
                                                @if(Auth::guard('customer')->user())
                                                    {{-- LOGGED IN USER WILL ADD TO CART --}}
                                                    <a href="{{ url('add-to-cart/'.$product['id']) }}" class="add-to-cart">ADD TO CART</a>
                                                @else
                                                    {{-- LOGIN FIRST SHOW PLEASE SIGN IN POP UP HERE --}}
                                                    <a href="javascript:;" class="add-to-cart">ADD TO CART</a>
                                                @endif
                                            @endif

                                        </div>
                                        <a class="product-title" href="{{ config('app.app_path') }}/product/{{ $product['slug'] }}">{{ $product['title'] }}</a>
                                        <div class="product-pricing">
                                            @if(!empty($product['old_price']) || $product['old_price']!=0 || $product['old_price'] != '' || $product['old_price'] != NULL)
                                                <span class="discount">Rs. {{ number_format($product['old_price'], 0) }}</span>
                                            @endif
                                            <span>Rs. {{ number_format($product['price'], 0) }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- product card style Ends Here-->
                        @endforeach
                    </div>
                </div>

                <div class="section pbn pagination">
                    {{ $allProducts->links('vendor.pagination.product') }}
                </div>
            </div>
            <div class="short-column small">
                @if(isset($productsCategory))
                    <div class="sidebar-section">
                        <h4 class="title">Shop by Category</h4>
                        <div class="inner-section">
                            <div class="sidebar-links">
                                <ul>
                                    @foreach($productsCategory as $productCategory)
                                        <li><a href="{{ config('app.app_path') }}/product-categories/{{ $productCategory['slug'] }}">{{ $productCategory['name'] }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="sidebar-section">
                    <h4 class="title">Filter by price</h4>
                    <div class="inner-section">
                        <div class="range-slider">
                            <div class="main">
                                <div class="slider-track"></div>
                                <input type="range" min="0" max="100" value="0" id="slider-1" oninput="slideOne()">
                                <input type="range" min="0" max="100" value="1000" id="slider-2" oninput="slideTwo()">
                            </div>
                            <div class="values">
                                <p>price:$
                                    <span id="range1">
                                        0
                                    </span>
                                </p>
                                <p> price:$
                                    <span id="range2">
                                        100
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($productTags))
                    <div class="sidebar-section">
                        <h4 class="title">Product Tags</h4>
                        <div class="inner-section">
                            <div class="sidebar-tags">
                                <ul>
                                    @foreach ($productTags as $product)
                                        @foreach ($product->tags as $tag)
                                            <li><a href="{{ config('app.app_path') }}/product-tag/{{ $tag->slug }}">{{ $tag->title }}</a></li>
                                        @endforeach
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="sidebar-section">
                    <h4 class="title">New Products</h4>
                    <div class="inner-section">
                        <div class="product-listing">
                            @foreach($latestProducts as $latestProduct)
                                <div class="prd">
                                    <a href="{{ config('app.app_path') }}/product/{{ $latestProduct['slug'] }}" class="image-container">
                                        <picture>
                                            <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($latestProduct['image']) ? $latestProduct['image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="75" height="75">
                                        </picture>
                                    </a>
                                    <div class="prdInfo">
                                        <a href="{{ config('app.app_path') }}/product/{{ $latestProduct['slug'] }}">{{ $latestProduct['title'] }}</a>
                                        <p class="price">Rs.{{ number_format($latestProduct['price'], 0) }}</p>
                                        <div class="rating">
                                            @php $i=0; @endphp
                                            @while($i < $latestProduct['rating'])
                                                <input type="radio" name="georgina" value="{{ $i }}">
                                                <label class="full RateActive"></label>
                                                @php $i++ @endphp
                                            @endwhile
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="view-all-prd">
                            <a href="{{config('app.app_path')}}/shop">
                                <span>VIEW ALL PRODUCTS</span>
                                <i class="x_arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
