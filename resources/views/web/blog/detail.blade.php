@extends('web.layouts.app')
@section('content')
@php
    $imgHolder = 'data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
@endphp
{{-- @dd($detail) --}}
    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">{!! $detail['title'] !!}</h1>
                <ul>
                    <li><a href="{{config('app.app_path')}}/">Home</a></li>
                    <li><a href="{{config('app.app_path')}}/blog">Blog</a></li>
                    <li><span>{!! $detail['title'] !!}</span></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="flex Rv rowbar">
                <div class="wide-column small">
                    <div class="section ptn">
                        <div class='blog-detail-card'>
                            <div class="description">
                                <h2 class="title">{!! $detail['title'] !!}</h2>
                                <ul class="blog-detail-info">
                                    <li class="author"><i class="x_user"></i> {!! $detail['user']['name'] !!}</li>
                                    <li class="date"><i class="x_comment"></i> Leave A Comment</li>
                                    <li class="date"><i class="x_bookmark"></i> Posted In Blog</li>
                                </ul>
                            </div>

                            <div class="image-container">
                                <picture>
                                    <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($detail['blog_image']) ? $detail['blog_image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="{!! $detail['title'] !!}" width="861" height="520"/>
                                </picture>
                            </div>
                        </div>
                        <div class="blog-detail-content">{!! html_entity_decode($detail['long_description']) !!}</div>
                    </div>
                    <div class="section pbn blog-detail-form">
                        <div class="contactForm">
                            <h2 class="top-heading left">Leave a message</h2>
                            <p class="sub-heading fz16 left">Your email address will not be published. Required fields are marked *</p>
                            <form class="formWrap">
                                <div class="inputWrapper fullColumn">
                                    <label for="">Comment</label>
                                    <textarea placeholder="Enter your message"></textarea>
                                </div>
                                <div class="inputWrapper fullColumn">
                                    <label for="name">Name *</label>
                                    <input type="text" name="name" value="" placeholder="Enter your complete name" id="input-name" required="">
                                </div>
                                <div class="inputWrapper fullColumn">
                                    <label for="email">Email *</label>
                                    <input type="text" name="email" value="" placeholder="Enter your e-mail address" id="input-email" required="">
                                </div>
                                <div class="chBxWr fullColumn">
                                    <span class="checkBox">
                                        <input class="checkbox" id="remember" type="checkbox" name="code" />
                                        <label for="remember">Save my name, email, and website in this browser for the next time I comment.</label>
                                    </span>
                                </div>
                                <div class="fullColumn button">
                                    <button type="submit" class="blue">SUBMIT</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="short-column small">
                    @if(isset($recentBlogs))
                        <div class="sidebar-section">
                            <h4 class="title">{{ trans('sentence.recent_post') }}</h4>
                            <div class="inner-section">
                                <div class="sidebar-links">
                                    <ul>
                                        @foreach($recentBlogs as $recentBlog)
                                            <li><a href="{{ config('app.app_path') }}/blog/{{ $recentBlog['slug'] }}">{!! $recentBlog['title'] !!}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="sidebar-section">
                        <h4 class="title">{{ trans('sentence.recent_comments') }}</h4>
                        <div class="inner-section">
                        </div>
                    </div>

                    @if(isset($recentBlogsByYear))
                        <div class="sidebar-section">
                            <h4 class="title">{{ trans('sentence.archives') }}</h4>
                            <div class="inner-section">
                                <div class="sidebar-links">
                                    <ul>
                                        @foreach($recentBlogsByYear as $recentByYear)
                                            @php
                                                $monthYear = date("M Y", strtotime($recentByYear['created_at']));
                                            @endphp
                                            <li><a href="{{ config('app.app_path') }}/blog/{{ $recentByYear['slug'] }}">{!! $monthYear !!}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(isset($blogCategory))
                        <div class="sidebar-section">
                            <h4 class="title">Categories</h4>
                            <div class="inner-section">
                                <div class="sidebar-links">
                                    <ul>
                                        @foreach($blogCategory as $cat)
                                            <li><a href="{{ config('app.app_path') }}/blog-categories/{{ $cat['slug'] }}">{{ $cat['title'] }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(isset($latestProducts))
                        <div class="sidebar-section">
                            <h4 class="title">New Products</h4>
                            <div class="inner-section">
                                <div class="product-listing">
                                    @foreach($latestProducts as $latestProduct)
                                        <div class="prd">
                                            <a href="{{ config('app.app_path') }}/product/{{ $latestProduct['slug'] }}" class="image-container">
                                                <picture>
                                                    <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($latestProduct['image']) ? $latestProduct['image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="75" height="75">
                                                </picture>
                                            </a>
                                            <div class="prdInfo">
                                                <a href="{{ config('app.app_path') }}/product/{{ $latestProduct['slug'] }}">{{ $latestProduct['title'] }}</a>
                                                <p class="price">Rs.{{ number_format($latestProduct['price'], 0) }}</p>
                                                <div class="rating">
                                                    @php $i=0; @endphp
                                                    @while($i < $latestProduct['rating'])
                                                        <input type="radio" name="georgina" value="{{ $i }}">
                                                        <label class="full RateActive"></label>
                                                        @php $i++ @endphp
                                                    @endwhile
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="view-all-prd">
                                    <a href="{{config('app.app_path')}}/shop">
                                        <span>VIEW ALL PRODUCTS</span>
                                        <i class="x_arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
