@extends('web.layouts.app')
@section('content')
    @php
        $imgHolder = 'data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
    @endphp
    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">BLOG</h1>
                <ul>
                    <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                    <li><span>Blog</span></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="flex Rv rowbar">
                <div class="wide-column small">
                    <div class="section ptn">
                        <div class="grid-column-1">
                            @foreach($allBlogs as $allBlog)
                                <div class="flex blog-card">
                                    <a class="image-container" href="{{ config('app.app_path') }}/blog/{{ $allBlog['slug'] }}">
                                        <picture>
                                            <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($allBlog['blog_image']) ? $allBlog['blog_image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="366" height="261" />
                                        </picture>
                                    </a>
                                    <div class="post-detail">
                                        <div class="postmeta">
                                            <span class="date">@php echo date("M j, Y", strtotime($allBlog['created_at'])); @endphp</span>
                                        </div>
                                        <a class="post-title" href="{{ config('app.app_path') }}/blog/{{ $allBlog['slug'] }}">{!! $allBlog['title'] !!}</a>
                                        <p class="post-description">{!! Str::limit($allBlog['short_description'], 150) !!}</p>
                                        <a class="blog-btn blue" href="{{ config('app.app_path') }}/blog/{{ $allBlog['slug'] }}">READ MORE</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="section pagination">
                        {{ $allBlogs->links('vendor.pagination.blog') }}
                    </div>
                </div>
                <div class="short-column small">
                    @if(isset($recentBlogs))
                        <div class="sidebar-section">
                            <h4 class="title">{{ trans('sentence.recent_post') }}</h4>
                            <div class="inner-section">
                                <div class="sidebar-links">
                                    <ul>
                                        @foreach($recentBlogs as $recentBlog)
                                            <li><a href="{{ config('app.app_path') }}/blog/{{ $recentBlog['slug'] }}">{!! $recentBlog['title'] !!}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="sidebar-section">
                        <h4 class="title">{{ trans('sentence.recent_comments') }}</h4>
                        <div class="inner-section">
                        </div>
                    </div>

                    @if(isset($recentBlogsByYear))
                        <div class="sidebar-section">
                            <h4 class="title">{{ trans('sentence.archives') }}</h4>
                            <div class="inner-section">
                                <div class="sidebar-links">
                                    <ul>
                                        @foreach($recentBlogsByYear as $recentByYear)
                                            @php
                                                $monthYear = date("M Y", strtotime($recentByYear['created_at']));
                                            @endphp
                                            <li><a href="{{ config('app.app_path') }}/blog/{{ $recentByYear['slug'] }}">{!! $monthYear !!}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(isset($blogCategory))
                        <div class="sidebar-section">
                            <h4 class="title">Categories</h4>
                            <div class="inner-section">
                                <div class="sidebar-links">
                                    <ul>
                                        @foreach($blogCategory as $cat)
                                            <li><a href="{{ config('app.app_path') }}/blog-categories/{{ $cat['slug'] }}">{{ $cat['title'] }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                @if(isset($latestProducts))
                    <div class="sidebar-section">
                        <h4 class="title">New Products</h4>
                        <div class="inner-section">
                            <div class="product-listing">
                                @foreach($latestProducts as $latestProduct)
                                    <div class="prd">
                                        <a href="{{ config('app.app_path') }}/product/{{ $latestProduct['slug'] }}" class="image-container">
                                            <picture>
                                                <img src="{{config('app.image_path')}}/build/images/placeholder.webp" data-src="{{ isset($latestProduct['image']) ? $latestProduct['image'] : config('app.app_image').'/build/images/placeholder.png' }}" alt="" width="75" height="75">
                                            </picture>
                                        </a>
                                        <div class="prdInfo">
                                            <a href="{{ config('app.app_path') }}/product/{{ $latestProduct['slug'] }}">{{ $latestProduct['title'] }}</a>
                                            <p class="price">Rs.{{ number_format($latestProduct['price'], 0) }}</p>
                                            <div class="rating">
                                                @php $i=0; @endphp
                                                @while($i < $latestProduct['rating'])
                                                    <input type="radio" name="georgina" value="{{ $i }}">
                                                    <label class="full RateActive"></label>
                                                    @php $i++ @endphp
                                                @endwhile
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="view-all-prd">
                                <a href="{{config('app.app_path')}}/shop">
                                    <span>VIEW ALL PRODUCTS</span>
                                    <i class="x_arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif

                </div>
            </div>
        </div>
    </div>
@endsection
