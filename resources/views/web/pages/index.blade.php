@extends('web.layouts.app')
@section('content')
    <div class="section main">
        <div class="section">
            <div class="container">
                <div class="richTextContent">
                    @if(!empty($pageRecord['title']))
                        <h1>{!! $pageRecord['title'] !!}</h1>
                    @endif
                    @if(!empty($pageRecord['description']))
                        {!! html_entity_decode($pageRecord['description']) !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
