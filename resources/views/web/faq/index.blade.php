@extends('web.layouts.app')
@section('content')

    <div class="main">
        <div class="breadcrumb">
            <div class="container">
                <h1 class="title">FAQs</h1>
                <ul>
                    <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                    <li><span>faqs</span></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="section">
                <div class="heading-wrapper">
                    <h2 class="top-heading">FAQs</h2>
                </div>
                <div class="contact-info">
                    <ul class="faq-accordian">
                        @php $i=1; @endphp
                        @foreach($faqs as $faq)
                            <li>
                                <a href="javascript:;" class="faq-toggle <?php if ($i === 1) { ?> active <?php } ?>">{{$i++}}. {{ $faq['title'] }}
                                    <i class="x_down"></i>
                                </a>
                               <div class="inner <?php if ($i === 1) { ?> show <?php } ?>">
                                    <div class="content">
                                        <p>{!! $faq['description'] !!}</p>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
