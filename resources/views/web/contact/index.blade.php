@extends('web.layouts.app')
@section('content')
    @if(isset($contact_details))
        @foreach($contact_details as $contact)
            <div class="main">
                <div class="breadcrumb">
                    <div class="container">
                        <h1 class="title"> CONTACT & MAP</h1>
                        <ul>
                            <li><a href="{{ config('app.app_path') }}/">Home</a></li>
                            <li><span>Contact & map</span></li>
                        </ul>
                    </div>
                </div>
                <div class="container">
                    @if(!empty($contact['embeded_map']))
                        <div class="section">
                            <div class="heading-wrapper">
                                <h2 class="top-heading">Google Map</h2>
                            </div>
                            <div class="map-direction">
                                {!! $contact['embeded_map'] !!}
                            </div>
                        </div>
                    @endif
                    <div class="section contact-details">
                        <div class="contact-desc">
                            <h3 class="title">CONTACT ADDRESSS</h3>
                            @if(!empty($contact['description']))
                                <p class="sub-title">{!! $contact['description'] !!}</p>
                            @endif
                        </div>
                        <div class="grid-column-3">
                            @if(!empty($contact['address']))
                            <div class="contact-cards">
                                <i class="x_map-marker"></i>
                                <div class="content">
                                    <p>Address</p>
                                    {!! $contact['address'] !!}
                                </div>
                            </div>
                            @endif
                            @if(!empty($contact['phone_number']))
                            <div class="contact-cards">
                                <i class="x_phone"></i>
                                <div class="content">
                                    <p>Phone Numbers</p>
                                    {!! $contact['phone_number'] !!}
                                </div>
                            </div>
                            @endif
                            @if(!empty($contact['email']))
                                <div class="contact-cards">
                                    <i class="x_envelope"></i>
                                    <div class="content">
                                        <p>Email</p>
                                        {!! $contact['email'] !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endsection
