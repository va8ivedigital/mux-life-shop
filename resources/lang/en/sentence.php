<?php

return [
    /******** HOME PAGE VARIABLES ********/
    'home_weekly_heading' => 'Weekly Featured',
    'home_weekly_sub'     => 'Amazing Weekly Featured Item Collection',
    'footer_text'         => 'Mux offers smart home and smart office solutions. You can buy all kinds of home automation products for your smart home. With our sensors and smart devices you can achieve energy efficiency and bring convenience in to your life.',
    'footer_copyright'    => 'Copyright &copy; 2021 Mux.life',
    'signup_newsletter'    => 'Sign Up For Newsletter',
    'subscribe_button'    => 'Subscribe',
    'our_clients_heading'    => 'Our Clients',
    'our_clients_sub'    => 'Some of our corporate clients',
    'user_reviews_heading'    => 'User Reviews',
    'user_reviews_sub'    => 'What Our Users Says About Us',
    'latest_news_heading'    => 'Our Latest News',
    'latest_news_sub'    => 'What are the advantages of a Smart Home in Pakistan',

    /******** BLOG VARIABLES ********/
    'recent_post'     => 'Recent Posts',
    'recent_comments' => 'Recent Comments',
    'archives'        => 'Archives',

];
